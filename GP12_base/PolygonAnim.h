#ifndef ___POLYGONANIM_H___
#define ___POLYGONANIM_H___

#pragma once

// ポリゴンのアニメーション用データ
// 2013.07.23

#include "main.h"

// 共通設定
typedef struct _tAnimKeyData
{
	int		frame;	// 変更フレーム
	RECT	pos;	// テクスチャ座標
	int		flag;	// フラグ(ループなど)
}tAnimKeyData;

#define ANIM_FLAG_NONE	(0x00)	// フラグなし
#define ANIM_FLAG_LOOP	(0x01)	// ループする（先頭へ）
#define ANIM_FLAG_STOP	(0x02)	// 停止する

// パーティクル
extern const D3DXVECTOR2 c_sizeParticle;
extern const tAnimKeyData* c_animParticle[];
// ビーコン
extern const D3DXVECTOR2 c_sizeBeacon;
extern const tAnimKeyData* c_animBeacon[];
// ロード
extern const D3DXVECTOR2 c_sizeNowLoading;
extern const tAnimKeyData* c_animNowLoading[];
// セレクト画面のマニュアル
extern const D3DXVECTOR2 c_sizeSelectMenuManual;
extern const tAnimKeyData* c_animSelectMenuManual[];


enum Particle
{
	ANIM_PARTICLE00,
	ANIM_PARTICLE01,
	ANIM_PARTICLE02,
};

enum Beacon
{
	ANIM_BEACON00,
	ANIM_BEACON01,
	ANIM_BEACON02,
};

enum SelectMenuManual
{
	MANUAL_UPARROW,
	MANUAL_DOWNARROW,
	MANUAL_ENTER,
};

#endif