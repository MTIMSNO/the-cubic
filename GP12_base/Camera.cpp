//===================================================================================
//
// カメラ.cpp
//
//===================================================================================

#include "Camera.h"
//#include "MeshStage01.h"
#include "StageData.h"

#include <math.h>

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// 回転
void CameraRoll(int time , int roll);
// 移動
void CameraMove ( int nTime );

//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------

typedef struct _tCamera
{
	// カメラの位置
	D3DXVECTOR3		vecEyePt;		
	// 注視位置
	D3DXVECTOR3		vecLookatPt;	
	// 上方位置
	D3DXVECTOR3		vecUpVec;		
	// ??
	D3DXMATRIXA16	matView;		
	// ??
	float			matrixOrthoLH;
	// 速度
	D3DXVECTOR3		speed;	
	// 角度
	float			angle;			
	// 距離
	int				distance;	
	// カメラの移動
	bool			move;
	// カメラの移動カウント
	int				cameraMoveCount;
}tCamera;

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

// カメラ
tCamera				Camera;
// ??
D3DXMATRIXA16		matProj;
// プレイヤーの位置
D3DXVECTOR3			g_nPlayerPos;
// 回転量
float				g_fCameraRollAngle;
// 回転カウント
int					g_nCameraRollCount;
// カメラステータス(画面遷移)
int					g_nCameraState;
// 回転フラグ
bool				g_bCameraRoll_R;
bool				g_bCameraRoll_L;
// カメラ移動
bool				g_bCameraMove;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitCamera(LPDIRECT3DDEVICE9 pDevice)
{
	// カメラ（視点）位置
	Camera.vecEyePt = D3DXVECTOR3( 0.0f, 300.0f, -300.0f );		
	// 注視位置
	Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );		
	// 上方位置
	Camera.vecUpVec = D3DXVECTOR3( 0.0f, 1.0f,  0.0f );		
	// 速度
	Camera.speed = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );
	// 角度
	Camera.angle = 90;
	// 距離
	Camera.distance = 200;
	// カメラの移動
	Camera.move = false;
	// カメラの移動カウント
	Camera.cameraMoveCount = 0;

	// プロジェクショントランスフォーム(射影変換)
	D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
	pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
	// ビュートランスフォーム(視点座標変換)
	D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
	pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );

	// 変数の初期化
	g_fCameraRollAngle = 0.0f;
	g_nCameraRollCount = 0;
	g_bCameraRoll_R = false;
	g_bCameraRoll_L = false;
	g_bCameraMove = false;
	g_nCameraState = 0;
	g_nPlayerPos = D3DXVECTOR3(0.0f,0.0f,0.0f);

	return S_OK;
}
//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateCamera(LPDIRECT3DDEVICE9 pDevice)
{
	g_nCameraState = GetData(DATA_MAINSTATE);

	switch ( g_nCameraState )
	{
	case STATE_MAINMENU:

		// カメラ（視点）位置
		Camera.vecEyePt = D3DXVECTOR3( 0.0f, 0.0f, -25.0f );		
		// 注視位置
		Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );

		Camera.vecEyePt.x = Camera.vecEyePt.x + cos( Camera.angle * D3DX_PI/180 ) * (Camera.distance-150.0f);
		Camera.vecEyePt.z = Camera.vecEyePt.z + sin( Camera.angle * D3DX_PI/180 ) * (Camera.distance-150.0f);

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );

		break;

	case STATE_SELECT:

		// カメラ（視点）位置
		Camera.vecEyePt = D3DXVECTOR3( 0.0f, 0.0f, 50.0f );		
		// 注視位置
		Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );

		break;

	case STATE_GAME:
		// プレイヤー座標の取得
		g_nPlayerPos = GetPlayerPos();

		if ( GetPlayerChange() == true )
			if ( GetKeyboardTrigger(DIK_SPACE) || GetJoyStickTrigger(1) )
					Camera.move = true;

		CameraMove(30);

		// 右
		if ( g_bCameraRoll_R == false && g_bCameraRoll_L == false )
		{
			if ( GetKeyboardTrigger(DIK_D) || GetJoyStickTrigger(5) || GetJoyStickTrigger(7) )
			{
				g_bCameraRoll_R = true;
			}
		}
		if ( g_bCameraRoll_R && g_bCameraRoll_L == false )
			CameraRoll(30,RIGHTROLL);

		// 左
		if ( g_bCameraRoll_R == false && g_bCameraRoll_L == false )
		{
			if ( GetKeyboardTrigger(DIK_A) || GetJoyStickTrigger(4) || GetJoyStickTrigger(6) )
			{
				g_bCameraRoll_L = true;
			}
		}
		if ( g_bCameraRoll_R == false && g_bCameraRoll_L )
			CameraRoll(30,LEFTROLL);

		if ( Camera.move == false )
		{
			Camera.vecLookatPt = g_nPlayerPos;
			Camera.vecEyePt.x = g_nPlayerPos.x + cos( Camera.angle * D3DX_PI/180 ) * Camera.distance;
			Camera.vecEyePt.y = g_nPlayerPos.y + Camera.distance;
			Camera.vecEyePt.z = g_nPlayerPos.z + sin( Camera.angle * D3DX_PI/180 ) * Camera.distance;
		}
	

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );
		
		break;

	case STATE_CLEAR:
		// カメラ（視点）位置
		Camera.vecEyePt = D3DXVECTOR3( 0.0f, 0.0f, -350.0f );		
		// 注視位置
		Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );
		break;


	case STATE_GAMEOVER:
		// カメラ（視点）位置
		Camera.vecEyePt = D3DXVECTOR3( 0.0f, 0.0f, -350.0f );		
		// 注視位置
		Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );
		break;

	case STATE_NOWLOADING:
		// カメラ（視点）位置
		Camera.vecEyePt = D3DXVECTOR3( 0.0f, 0.0f, -400.0f );		
		// 注視位置
		Camera.vecLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );

		// プロジェクショントランスフォーム(射影変換)
		D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(45), ASPECT_RATIO, NEAR_CLIP, FAR_CLIP);
		pDevice->SetTransform(D3DTS_PROJECTION, &matProj);
		// ビュートランスフォーム(視点座標変換)
		D3DXMatrixLookAtLH( &Camera.matView, &Camera.vecEyePt, &Camera.vecLookatPt, &Camera.vecUpVec );
		pDevice->SetTransform( D3DTS_VIEW, &Camera.matView );
		break;

	}
}

//-----------------------------------------------------------------------------------
// 回転
//-----------------------------------------------------------------------------------
void CameraRoll(int time , int roll)
{
	// 時間による角度量
	g_fCameraRollAngle = (float)MAX_CAMERAROLLANGLE/time;

	Camera.angle += g_fCameraRollAngle * roll;
	if (roll == LEFTROLL)
	{
		if ( Camera.angle < 0.0f )
			Camera.angle += 360.0f;
	}
	else if (roll == RIGHTROLL)
	{
		if ( Camera.angle >= 360.0f )
			Camera.angle -= 360.0f;
	}

	Camera.vecEyePt.x = g_nPlayerPos.x + cos( Camera.angle * D3DX_PI/180 ) * Camera.distance;
	Camera.vecEyePt.z = g_nPlayerPos.z + sin( Camera.angle * D3DX_PI/180 ) * Camera.distance;

	// カウントアップ
	g_nCameraRollCount++;

	// 終了判定
	if ( g_nCameraRollCount >= time )
	{
		g_bCameraRoll_L = false;
		g_bCameraRoll_R = false;
		g_fCameraRollAngle = 0.0f;
		g_nCameraRollCount = 0;
	}
}


//-----------------------------------------------------------------------------------
// カメラの移動
//-----------------------------------------------------------------------------------
void CameraMove ( int nTime )
{
	if ( Camera.move == false ) return;

	// 計算
	Camera.speed.x = GetCameraMoveVec().x / nTime;
	Camera.speed.z = GetCameraMoveVec().z / nTime;

	Camera.vecLookatPt += Camera.speed;
	Camera.vecEyePt += Camera.speed;

	// カウントアップ
	Camera.cameraMoveCount++;

	if ( Camera.cameraMoveCount >= nTime )
	{
		Camera.move = false;
		Camera.cameraMoveCount = 0;
		Camera.speed = D3DXVECTOR3(0.0f,0.0f,0.0f);
	}
}

//-----------------------------------------------------------------------------------
// 注視位置の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCameraLook(void)
{
	return Camera.vecLookatPt;
}

//-----------------------------------------------------------------------------------
// 位置の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCameraEyePt(void)
{
	return Camera.vecEyePt;
}

//-----------------------------------------------------------------------------------
// カメラの角度
//-----------------------------------------------------------------------------------
float GetCameraAngle(void)
{
	return Camera.angle;
}