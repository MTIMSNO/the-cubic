//===================================================================================
//
// メッシュクラス.cpp
//
//===================================================================================

#include "MeshClass.h"

//===================================================================================
// コンストラクタ
//===================================================================================
cMeshClass::cMeshClass()
{
	m_pD3DMesh = NULL;
	m_pD3DMaterials = NULL;
	m_pD3DTextures = NULL;
	m_dwNumMaterials = NULL;

	// 中心座標
	m_dPos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// 回転角度
	m_dRot = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// スケール
	m_dScale = D3DXVECTOR3(1.0f,1.0f,1.0f);
	// 角度
	m_nAngle = 0;
	// 速度
	m_fSpeed = 0.0f;
	// 表示
	m_bExist = true;
	// 移動
	m_bMove = true;
	// タイプ
	m_nType = STYPE_STAGE;
}

//===================================================================================
// デストラクタ
//===================================================================================
cMeshClass::~cMeshClass()
{
	this->Uninit();
}

//---------------------------------------------------------------------------------------
// メッシュ初期化
//---------------------------------------------------------------------------------------
HRESULT cMeshClass::Init(LPDIRECT3DDEVICE9 pDevice , TCHAR* xfile)
{
    LPD3DXBUFFER pD3DXMtrlBuffer = NULL;

	// Ｘファイルからメッシュデータを読み込む
	if (FAILED(D3DXLoadMeshFromX(xfile, D3DXMESH_SYSTEMMEM, pDevice, NULL,	// ここでＸファイルを指定
		&pD3DXMtrlBuffer, NULL, &m_dwNumMaterials, &m_pD3DMesh)))
	{
		MessageBox(NULL, _T("Xファイルの読み込みに失敗しました"), NULL, MB_OK);
		return E_FAIL;
	}

	// 法線が無い場合は強制的に追加
	if ((m_pD3DMesh->GetFVF() & D3DFVF_NORMAL) == 0)
	{
		LPD3DXMESH pMesh = m_pD3DMesh;
		pMesh->CloneMeshFVF(pMesh->GetOptions(), pMesh->GetFVF() | D3DFVF_NORMAL, pDevice, &m_pD3DMesh);
		SAFE_RELEASE(pMesh);
		D3DXComputeNormals(m_pD3DMesh, NULL);
	}

	// マテリアル保存用配列の確保
	D3DXMATERIAL* pD3DMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();
	// マテリアル情報バッファのポインタ取得
	m_pD3DMaterials = new D3DMATERIAL9[m_dwNumMaterials];
	m_pD3DTextures  = new LPDIRECT3DTEXTURE9[m_dwNumMaterials];
	for (DWORD i = 0; i < m_dwNumMaterials; i++)
	{ 
		m_pD3DMaterials[i] = pD3DMaterials[i].MatD3D;
		m_pD3DMaterials[i].Ambient = m_pD3DMaterials[i].Diffuse;
		m_pD3DTextures[i] = NULL;
		if (pD3DMaterials[i].pTextureFilename && pD3DMaterials[i].pTextureFilename[0])
		{
			// テクスチャファイルを読み込む
			if (FAILED(D3DXCreateTextureFromFileA(pDevice, pD3DMaterials[i].pTextureFilename, &m_pD3DTextures[i]))) {
				MessageBox(NULL, _T("テクスチャの読み込みに失敗しました"), NULL, MB_OK);
			}
		}
	}
	pD3DXMtrlBuffer->Release();

	return S_OK;
}

//---------------------------------------------------------------------------------------
// メッシュ解放
//---------------------------------------------------------------------------------------
void cMeshClass::Uninit(void)
{
	// テクスチャオブジェクトを解放
	for (DWORD i = 0; i < m_dwNumMaterials; i++)
	{
		SAFE_RELEASE(m_pD3DTextures[i]);
	}

	delete[] m_pD3DTextures;
	m_pD3DTextures = NULL;

	delete[] m_pD3DMaterials;

	SAFE_RELEASE(m_pD3DMesh);			// メッシュオブジェクトを解放
}

//---------------------------------------------------------------------------------------
// メッシュ描画
//---------------------------------------------------------------------------------------
void cMeshClass::Draw( LPDIRECT3DDEVICE9 pDevice )
{
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	// ワールドトランスフォーム（絶対座標変換）
	D3DXMATRIXA16 matWorld, matRX, matRY, matRZ, matTranslation, matScale;

	D3DXMatrixIdentity( &matWorld );
	D3DXMatrixScaling( &matScale, m_dScale.x, m_dScale.y, m_dScale.z );
	D3DXMatrixRotationY( &matRY, m_dRot.y );
	D3DXMatrixRotationX( &matRX, m_dRot.x );
	D3DXMatrixRotationZ( &matRZ, m_dRot.z );
	D3DXMatrixTranslation( &matTranslation, m_dPos.x, m_dPos.y, m_dPos.z );

	D3DXMatrixMultiply( &matWorld, &matWorld, &matScale );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matRY );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matRX );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matRZ );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matTranslation );
	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	// 描画
	for( DWORD i = 0; i < m_dwNumMaterials; i++ )
	{
		pDevice->SetMaterial(&m_pD3DMaterials[i]);
		pDevice->SetTexture(0, m_pD3DTextures[i]);	// テクスチャを設定
		m_pD3DMesh->DrawSubset(i);							// 描画を実行
	}
}

//---------------------------------------------------------------------------------------
// メッシュカラーの設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshColor(LPDIRECT3DDEVICE9 pDevice , DWORD num , D3DXCOLOR color)
{
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	m_pD3DMaterials[num].Diffuse = 
	m_pD3DMaterials[num].Ambient = color;

}

void cMeshClass::SetMeshColor(LPDIRECT3DDEVICE9 pDevice , D3DXCOLOR color)
{
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	for (DWORD i = 0; i < m_dwNumMaterials; i++)
	{ 
		m_pD3DMaterials[i].Diffuse = 
		m_pD3DMaterials[i].Ambient = color;
	}
}

//---------------------------------------------------------------------------------------
// 中心座標の取得
//---------------------------------------------------------------------------------------
D3DXVECTOR3 cMeshClass::GetMeshPos(void)
{
	return m_dPos;
}

//---------------------------------------------------------------------------------------
// 中心座標の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshPos(D3DXVECTOR3 pos)
{
	m_dPos = pos;
}

//---------------------------------------------------------------------------------------
// 中心座標の移動
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshPos(float z,float y,float x)
{
	m_dPos.z += z;	m_dPos.y += y;	m_dPos.x += x;
}

//---------------------------------------------------------------------------------------
// 回転角度の取得
//---------------------------------------------------------------------------------------
D3DXVECTOR3 cMeshClass::GetMeshRot(void)
{
	return m_dRot;
}

//---------------------------------------------------------------------------------------
// 回転角度の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshRot(D3DXVECTOR3 rot)
{
	m_dRot = rot;
}

//---------------------------------------------------------------------------------------
// 回転角度の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshRot(float z,float y,float x)
{
	m_dRot.z += z; m_dRot.y += y; m_dRot.x += x;
}

//---------------------------------------------------------------------------------------
// 拡大縮小の取得
//---------------------------------------------------------------------------------------
D3DXVECTOR3 cMeshClass::GetMeshScale(void)
{
	return m_dScale;
}

//---------------------------------------------------------------------------------------
// 拡大縮小の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshScale(D3DXVECTOR3 scale)
{
	m_dScale = scale;
}

//---------------------------------------------------------------------------------------
// 拡大縮小の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshScale(float z,float y,float x)
{
	m_dScale.x += x; m_dScale.y += y; m_dScale.z += z;
}

//---------------------------------------------------------------------------------------
// タイプの取得
//---------------------------------------------------------------------------------------
int cMeshClass::GetMeshType(void)
{
	return m_nType;
}

//---------------------------------------------------------------------------------------
// タイプの設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshType(int type)
{
	m_nType = type;
}

//---------------------------------------------------------------------------------------
// 角度の取得
//---------------------------------------------------------------------------------------
int cMeshClass::GetMeshAngle(void)
{
	return m_nAngle;
}

//---------------------------------------------------------------------------------------
// 角度の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshAngle(int angle)
{
	m_nAngle = angle;
}

//---------------------------------------------------------------------------------------
// 角度の設定
//---------------------------------------------------------------------------------------
void cMeshClass::AddMeshAngle(int angle)
{
	m_nAngle += angle;
}

//---------------------------------------------------------------------------------------
// 速度の取得
//---------------------------------------------------------------------------------------
float cMeshClass::GetMeshSpeed(void)
{
	return m_fSpeed;
}

//---------------------------------------------------------------------------------------
// 速度の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshSpeed(float speed)
{
	m_fSpeed += speed;
}

//---------------------------------------------------------------------------------------
// 表示表体の取得
//---------------------------------------------------------------------------------------
bool cMeshClass::GetMeshExist(void)
{
	return m_bExist;
}

//---------------------------------------------------------------------------------------
// 表示状態の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshExist(bool exist)
{
	m_bExist = exist;
}

//---------------------------------------------------------------------------------------
// 移動状態の取得
//---------------------------------------------------------------------------------------
bool cMeshClass::GetMeshMove(void)
{
	return m_bMove;
}

//---------------------------------------------------------------------------------------
// 移動状態の設定
//---------------------------------------------------------------------------------------
void cMeshClass::SetMeshMove(bool move)
{
	m_bMove = move;
}

