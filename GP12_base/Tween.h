//===================================================================================
// Tween.h
//===================================================================================

#include "main.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void InitTween(void);
void SetTween(float from,float to,int maxframe);
void SetTween(D3DXVECTOR2 from,D3DXVECTOR2 to,int maxframe);
bool isEnd(void);
float GetCurrentValue(void);
D3DXVECTOR2 GetCurrentVec2Value(void);
