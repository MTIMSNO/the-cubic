//===================================================================================
//
// フォグ.h
//
//===================================================================================

#include "Main.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
// フォグ有効
void FogOn(LPDIRECT3DDEVICE9 pDevice,float Start , float End);
// フォグ無効
void FogOff(LPDIRECT3DDEVICE9 pDevice);


//=============================================================================
// フォグの有効
//=============================================================================
void FogOn(LPDIRECT3DDEVICE9 pDevice,float Start , float End)
{
	D3DCAPS9 caps;
	ZeroMemory( &caps , sizeof(D3DCAPS9));
	pDevice->GetDeviceCaps( &caps);

	// フォグ設定
	float StartPos		= Start;
	float EndPos		= End;
	float FogDensity	= 0.1f;

	// フォグ有効
	pDevice->SetRenderState( D3DRS_FOGENABLE , true);
	// フォグの色
	pDevice->SetRenderState( D3DRS_FOGCOLOR , D3DXCOLOR( 0x00 , 0x00 , 0x00 , 0x00 ));
	// フォグ密度
	pDevice->SetRenderState( D3DRS_FOGDENSITY , *(DWORD*)(&FogDensity));
	// テーブルモード
	pDevice->SetRenderState( D3DRS_FOGTABLEMODE , D3DFOG_LINEAR);
	// 開始位置
	pDevice->SetRenderState( D3DRS_FOGSTART , *(DWORD*)(&StartPos));
	// 終了処理
	pDevice->SetRenderState( D3DRS_FOGEND , *(DWORD*)(&EndPos));
}

//=============================================================================
// フォグの無効
//=============================================================================
void FogOff(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState( D3DRS_FOGENABLE , false );
}