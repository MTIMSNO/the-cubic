//===================================================================================
//
// メッシュステージ
//
//===================================================================================

#include "MeshStage03.h"
#include "Particle.h"
#include "Beacon.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// ステージ配置
void SetInitialStage03(void);
// ステージのタイプ設定
void SetStageType03(int z,int y,int x);
// ステージカラーの設定
void SetStageColor03(LPDIRECT3DDEVICE9 pDevice);
// ステージの床のカラー設定
void DirectionStageColor03(LPDIRECT3DDEVICE9 pDevice);
// 進む方向を調べる
tStageData SearchDirection03(tStageData Player,tStageData Direct);
// 角度判定
void AngleJudgement03(void);
// プレイヤーの切り替え
void ChangePlayer03(void);
// プレイヤーの移動
void MovePlayer03(void);
// オブジェクトカラーの設定
void SetStageColor03(LPDIRECT3DDEVICE9 pDevice);
// その列にあるホールの数
int GetHoleCount03(int x, int y, int z);
// 配列更新
void UpdateArray03(tStageData tArray[],int nArray,tStageData tDecision);
// 表示・非表示設定
void SetExist03(tStageData tPlayer,tStageData tDicision);
// ワープ使用時の表示・非表示設定
void SetWarpExist03(tStageData tP,tStageData tD);
// 終了判定
bool ExitStage03(void);


//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

// ポインタ作成
cMeshStage03*				pMeshStage03;

// 方向
tStageData					Direction03[4] =
							{
								{0,1,1},
								{-1,1,0},
								{0,1,-1},
								{1,1,0}
							};

//-----------------------------------------------------------------------------------
// ステージの配置
//-----------------------------------------------------------------------------------
void SetInitialStage03(void)
{
	if (GetData(DATA_SELECTSTAGE) == STAGE_ONE)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER03+1] = {
			{2,STAGE03_Y-1,8},
			{2,STAGE03_Y-1,5},
			{5,STAGE03_Y-1,2},
			{STAGE03_X/2,STAGE03_Y/2,STAGE03_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER03+1; i++ )
			pMeshStage03->Player03[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE03] = {
			{5,STAGE03_Y-2,5},
			{8,STAGE03_Y-2,5},
			{5,STAGE03_Y-2,7},
		};

		for ( int i = 0; i < MAX_HOLE03; i++ )
			pMeshStage03->Hole03[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK03] = {
			{8,STAGE03_Y-1,4},
			{2,STAGE03_Y-1,2},
		};

		for ( int i = 0; i < MAX_BLOCK03; i++ )
			pMeshStage03->Block03[i] = B[i];

		// ワープ
		tStageData W[MAX_WARP03] = {
			{8,STAGE03_Y-2,3},
			{8,STAGE03_Y-2,7},
		};

		for ( int i = 0; i < MAX_WARP03; i++ )
			pMeshStage03->Warp03[i] = W[i];
	}
	else if (GetData(DATA_SELECTSTAGE) == STAGE_TWO)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER03+1] = {
			{3,STAGE03_Y-1,3},
			{7,STAGE03_Y-1,4},
			{7,STAGE03_Y-1,7},
			{STAGE03_X/2,STAGE03_Y/2,STAGE03_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER03+1; i++ )
			pMeshStage03->Player03[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE03] = {
			{2,STAGE03_Y-2,2},
			{2,STAGE03_Y-3,2},
			{2,STAGE03_Y-4,2},
		};

		for ( int i = 0; i < MAX_HOLE03; i++ )
			pMeshStage03->Hole03[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK03] = {
			{4,STAGE03_Y-1,2},
			{3,STAGE03_Y-1,7},
		};

		for ( int i = 0; i < MAX_BLOCK03; i++ )
			pMeshStage03->Block03[i] = B[i];

		// ワープ
		tStageData W[MAX_WARP03] = {
			{8,STAGE03_Y-2,3},
			{2,STAGE03_Y-2,7},
		};

		for ( int i = 0; i < MAX_WARP03; i++ )
			pMeshStage03->Warp03[i] = W[i];
	}
	else if (GetData(DATA_SELECTSTAGE) == STAGE_THREE)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER03+1] = {
			{2,STAGE03_Y-1,5},
			{4,STAGE03_Y-1,8},
			{7,STAGE03_Y-1,8},
			{STAGE03_X/2,STAGE03_Y/2,STAGE03_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER03+1; i++ )
			pMeshStage03->Player03[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE03] = {
			{8,STAGE03_Y-2,5},
			{6,STAGE03_Y-2,5},
			{6,STAGE03_Y-2,7},
		};

		for ( int i = 0; i < MAX_HOLE03; i++ )
			pMeshStage03->Hole03[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK03] = {
			{5,STAGE03_Y-1,2},
			{8,STAGE03_Y-1,7},
		};

		for ( int i = 0; i < MAX_BLOCK03; i++ )
			pMeshStage03->Block03[i] = B[i];

		// ワープ
		tStageData W[MAX_WARP03] = {
			{8,STAGE03_Y-2,2},
			{2,STAGE03_Y-2,8},
		};

		for ( int i = 0; i < MAX_WARP03; i++ )
			pMeshStage03->Warp03[i] = W[i];

	}
}

//-----------------------------------------------------------------------------------
// ステージのタイプ設定
//-----------------------------------------------------------------------------------
void SetStageType03(int z,int y,int x)
{

	// 外枠
	// Z+.Z-.X+.X-.Y-.Y+方向に配列１つ分づつ表示しない
	if ( y == 0 || y == STAGE03_Y-1 || x == 0 || x == STAGE03_X-1 || z == 0 || z == STAGE03_Z-1 )
	{	
		pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_NONE);
		pMeshStage03->mStage03[z][y][x]->SetMeshExist(false);
	}

	// プレイヤーの設定
	for ( int i = 0; i < MAX_PLAYER03+1; i++ )
	{
		if ( pMeshStage03->Player03[i].x == x && pMeshStage03->Player03[i].y == y && pMeshStage03->Player03[i].z == z )
		{
			pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_PLAYER);
			pMeshStage03->mStage03[z][y][x]->SetMeshExist(true);
			if ( i == MAX_PLAYER03 )
			{
				pMeshStage03->mStage03[z][y][x]->SetMeshMove(false);
				pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_NONE);
			}
		}
	}
	

	// ホールの設定
	for ( int i = 0; i < MAX_HOLE03; i++ )
	{
		if (pMeshStage03->Hole03[i].x == x && pMeshStage03->Hole03[i].y == y && pMeshStage03->Hole03[i].z == z )
		{
			pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_HOLE);
			pMeshStage03->mStage03[z][y][x]->SetMeshExist(true);
		}
	}

	// 各ギミックがONのとき
	// ブロック
	if ( pMeshStage03->GetBlockGimmick())
	{
		for ( int i = 0; i < MAX_BLOCK03; i++ )
		{
			if ( pMeshStage03->Block03[i].x == x && pMeshStage03->Block03[i].y == y && pMeshStage03->Block03[i].z == z )
			{
				pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_BLOCK);
				pMeshStage03->mStage03[z][y][x]->SetMeshExist(true);
			}
		}
	}
	// ワープ
	if ( pMeshStage03->GetWarpGimmick())
	{
		for ( int i = 0; i < MAX_WARP03; i++ )
		{
			if ( pMeshStage03->Warp03[i].x == x && pMeshStage03->Warp03[i].y == y && pMeshStage03->Warp03[i].z == z )
			{
				pMeshStage03->mStage03[z][y][x]->SetMeshType(STYPE_WARP);
				pMeshStage03->mStage03[z][y][x]->SetMeshExist(true);				
			}
		}
	}
}

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitMeshStage03 ( LPDIRECT3DDEVICE9 pDevice )
{
	// 動的オブジェクトの作成
	pMeshStage03 = new cMeshStage03();

	// ギミックの設定
	pMeshStage03->SetBlockGimmick(true);
	pMeshStage03->SetWarpGimmick(true);

	// ステージの配置設定
	SetInitialStage03();

	for ( int z = 0; z < STAGE03_Z; z++ )
		for ( int y = 0; y < STAGE03_Y; y++ )
			for ( int x = 0; x < STAGE03_X; x++ )
			{
				// 動的オブジェクトの作成
				pMeshStage03->mStage03[z][y][x] = new cMeshClass();

				// 位置の設定
				pMeshStage03->mStage03[z][y][x]->SetMeshPos(D3DXVECTOR3(
					(float)(-CUBESIZE*(STAGE03_Z/2))+CUBESIZE*z,
					(float)(-CUBESIZE*(STAGE03_Y/2))+CUBESIZE*y,
					(float)(-CUBESIZE*(STAGE03_X/2))+CUBESIZE*x)
					);

				// オブジェクトのタイプ設定
				SetStageType03(z,y,x);

				pMeshStage03->mStage03[z][y][x]->Init(pDevice,MESH_CUBE);

			}

	return S_OK;
}
//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitMeshStage03(void)
{
	for ( int z = 0; z < STAGE03_Z; z++ )
	for ( int y = 0; y < STAGE03_Y; y++ )
	for ( int x = 0; x < STAGE03_X; x++ )
	{
		delete pMeshStage03->mStage03[z][y][x];
	}

	delete pMeshStage03;
}
//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateMeshStage03(void)
{
	// 選択中のプレイヤー配列を取得
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	
	// 終了判定
	if ( !(ExitStage03()) && pMeshStage03->GetFade() == false )
	{
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		pMeshStage03->SetFade(true);
	}
	else if ( pMeshStage03->GetFade() && IsCheckFade() == FADE_OFF )
	{
		SetFade(0.0f,0.0f,0.0f,0.0f,15);
		SetData(DATA_MAINSTATE,pMeshStage03->Transition(pMeshStage03->Player03,pMeshStage03->Hole03,MAX_PLAYER03,MAX_HOLE03));
		pMeshStage03->SetFade(false);
	}

	// 角度設定
	if ( pMeshStage03->GetPlayerChange() == true )
		pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshAngle(
			pMeshStage03->SetPlayerAngle(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle()));
	
	// 角度判定
	if ( pMeshStage03->GetPlayerChange() == true )
		AngleJudgement03();

	// プレイヤーの切り替え
	if ( pMeshStage03->GetPlayerChange() == true )
	if( GetKeyboardTrigger(DIK_SPACE) || GetJoyStickTrigger(1) )
		ChangePlayer03();

	// プレイヤーの移動
	if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() == true )
	if ( GetKeyboardTrigger(DIK_RETURN) || GetJoyStickTrigger(2))
	{
		// 移動を可能に
		pMeshStage03->SetPlayerMove(true);
		// プレイヤーの切り替えを無効に
		pMeshStage03->SetPlayerChange(false);
	}
	MovePlayer03();

}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawMeshStage03(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int z = 0; z < STAGE03_Z; z++ )
	for ( int y = 0; y < STAGE03_Y; y++ )
	for ( int x = 0; x < STAGE03_X; x++ )
	{
		// 表示しない
		if ( pMeshStage03->mStage03[z][y][x]->GetMeshExist() == false ) continue;

		// ビーコンの設置
		if ( pMeshStage03->mStage03[z][y][x]->GetMeshType() == STYPE_HOLE )
			SetBeacon( pDevice,pMeshStage03->mStage03[z][y][x]->GetMeshPos(),GetHoleCount03(x,y,z),1);
		// 色の初期化処理
		pMeshStage03->mStage03[z][y][x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
		// ステージカラーの設定
		SetStageColor03(pDevice);
		// 進む方向の床のカラー設定
		DirectionStageColor03(pDevice);
		
		pMeshStage03->mStage03[z][y][x]->Draw(pDevice);
	}
}

//-----------------------------------------------------------------------------------
// プレイヤーの中心座標の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetPlayerPos03(void)
{
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	return pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos();
}

//-----------------------------------------------------------------------------------
// プレイヤー角度の取得
//-----------------------------------------------------------------------------------
int GetPlayerAngle03()
{
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	return pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle();
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替えの取得
//-----------------------------------------------------------------------------------
bool GetPlayerChange03(void)
{
	return pMeshStage03->GetPlayerChange();
}

//-----------------------------------------------------------------------------------
// 進む方向の色の設定
//-----------------------------------------------------------------------------------
void DirectionStageColor03(LPDIRECT3DDEVICE9 pDevice)
{
	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	// 角度の取得
	int nAngle = pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle();

	// ステージカラーの設定
	switch ( nAngle / 90 )
	{
	case 0:
		for ( int i = P.z; i <= pMeshStage03->GetDecision().z; i++ )
			if ( P.y != 0 || pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage03->mStage03[i][P.y-1][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 1:
		for ( int i = P.x; i >= pMeshStage03->GetDecision().x; i-- )
			if ( P.y != 0 || pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage03->mStage03[P.z][P.y-1][i]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 2:
		for ( int i = P.z; i >= pMeshStage03->GetDecision().z; i-- )
			if ( P.y != 0 || pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage03->mStage03[i][P.y-1][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 3:
		for ( int i = P.x; i <= pMeshStage03->GetDecision().x; i++ )
			if ( P.y != 0 || pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage03->mStage03[P.z][P.y-1][i]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;
	}
}

//-----------------------------------------------------------------------------------
// 角度判定
//-----------------------------------------------------------------------------------
void AngleJudgement03(void)
{
	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	// 角度代入
	int nAngle = pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle();
	// 移動先代入先の初期化
	tStageData D = {0,0,0};
	pMeshStage03->SetDecision(D);

	// 角度によってその方向を調べる
	switch ( nAngle / 90 )
	{
	case 0:
		P.z = P.z + 1;
		pMeshStage03->SetDecision(SearchDirection03(P,Direction03[DIR_Z_UP]));
		break;

	case 1:
		P.x = P.x - 1;
		pMeshStage03->SetDecision(SearchDirection03(P,Direction03[DIR_X_DOWN]));
		break;

	case 2:
		P.z = P.z - 1;
		pMeshStage03->SetDecision(SearchDirection03(P,Direction03[DIR_Z_DOWN]));
		break;

	case 3:
		P.x = P.x + 1;
		pMeshStage03->SetDecision(SearchDirection03(P,Direction03[DIR_X_UP]));
		break;
	}
}

//-----------------------------------------------------------------------------------
// 進む方向を調べる
// 一つ一つ見ていき条件に当てはまったらreturn
//-----------------------------------------------------------------------------------
tStageData SearchDirection03(tStageData Player,tStageData Direct)
{
	// 今見ている位置
	// カレントPos
	tStageData cP = Player;
	// 下から見るので一番下に設定
	cP.y = 1;

	while ( cP.z > 0 && cP.z < STAGE03_Z-1 && 
			cP.y > 0 && cP.y < STAGE03_Y-1 &&
			cP.x > 0 && cP.x < STAGE03_X-1 )
	{
		// 下から上に
		cP.y += Direct.y;

		//
		// ほかの要素をいかに記入
		//

		// 選択外プレイヤー or ブロック があるか
		if ( pMeshStage03->mStage03[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_PLAYER ||
			 pMeshStage03->mStage03[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_BLOCK )
		{
			pMeshStage03->SetWarpUseFlag(false);
			cP.z -= Direct.z;
			cP.x -= Direct.x;
			return cP;
		}
		// ワープはあるか
		else if ( pMeshStage03->mStage03[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_WARP )
		{
			for ( int i = 0; i < MAX_WARP03; i++ )
			{
				if ( pMeshStage03->Warp03[i].x == cP.x && pMeshStage03->Warp03[i].y == cP.y && pMeshStage03->Warp03[i].z == cP.z )		pMeshStage03->SetWarpNumber(i);
			}

			tStageData t = pMeshStage03->Warp03[!(pMeshStage03->GetWarpNumber())];
			if ( pMeshStage03->mStage03[t.z][t.y+1][t.x]->GetMeshType() == STYPE_PLAYER )	continue;

			pMeshStage03->SetWarpUseFlag(true);
			cP.y = cP.y + 1;
			return cP;
		}
		// 穴があるか
		else if ( pMeshStage03->mStage03[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_HOLE )
		{
			pMeshStage03->SetWarpUseFlag(false);
			return cP;
		}

		// 一番上まで見たら見ている方向に一つ進める
		if ( cP.y >= STAGE03_Y-1 )
		{
			cP.x += Direct.x;
			cP.y = 1;
			cP.z += Direct.z;
		}
	}

	// ステージ外まで見た場合
	cP.y = 0;
	return cP;
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替え
//-----------------------------------------------------------------------------------
void ChangePlayer03(void)
{
	// 押したタイミングのプレイヤーナンバーを保持
	pMeshStage03->SetBeforePlayerNum(pMeshStage03->GetPlayerNum());

	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	// 何か所確認したかカウント
	int nCount = 0;

	// プレイヤーナンバーの増加
	do
	{
		// 増加
		pMeshStage03->SetPlayerNum(pMeshStage03->GetPlayerNum()+1);
		nCount++;

		// プレイヤー全てが穴に入っていた場合
		if ( nCount > MAX_PLAYER03 )
		{
			P = pMeshStage03->Player03[MAX_PLAYER03];
			break;
		}

		// MAXまで見たら0へ戻す
		if ( pMeshStage03->GetPlayerNum() >= MAX_PLAYER03 ) pMeshStage03->SetPlayerNum(0);

		// 現プレイヤーナンバーの配列を代入
		P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	}
	// 穴に入っているか(移動が可能かどうか)
	while ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() == false );
	
}

//-----------------------------------------------------------------------------------
// 配列の更新
// Playerの配列を移動後に変更する
//-----------------------------------------------------------------------------------
void UpdateArray03(tStageData tArray[],int nArray,tStageData tDecision)
{
	// 配列更新
	tArray[nArray] = tDecision;

	// 位置を整列
	for ( int z = 0; z < STAGE03_Z; z++ )
	for ( int y = 0; y < STAGE03_Y; y++ )
	for ( int x = 0; x < STAGE03_X; x++ )
	{
		// 位置の設定
		pMeshStage03->mStage03[z][y][x]->SetMeshPos(D3DXVECTOR3(
			(float)(-CUBESIZE*(STAGE03_Z/2))+CUBESIZE*z,
			(float)(-CUBESIZE*(STAGE03_Y/2))+CUBESIZE*y,
			(float)(-CUBESIZE*(STAGE03_X/2))+CUBESIZE*x)
			);
		// 回転角度の設定
		pMeshStage03->mStage03[z][y][x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
		if ( !(pMeshStage03->GetWarpGimmick())) 
		// スケールの設定
		pMeshStage03->mStage03[z][y][x]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));
	}
}

//-----------------------------------------------------------------------------------
// 色の設定
//-----------------------------------------------------------------------------------
void SetStageColor03(LPDIRECT3DDEVICE9 pDevice)
{
	// ホールの色の設定
	for ( int cH = 0; cH < MAX_HOLE03; cH++ )
	{
		// ホールの配列代入
		tStageData H = pMeshStage03->Hole03[cH];
		pMeshStage03->mStage03[H.z][H.y][H.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.0f,0.0f,0.0f,0.85f));
	}

	// ブロックの色の設定
	if ( pMeshStage03->GetBlockGimmick() )
	{
		for ( int cB = 0; cB < MAX_BLOCK03; cB++ )
		{
			// ブロックの配列代入
			tStageData B = pMeshStage03->Block03[cB];
			pMeshStage03->mStage03[B.z][B.y][B.x]->SetMeshColor(pDevice,0,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
			pMeshStage03->mStage03[B.z][B.y][B.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f));
		}
	}

	// ワープの色の設定
	if ( pMeshStage03->GetWarpGimmick() )
	{
		for ( int cW = 0; cW < MAX_WARP03; cW++ )
		{
			// ワープの配列代入
			tStageData W = pMeshStage03->Warp03[cW];
			pMeshStage03->mStage03[W.z][W.y][W.x]->SetMeshColor(pDevice,D3DXCOLOR(0.0f,0.0f,0.5f,1.0f));
		}
	}

	// プレイヤーの色の設定
	for ( int cP = 0; cP < MAX_PLAYER03; cP++ )
	{
		// プレイヤーの配列代入
		tStageData P = pMeshStage03->Player03[cP];
		// 選択プレイヤー
		if ( cP == pMeshStage03->GetPlayerNum() )
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,0.2f,0.2f,1.0f));
		// 移動不可プレイヤー(穴に入っているプレイヤー)
		else if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshMove() == false )
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
		// 選択外プレイヤー
		else 
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,0.6f,0.6f,1.0f));
	}	
}

//-----------------------------------------------------------------------------------
// ホールが何個連なっているか
//-----------------------------------------------------------------------------------
int GetHoleCount03(int x, int y, int z)
{
	int HoleCount = 0;

	for ( int cH = 0; cH < STAGE03_Y; cH++ )
	{
		if ( pMeshStage03->mStage03[z][cH][x]->GetMeshType() == STYPE_HOLE ) HoleCount++;
		else HoleCount;
	}
	return HoleCount;
}

//-----------------------------------------------------------------------------------
// 終了判定
//-----------------------------------------------------------------------------------
bool ExitStage03(void)
{
	// 一つでもステージ外にいた場合ゲーム終了
	// cP...カレントプレイヤー
	for ( int cP = 0; cP < MAX_PLAYER03; cP++ )
	{
		tStageData P = pMeshStage03->Player03[cP];
		if ( P.y == 0 )		return false;
	}

	// ステージタイプがプレイヤーであるブロックが
	// 一つでも残っていたらゲーム続行
	for ( int z = 0; z < STAGE03_Z; z++ )
	for ( int y = 0; y < STAGE03_Y; y++ )
	for ( int x = 0; x < STAGE03_X; x++ )
	{
		if ( y == STAGE03_Y-1 )
			if ( pMeshStage03->mStage03[z][y][x]->GetMeshType() == STYPE_PLAYER )		return true;
	}

	// プレイヤーが一つもいなければゲーム終了
	return false;
}

//-----------------------------------------------------------------------------------
// プレイヤーの表示・非表示設定
//-----------------------------------------------------------------------------------
void SetExist03(tStageData tPlayer,tStageData tDicision)
{
	// 配列更新
	UpdateArray03(pMeshStage03->Player03,pMeshStage03->GetPlayerNum(),pMeshStage03->GetDecision());

	// 移動前の位置を非表示
	pMeshStage03->mStage03[tPlayer.z][tPlayer.y][tPlayer.x]->SetMeshType(STYPE_NONE);
	pMeshStage03->mStage03[tPlayer.z][tPlayer.y][tPlayer.x]->SetMeshExist(false);

	// 移動後を表示

	// 移動後の位置がステージ一番上の場合
	// ブロックにぶつかったときetc...
	if ( tDicision.y == STAGE03_Y-1 )
	{
		pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_PLAYER);
		pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshExist(true);
	}
	// 移動後の位置が 穴 or ステージ外の場合
	else
	{
		// 移動できるかどうか(穴に入っているか)
		// 移動不能に設定
		pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshMove(false);

		// ステージ外のとき
		if ( tDicision.y == 0 )
		{
			pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_NONE);
			pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshExist(false);
		}
		// 穴に入るとき
		else
		{
			SetParticle(pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->GetMeshPos(),50,GetHoleCount03(tDicision.x,tDicision.y,tDicision.z));
			pMeshStage03->mStage03[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_STAGE);
		}

		// 次のプレイヤーに切り替え
		ChangePlayer03();
	}

	// この移動処理に入るか
	pMeshStage03->SetPlayerMove(false);
	// プレイヤーの切り替え
	pMeshStage03->SetPlayerChange(true);
}

//-----------------------------------------------------------------------------------
// ワープ使用時のプレイヤーの表示・非表示etc...
//-----------------------------------------------------------------------------------
void SetWarpExist03(tStageData tP,tStageData tD)
{
	if (pMeshStage03->GetWarpUseFlag() == false)		return; 

	// 配列更新
	UpdateArray03(pMeshStage03->Player03,pMeshStage03->GetPlayerNum(),pMeshStage03->GetDecision());

	// 移動前の位置を非表示
	pMeshStage03->mStage03[tP.z][tP.y][tP.x]->SetMeshType(STYPE_NONE);
	pMeshStage03->mStage03[tP.z][tP.y][tP.x]->SetMeshExist(false);

	// 移動後の位置を表示
	pMeshStage03->mStage03[tD.z][tD.y][tD.x]->SetMeshType(STYPE_PLAYER);
	pMeshStage03->mStage03[tD.z][tD.y][tD.x]->SetMeshExist(true);	

	static tStageData W;	

	// だんだん小さく・だんだん大きく
	if (pMeshStage03->mStage03[tD.z][tD.y][tD.x]->GetMeshScale().x >= 0.0f &&
		pMeshStage03->mStage03[tD.z][tD.y][tD.x]->GetMeshScale().y >= 0.0f &&
		pMeshStage03->mStage03[tD.z][tD.y][tD.x]->GetMeshScale().z >= 0.0f )
	{
		pMeshStage03->mStage03[tD.z][tD.y][tD.x]->SetMeshScale(-0.025f,-0.025f,-0.025f);
		D3DXVECTOR3 Scale = pMeshStage03->mStage03[tD.z][tD.y][tD.x]->GetMeshScale();
		// 入った方と違う方のワープの位置を代入
		W = pMeshStage03->Warp03[!(pMeshStage03->GetWarpNumber())];
		// 位置修正
		W.y = W.y + 1;
		// ワープ先を表示
		pMeshStage03->mStage03[W.z][W.y][W.x]->SetMeshType(STYPE_PLAYER);
		pMeshStage03->mStage03[W.z][W.y][W.x]->SetMeshExist(true);

		pMeshStage03->mStage03[W.z][W.y][W.x]->SetMeshScale(D3DXVECTOR3(1.0f-Scale.x,1.0f-Scale.y,1.0f-Scale.z));
	}
	else 
	{
		// 移動後の位置を非表示
		pMeshStage03->mStage03[tD.z][tD.y][tD.x]->SetMeshType(STYPE_NONE);
		pMeshStage03->mStage03[tD.z][tD.y][tD.x]->SetMeshExist(false);

		// 大きさの修正
		pMeshStage03->mStage03[W.z][W.y][W.x]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));

		// 配列の更新
		UpdateArray03(pMeshStage03->Player03,pMeshStage03->GetPlayerNum(),W);

		// 位置を整列
		for ( int z = 0; z < STAGE03_Z; z++ )
		for ( int y = 0; y < STAGE03_Y; y++ )
		for ( int x = 0; x < STAGE03_X; x++ )
		{
			pMeshStage03->mStage03[z][y][x]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));
		}

		// この移動処理に入るか
		pMeshStage03->SetPlayerMove(false);
		// プレイヤーの切り替え
		pMeshStage03->SetPlayerChange(true);
	}
}

//-----------------------------------------------------------------------------------
// カメラの移動量の計算
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCameraMoveVol03(void)
{
	// 現在のプレイヤー位置
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	// 一つ前のプレイヤーの位置
	tStageData BeP = pMeshStage03->Player03[pMeshStage03->GetBeforePlayerNum()];

	// 計算
	D3DXVECTOR3 dMoveVol = 
		D3DXVECTOR3(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().x - pMeshStage03->mStage03[BeP.z][BeP.y][BeP.x]->GetMeshPos().x,
					pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().y - pMeshStage03->mStage03[BeP.z][BeP.y][BeP.x]->GetMeshPos().y,
					pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().z - pMeshStage03->mStage03[BeP.z][BeP.y][BeP.x]->GetMeshPos().z);

	return dMoveVol;
}

//-----------------------------------------------------------------------------------
// プレイヤー移動
//-----------------------------------------------------------------------------------
void MovePlayer03(void)
{
	// 移動できなければreturn 
	if ( pMeshStage03->GetPlayerMove() != true ) return;

	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage03->Player03[pMeshStage03->GetPlayerNum()];
	// 移動先の位置を代入
	tStageData D = pMeshStage03->GetDecision();
	// 選択プレイヤーの角度取得
	int nAngle = pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle();

	// 代入用変数の初期化
	// cPlayerPos...カレントプレイヤーポス
	// DecisionPos...移動先
	// Distance...距離
	D3DXVECTOR3 cPlayerPos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	D3DXVECTOR3 DecisionPos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	D3DXVECTOR3 Distance = D3DXVECTOR3(0.0f,0.0f,0.0f);

	// 各変数に座標を代入
	cPlayerPos = pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos();
	DecisionPos = pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos();
	// 距離の計算
	Distance = cPlayerPos - DecisionPos;

	
	// 移動
	switch ( nAngle / 90 )
	{
	case 0:
		// 横移動
		if ( Distance.x <= 0.0f )
		{
			// プレイヤー移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,0,CUBESPEED);
			// 回転
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(-CUBEROLL,0,0);
			// 距離の減少
			Distance.x += CUBESPEED;
			// 位置・回転修正
			if ( Distance.x >= 0.0f )
			{
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().z));
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			}
		}
		// 縦移動
		if ( Distance.x >= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage03->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.x >= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos() == pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage03->mStage03[D.z][D.y][D.x]->SetMeshAngle(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage03->GetWarpUseFlag() )
				SetWarpExist03(P,D);
			else if ( pMeshStage03->GetWarpUseFlag() == false )
				SetExist03(P,D);
		}
		break;

	case 1:
		// 横移動
		if ( Distance.z >= 0.0f )
		{
			// プレイヤー移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(-CUBESPEED,0,0);
			// 回転
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(0,0,CUBEROLL);
			// 距離の減少
			Distance.z -= CUBESPEED;
			// 位置・回転修正
			if ( Distance.z <= 0.0f )
			{
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().z));
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			
			}
		}
		// 縦移動
		if ( Distance.z <= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage03->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.z <= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos() == pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos() )		
		{
			// 保持角度の受け渡し
			pMeshStage03->mStage03[D.z][D.y][D.x]->SetMeshAngle(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage03->GetWarpUseFlag() )
				SetWarpExist03(P,D);
			else if ( pMeshStage03->GetWarpUseFlag() == false )
				SetExist03(P,D);
		}
		break;

	case 2:
		// 横移動
		if ( Distance.x >= 0.0f )
		{
			// プレイヤー移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,0,-CUBESPEED);
			// 回転
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(CUBEROLL,0,0);
			// 距離の減少
			Distance.x -= CUBESPEED;
			// 位置・回転修正
			if ( Distance.x <= 0.0f )
			{
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().z));
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			
			}
		}
		// 縦移動
		if ( Distance.x <= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage03->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.x <= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos() == pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage03->mStage03[D.z][D.y][D.x]->SetMeshAngle(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage03->GetWarpUseFlag() )
				SetWarpExist03(P,D);
			else if ( pMeshStage03->GetWarpUseFlag() == false )
				SetExist03(P,D);
		}
		break;

	case 3:
		// 横移動
		if ( Distance.z <= 0.0f )
		{
			// プレイヤー移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(CUBESPEED,0,0);
			// 回転
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(0,0,CUBEROLL);
			// 距離の増加
			Distance.z += CUBESPEED;
			// 位置・回転修正
			if ( Distance.z >= 0.0f )
			{
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().z));
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
		
			}
		}
		// 縦移動
		if ( Distance.z >= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage03->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage03->mStage03[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.z >= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshPos() == pMeshStage03->mStage03[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage03->mStage03[D.z][D.y][D.x]->SetMeshAngle(pMeshStage03->mStage03[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage03->GetWarpUseFlag() )
				SetWarpExist03(P,D);
			else if ( pMeshStage03->GetWarpUseFlag() == false )
				SetExist03(P,D);
		}
		break;
	}
}