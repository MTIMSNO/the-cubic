//===================================================================================
//
// メッシュクラス.h
//
//===================================================================================

#pragma once

#include "Main.h"

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cMeshClass
{
private:
	// メッシュオブジェクト
	LPD3DXMESH					m_pD3DMesh;	
	// マテリアルオブジェクト
	D3DMATERIAL9*				m_pD3DMaterials;	
	// テクスチャオブジェクト
	LPDIRECT3DTEXTURE9*			m_pD3DTextures;	
	// マテリアル数
	DWORD						m_dwNumMaterials;	
	// 中心座標
	D3DXVECTOR3					m_dPos;	
	// 回転
	D3DXVECTOR3					m_dRot;			
	// 拡大縮小(1.0fベース)
	D3DXVECTOR3					m_dScale;	
	// タイプ
	int							m_nType;
	// 角度
	int							m_nAngle;
	// 速度
	float						m_fSpeed;
	// 表示
	bool						m_bExist;
	// 移動
	bool						m_bMove;

public:
	// コンストラクタ
	cMeshClass();
	// デストラクタ
	~cMeshClass();
	// メッシュの初期化
	HRESULT Init(LPDIRECT3DDEVICE9 pDevice , TCHAR* xfile);
	// メッシュの終了処理
	void Uninit(void);
	// メッシュの描画処理
	void Draw(LPDIRECT3DDEVICE9 pDevice);

public:
	// 中心座標の取得
	D3DXVECTOR3 GetMeshPos(void);
	// 中心座標の設定
	void SetMeshPos(D3DXVECTOR3 pos);
	// 中心座標の移動
	void SetMeshPos(float z,float y,float x);
	// 回転角度の取得
	D3DXVECTOR3 GetMeshRot(void);
	// 回転角度の設定
	void SetMeshRot(D3DXVECTOR3 rot);
	// 回転の設定
	void SetMeshRot(float z,float y,float x);
	// 拡大縮小の取得
	D3DXVECTOR3 GetMeshScale(void);
	// 拡大縮小の設定
	void SetMeshScale(D3DXVECTOR3 scale);
	// 拡大縮小の設定
	void SetMeshScale(float z,float y,float x);
	// タイプの取得
	int GetMeshType(void);
	// タイプの設定
	void SetMeshType(int type);
	// 角度の取得
	int GetMeshAngle(void);
	// 角度の設定
	void SetMeshAngle(int angle);
	// 角度の設定
	void AddMeshAngle(int angle);
	// 速度の取得
	float GetMeshSpeed(void);
	// 速度の設定
	void SetMeshSpeed(float speed);
	// 表示状態の取得
	bool GetMeshExist(void);
	// 表示状態の設定
	void SetMeshExist(bool exist);
	// 移動状態の取得
	bool GetMeshMove(void);
	// 移動状態の設定
	void SetMeshMove(bool move);
	// メッシュカラーの設定
	void SetMeshColor(LPDIRECT3DDEVICE9 pDevice , DWORD num , D3DXCOLOR color);
	void SetMeshColor(LPDIRECT3DDEVICE9 pDevice , D3DXCOLOR color);
};

