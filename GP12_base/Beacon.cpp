//=============================================================================
//
// ポリゴン処理 [polygon.cpp]
//
//=============================================================================


#include "Beacon.h"
#include "Input.h"
#include "PolygonBase.h"
#include "PolygonAnim.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************

cBeacon*			p_Beacon;

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitBeacon(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	p_Beacon = new cBeacon();

	for ( int i = 0; i < MAX_BEACON; i++ )
	{
		// オブジェクトの作成
		p_Beacon->mBeacon[i] = new cPolygonClass();

		// サイズ
		p_Beacon->mBeacon[i]->SetPolygonSize(D3DXVECTOR2((float)BEACON_SIZE_X,(float)BEACON_SIZE_Y));
		p_Beacon->mBeacon[i]->SetPolygonSizeMax(c_sizeBeacon);
		p_Beacon->mBeacon[i]->SetPolygonPAnim(c_animBeacon);

		// 設定用変数の初期化
		p_Beacon->tBeacon[i].alpha = 1.0f;
		p_Beacon->tBeacon[i].count = 0;
		p_Beacon->tBeacon[i].pos = D3DXVECTOR3(0.0f,0.0f,0.0f);
		p_Beacon->tBeacon[i].vel = D3DXVECTOR3(0.0f,0.0f,0.0f);

		// ポリゴン作成
		p_Beacon->mBeacon[i]->Init3D(pDevice,TEXTURE_GAME_BEACON);
	}
	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitBeacon(void)
{
	for ( int i = 0; i < MAX_BEACON; i++ )
		delete p_Beacon->mBeacon[i];

	delete p_Beacon;
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateBeacon(void)
{
	for ( int i = 0; i < MAX_BEACON; i++ )
	{
		if ( p_Beacon->mBeacon[i]->GetPolygonExist() == false ) continue;

		// ビルボード設定
		float y = -atan2f(GetCameraEyePt().z - GetCameraLook().z,GetCameraEyePt().x - GetCameraLook().x);
		p_Beacon->mBeacon[i]->SetPolygonRot(D3DXVECTOR3(0.0f,y,0.0f));
		// 表示OFF
		if ( p_Beacon->mBeacon[i]->GetPolygonAlpha() <= 0.0f )
			p_Beacon->mBeacon[i]->SetPolygonExist(false);
		// 透過度
		p_Beacon->tBeacon[i].alpha -= 1.0f / 30;
		p_Beacon->mBeacon[i]->SetPolygonAlpha(p_Beacon->tBeacon[i].alpha);
		// ポリゴン座標のセット
		p_Beacon->mBeacon[i]->SetTexturePolygon3D();
		p_Beacon->mBeacon[i]->SetVertexPolygon3D();
	}
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawBeacon(LPDIRECT3DDEVICE9 pDevice)
{	
	for ( int i = 0; i < MAX_BEACON; i++ )
	{
		if ( p_Beacon->mBeacon[i]->GetPolygonExist() == false ) continue;
		p_Beacon->mBeacon[i]->Draw3D(pDevice);
	}
}

//=============================================================================
// パーティクルの設置
//=============================================================================
void SetBeacon(LPDIRECT3DDEVICE9 pDevice,D3DXVECTOR3 pos,int HoleCount,int num)
{
	for ( int count = num; count  > 0; count-- )
	{
		for ( int i = 0; i < MAX_BEACON; i++ )
		{
			// 表示している場合はコンテニュー
			if ( p_Beacon->mBeacon[i]->GetPolygonExist() ) continue;
			// ホール数代入
			p_Beacon->nHoleCount = HoleCount;
			// ビルボード設定
			float y = -atan2f(GetCameraEyePt().z - GetCameraLook().z,GetCameraEyePt().x - GetCameraLook().x);
			p_Beacon->mBeacon[i]->SetPolygonRot(D3DXVECTOR3(0.0f,y,0.0f));
			// 表示
			p_Beacon->mBeacon[i]->SetPolygonExist(true);
			// 位置
			p_Beacon->mBeacon[i]->SetPolygonPos(D3DXVECTOR3(pos.x,pos.y + BEACON_SIZE_Y/2 ,pos.z));
			// 透過度
			p_Beacon->tBeacon[i].alpha = 1.0f;
			p_Beacon->mBeacon[i]->SetPolygonAlpha(1.0f);
			// 色の設定
			switch ( p_Beacon->nHoleCount )
			{
			case 1:
				p_Beacon->mBeacon[i]->SetAnimationNo(ANIM_BEACON00);
				break;

			case 2:
				p_Beacon->mBeacon[i]->SetAnimationNo(ANIM_BEACON01);
				break;

			case 3:
				p_Beacon->mBeacon[i]->SetAnimationNo(ANIM_BEACON02);
				break;
			}
		
			break;
		}
	}
}