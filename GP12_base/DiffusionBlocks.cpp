//===================================================================================
//
// 拡散ブロック
//
//===================================================================================

#include "DiffusionBlocks.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// 現在座標を取得
D3DXVECTOR3 GetCurrentVec3Value(int i);

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

cDiffusionBlocks*		p_DiffusionBlocks;

int						g_nColorArray[5];

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitDiffusionBlocks(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	p_DiffusionBlocks = new cDiffusionBlocks();

	for ( int i = 0; i < MAX_DIFBLOCK; i++ )
	{
		// オブジェクトの作成
		p_DiffusionBlocks->mDiffusionBlocks[i] = new cMeshClass();

		// 表示
		p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshExist(false);
		p_DiffusionBlocks->mDiffusionBlocks[i]->Init(pDevice,MESH_CUBE);

		p_DiffusionBlocks->tDiffusionBlocks[i].d_Pos = D3DXVECTOR3(0.0f,0.0f,0.0f);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Speed = D3DXVECTOR3(0.0f,0.0f,0.0f);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Color = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Scale = D3DXVECTOR3(0.01f,0.01f,0.01f);
	}

	g_nColorArray[0] = 0xff0030;
	g_nColorArray[1] = 0xff7e00;
	g_nColorArray[2] = 0xffc600;
	g_nColorArray[3] = 0x34ef1b;
	g_nColorArray[4] = 0x2c6edb;

	p_DiffusionBlocks->nMaxFrame = 500;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitDiffusionBlocks(void)
{
	for ( int i = 0; i < MAX_DIFBLOCK; i++ )
		delete p_DiffusionBlocks->mDiffusionBlocks[i];

	delete p_DiffusionBlocks;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateDiffusionBlocks(void)
{
	for ( int i = 0; i < MAX_DIFBLOCK; i++ )
	{
		// 非表示にする
		if ( p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshScale().x <= 0.0f &&
			 p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshScale().y <= 0.0f &&
			 p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshScale().z <= 0.0f  )
		{
			p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshExist(false);
			p_DiffusionBlocks->nCurrentFrame[i] = 0;
		}

		// メッシュの移動(表示しているののみ)
		if ( p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshExist() == false) continue;
			// 移動処理
			p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshPos(GetCurrentVec3Value(i));
			// 縮小処理
			p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshScale(
				-p_DiffusionBlocks->tDiffusionBlocks[i].d_Scale.z,
				-p_DiffusionBlocks->tDiffusionBlocks[i].d_Scale.y,
				-p_DiffusionBlocks->tDiffusionBlocks[i].d_Scale.x);
			// 透過度設定
			p_DiffusionBlocks->tDiffusionBlocks[i].d_Color.a -= 1.0f/p_DiffusionBlocks->nMaxFrame;
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawDiffusionBlocks(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_DIFBLOCK; i++ )
	{
		if ( p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshExist() != true ) continue;

		p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshColor(pDevice,p_DiffusionBlocks->tDiffusionBlocks[i].d_Color);
		p_DiffusionBlocks->mDiffusionBlocks[i]->Draw(pDevice);	
	}
}

//-----------------------------------------------------------------------------------
// ブロックの設置
//-----------------------------------------------------------------------------------
void SetDiffusionBlocks(void)
{
	static int count = 0;
	count++;
	for ( int j = 0; j < 2; j++ )
	for ( int i = 0; i < MAX_DIFBLOCK; i++ )
	{
		// 表示している場合はコンテニュー
		if ( p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshExist() )	continue;

		// 移動スピードの設定
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Speed.x = (float)(rand()% 6 - 0);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Speed.y = (float)(rand()% 6 - 0);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Speed.z = (float)(rand()% 6 - 0);

		// 表示するように設定
		p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshExist(true);

		// 大きさの設定
		p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));

		// 位置の代入
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Pos.x = (float)(rand()%(200-(-200)+200)-200);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Pos.y = (float)(rand()%(200-(-200)+200)-200);
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Pos.z = (float)(rand()%(200-(-200)+200)-200);

		D3DXVECTOR3 pos;
		pos.x = (float)(rand()%(200-(-200)+200)-200);
		pos.y = (float)(rand()%(200-(-200)+200)-200);
		pos.z = (float)(rand()%(200-(-200)+200)-200);
		p_DiffusionBlocks->mDiffusionBlocks[i]->SetMeshPos(pos);


		// 色の設定
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Color = g_nColorArray[0 + rand()%5-0];		
		p_DiffusionBlocks->tDiffusionBlocks[i].d_Color.a = 1.0f;
		break;
	}
}

//-----------------------------------------------------------------------------------
// 現在位置を取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCurrentVec3Value(int i)
{
	return p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshPos() + 
		( p_DiffusionBlocks->tDiffusionBlocks[i].d_Pos - p_DiffusionBlocks->mDiffusionBlocks[i]->GetMeshPos() ) *
		( ++p_DiffusionBlocks->nCurrentFrame[i] / (float) p_DiffusionBlocks->nMaxFrame );
}