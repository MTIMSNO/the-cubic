//===================================================================================
//
// セレクトメニューのマニュアル
//
//===================================================================================

#include "SelectMenuManual.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

void InitialPosition(int i);
void SetPositionMenu(int i);
void ManualScaling(int i);

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

cSelectMenuManual*			pSelectMenuManual;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitSelectMenuManual(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	pSelectMenuManual = new cSelectMenuManual();

	for ( int i = 0; i < MAX_MANUAL; i++ )
	{
		// オブジェクトの作成
		pSelectMenuManual->mSelectMenuManual[i] = new cPolygonClass();

		// 各種設定
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonSizeMax(c_sizeSelectMenuManual);
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPAnim(c_animSelectMenuManual);
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonExist(true);
		InitialPosition(i);
		pSelectMenuManual->bBigFlag[i] = true;
		pSelectMenuManual->bSmallFlag[i] = false;

		// ポリゴンの作成
		pSelectMenuManual->mSelectMenuManual[i]->Init(pDevice,TEXTURE_MANUAL);
	}



	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitSelectMenuManual(void)
{
	for ( int i = 0; i < MAX_MANUAL; i++ )
		delete pSelectMenuManual->mSelectMenuManual[i];

	delete pSelectMenuManual;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateSelectMenuManual(void)
{
	for ( int i = 0; i < MAX_MANUAL; i++ )
	{
		SetPositionMenu(i);
		ManualScaling(i);
		pSelectMenuManual->mSelectMenuManual[i]->SetAnimationNo(i);
		pSelectMenuManual->mSelectMenuManual[i]->SetTexturePolygon();
		pSelectMenuManual->mSelectMenuManual[i]->SetVertexPolygon();		
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawSelectMenuManual(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_MANUAL; i++ )
	{
		if ( pSelectMenuManual->mSelectMenuManual[i]->GetPolygonExist() == false ) continue;

		pSelectMenuManual->mSelectMenuManual[i]->Draw(pDevice);
	}
}

//-----------------------------------------------------------------------------------
// 初期位置の設定
//-----------------------------------------------------------------------------------
void InitialPosition(int i)
{
	switch(i)
	{
	case 0:
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X-140.0f,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/1.15f,0.0f));
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonSize(D3DXVECTOR2((float)MANUAL_ARROWSIZE_X,(float)MANUAL_ARROWSIZE_Y));	
		break;
		
	case 1:
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X-140.0f,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/1.15f,0.0f));
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonSize(D3DXVECTOR2((float)MANUAL_ARROWSIZE_X,(float)MANUAL_ARROWSIZE_Y));				
		break;

	case 2:
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y + SCREEN_CENTER_Y/1.5f,0.0f));
		pSelectMenuManual->mSelectMenuManual[i]->SetPolygonSize(D3DXVECTOR2((float)MANUAL_ENTER_X,(float)MANUAL_ENTER_Y));
		break;
	}
}

//-----------------------------------------------------------------------------------
// 位置の設定
//-----------------------------------------------------------------------------------
void SetPositionMenu(int i)
{
	switch(GetSelectMenuInfo(2))
	{
	case STATE_SELECTLEVEL:
		switch(i)
		{
		case 0:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X-140.0f,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/1.15f,0.0f));
			break;

		case 1:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X-140.0f,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/1.15f,0.0f));
			break;

		case 2:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonExist(false);
			break;
		}
		break;

	case STATE_SELECTSTAGE:
		switch(i)
		{
		case 0:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X+140.0f,(float)SCREEN_CENTER_Y-SCREEN_CENTER_Y/1.15f,0.0f));
			break;

		case 1:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X+140.0f,(float)SCREEN_CENTER_Y+SCREEN_CENTER_Y/1.15f,0.0f));
			break;

		case 2:
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y + SCREEN_CENTER_Y/1.5f,0.0f));
			pSelectMenuManual->mSelectMenuManual[i]->SetPolygonExist(true);
			break;
		}
		break;
	}
}

//-----------------------------------------------------------------------------------
// 拡縮
//-----------------------------------------------------------------------------------
void ManualScaling(int i)
{
	if (pSelectMenuManual->bBigFlag[i] && pSelectMenuManual->bSmallFlag[i] == false)
	{
		pSelectMenuManual->mSelectMenuManual[i]->AddPolygonScale(0.025f);
		if ( pSelectMenuManual->mSelectMenuManual[i]->GetPolygonScale() >= 1.15f )
		{
			pSelectMenuManual->bBigFlag[i] = false;
			pSelectMenuManual->bSmallFlag[i] = true;
		}
	}
	else if (pSelectMenuManual->bBigFlag[i] == false && pSelectMenuManual->bSmallFlag[i])
	{
		pSelectMenuManual->mSelectMenuManual[i]->AddPolygonScale(-0.025f);
		if ( pSelectMenuManual->mSelectMenuManual[i]->GetPolygonScale() <= 0.85f )
		{
			pSelectMenuManual->bBigFlag[i] = true;
			pSelectMenuManual->bSmallFlag[i] = false;
		}
	}
}