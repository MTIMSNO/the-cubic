//===================================================================================
//
// セレクトメニュー[SelectMenu.cpp]
//
//===================================================================================

#include "SelectMenu.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// セレクトレベル
void SelectLevel(void);
// セレクトステージ
void SelectStage(void);
// 回転
void RollSelectMenu( int time , int roll );

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

// ポインタ作成
cSelectMenu*				pSelectMenu;

// Xfile
TCHAR*						SelectLevelFileName[] = {
	_T("data/MODEL/EASY.x"),
	_T("data/MODEL/NORMAL.x"),
	_T("data/MODEL/HARD.x"),
};
TCHAR*						SelectStageFileName[] = {
	_T("data/MODEL/ONE.x"),
	_T("data/MODEL/TWO.x"),
	_T("data/MODEL/THREE.x"),
};

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitSelectMenu(LPDIRECT3DDEVICE9 pDevice)
{
	// 動的オブジェクトの作成
	pSelectMenu = new cSelectMenu();

	// 距離の設定
	pSelectMenu->nSelectDistance = 150;
	
	float fAngle = 270.0f;
	for ( int i = 0; i < MAX_SELECTMENU; i++ )
	{
		// 動的オブジェクトの作成
		pSelectMenu->mSelectLevel[i] = new cMeshClass();
		pSelectMenu->mSelectStage[i] = new cMeshClass();

		// 角度の設定
		pSelectMenu->mSelectLevel[i]->SetMeshAngle((int)fAngle);
		pSelectMenu->mSelectStage[i]->SetMeshAngle((int)fAngle);

		// 位置の設定
		D3DXVECTOR3	pos = D3DXVECTOR3(40.0f,0.0f,0.0f);
		pos.y = cos( pSelectMenu->mSelectLevel[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
		pos.z = sin( pSelectMenu->mSelectLevel[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
		pSelectMenu->mSelectLevel[i]->SetMeshPos(pos);

		pos = D3DXVECTOR3(-40.0f,0.0f,0.0f);
		pos.y = cos( pSelectMenu->mSelectStage[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
		pos.z = sin( pSelectMenu->mSelectStage[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
		pSelectMenu->mSelectStage[i]->SetMeshPos(pos);

		// オブジェクトの作成
		pSelectMenu->mSelectLevel[i]->Init(pDevice,SelectLevelFileName[i]);
		pSelectMenu->mSelectStage[i]->Init(pDevice,SelectStageFileName[i]);

		fAngle += 120.0f;
		if ( fAngle >= 360.0f )
			fAngle -= 360.0f;
	}

	// 各変数の初期化
	pSelectMenu->nSelectLevel = 0;
	pSelectMenu->nSelectStage = 0;
	pSelectMenu->nSelectMenu = STATE_SELECTLEVEL;
	pSelectMenu->fSelectAngle = 0.0f;
	pSelectMenu->bRollLeft = false;
	pSelectMenu->bRollRight = false;
	pSelectMenu->nCount = 0;
	pSelectMenu->bFade = false;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitSelectMenu(void)
{
	for ( int i = 0; i < MAX_SELECTMENU; i++ )
	{
		delete pSelectMenu->mSelectLevel[i];
		delete pSelectMenu->mSelectStage[i];
	}

	delete pSelectMenu;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateSelectMenu(void)
{
	// ステージの設定
	SelectStage();
	// レベルの設定
	SelectLevel();
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawSelectMenu(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_SELECTMENU; i++ )
	{
		if ( pSelectMenu->mSelectLevel[i]->GetMeshExist() == false ||
			 pSelectMenu->mSelectStage[i]->GetMeshExist() == false )
			 continue;

		pSelectMenu->mSelectLevel[i]->Draw(pDevice);
		pSelectMenu->mSelectStage[i]->Draw(pDevice);
	}
}

//-----------------------------------------------------------------------------------
// 回転処理
//-----------------------------------------------------------------------------------
void RollSelectMenu( int time , int roll )
{
	// 中心座標
	D3DXVECTOR3	pos = D3DXVECTOR3(0.0f,0.0f,0.0f);

	// 時間による回転量
	pSelectMenu->fSelectAngle = (float)SPEED / time;

	// 全てのキューブに適応
	for ( int i = 0; i < MAX_SELECTMENU; i++ )
	{
		switch ( pSelectMenu->nSelectMenu )
		{
		case STATE_SELECTLEVEL:
			pSelectMenu->mSelectLevel[i]->AddMeshAngle((int)pSelectMenu->fSelectAngle * roll);
			if ( roll == -1 )
			{
				if ((float)pSelectMenu->mSelectLevel[i]->GetMeshAngle() < 0 )
					pSelectMenu->mSelectLevel[i]->SetMeshAngle(pSelectMenu->mSelectLevel[i]->GetMeshAngle() + 360);
			}
			else if ( roll == 1 )
			{
				if ((float)pSelectMenu->mSelectLevel[i]->GetMeshAngle() > 360 )
					pSelectMenu->mSelectLevel[i]->SetMeshAngle(pSelectMenu->mSelectLevel[i]->GetMeshAngle() - 360);
			}

			pos = D3DXVECTOR3(30.0f,0.0f,0.0f);
			pos.y = cos( pSelectMenu->mSelectLevel[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
			pos.z = sin( pSelectMenu->mSelectLevel[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
			pSelectMenu->mSelectLevel[i]->SetMeshPos(pos);
			break;

		case STATE_SELECTSTAGE:
			pSelectMenu->mSelectStage[i]->AddMeshAngle((int)pSelectMenu->fSelectAngle * roll);
			if ( roll == -1 )
			{
				if ((float)pSelectMenu->mSelectStage[i]->GetMeshAngle() < 0 )
					pSelectMenu->mSelectStage[i]->SetMeshAngle(pSelectMenu->mSelectStage[i]->GetMeshAngle() + 360);
			}
			else if ( roll == 1 )
			{
				if ((float)pSelectMenu->mSelectStage[i]->GetMeshAngle() > 360 )
					pSelectMenu->mSelectStage[i]->SetMeshAngle(pSelectMenu->mSelectStage[i]->GetMeshAngle() - 360);
			}

			pos = D3DXVECTOR3(-30.0f,0.0f,0.0f);
			pos.y = cos( pSelectMenu->mSelectStage[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
			pos.z = sin( pSelectMenu->mSelectStage[i]->GetMeshAngle() * D3DX_PI/180 ) * pSelectMenu->nSelectDistance;
			pSelectMenu->mSelectStage[i]->SetMeshPos(pos);
			break;
		}
	}

	// カウントアップ
	pSelectMenu->nCount++;

	// タイムと同じになったら終了
	if ( pSelectMenu->nCount >= time )
	{
		pSelectMenu->bRollLeft = false;
		pSelectMenu->bRollRight = false;
		pSelectMenu->fSelectAngle = 0.0f;
		pSelectMenu->nCount = 0;
	}

}

//-----------------------------------------------------------------------------------
// セレクトレベル操作
//-----------------------------------------------------------------------------------
void SelectLevel(void)
{
	if (!(pSelectMenu->nSelectMenu == STATE_SELECTLEVEL)) return;

	if ( pSelectMenu->bRollLeft == false && pSelectMenu->bRollRight == false )
	{
		if ( GetKeyboardPress(DIK_UP) )
		{
			pSelectMenu->nSelectLevel++;
			if ( pSelectMenu->nSelectLevel >= MAX_SELECTMENU )	pSelectMenu->nSelectLevel = 0;
			pSelectMenu->bRollRight = true;
		}
		else if ( GetKeyboardPress(DIK_DOWN) )
		{
			pSelectMenu->nSelectLevel--;
			if ( pSelectMenu->nSelectLevel < 0 )	pSelectMenu->nSelectLevel = MAX_SELECTMENU-1;
			pSelectMenu->bRollLeft = true;
		}
	}

	// 選択中を回転
	pSelectMenu->mSelectLevel[pSelectMenu->nSelectLevel]->SetMeshRot(0.02f,0.02f,0.02f);
	// ステージの方の回転を修正
	pSelectMenu->mSelectStage[pSelectMenu->nSelectStage]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));

	if ( pSelectMenu->bRollRight && pSelectMenu->bRollLeft == false )	RollSelectMenu(30,ROLLRIGHT);
	if ( pSelectMenu->bRollRight == false && pSelectMenu->bRollLeft )	RollSelectMenu(30,ROLLLEFT);
	if ( GetKeyboardTrigger(DIK_RETURN) && !(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) pSelectMenu->nSelectMenu = STATE_SELECTSTAGE;
	else if ( GetKeyboardTrigger(DIK_RIGHT) && !(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) pSelectMenu->nSelectMenu = STATE_SELECTSTAGE;
}

//-----------------------------------------------------------------------------------
// セレクトステージ
//-----------------------------------------------------------------------------------
void SelectStage(void)
{
	if (!(pSelectMenu->nSelectMenu == STATE_SELECTSTAGE))	return;

	if ( pSelectMenu->bRollLeft == false && pSelectMenu->bRollRight == false )
	{
		if ( GetKeyboardPress(DIK_UP) )
		{
			pSelectMenu->nSelectStage++;
			if ( pSelectMenu->nSelectStage >= MAX_SELECTMENU )	pSelectMenu->nSelectStage = 0;
			pSelectMenu->bRollRight = true;
		}
		else if ( GetKeyboardPress(DIK_DOWN) )
		{
			pSelectMenu->nSelectStage--;
			if ( pSelectMenu->nSelectStage < 0 )	pSelectMenu->nSelectStage = MAX_SELECTMENU-1;
			pSelectMenu->bRollLeft = true;
		}
	}

	// 選択中を回転
	pSelectMenu->mSelectStage[pSelectMenu->nSelectStage]->SetMeshRot(-0.02f,-0.02f,-0.02f);
	// レベルの方の回転を修正
	pSelectMenu->mSelectLevel[pSelectMenu->nSelectLevel]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));

	if ( pSelectMenu->bRollRight && pSelectMenu->bRollLeft == false )	RollSelectMenu(30,ROLLRIGHT);
	if ( pSelectMenu->bRollRight == false && pSelectMenu->bRollLeft )	RollSelectMenu(30,ROLLLEFT);
	if ( GetKeyboardTrigger(DIK_BACKSPACE) && !(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) pSelectMenu->nSelectMenu = STATE_SELECTLEVEL;
	else if ( GetKeyboardTrigger(DIK_LEFT) && !(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) pSelectMenu->nSelectMenu = STATE_SELECTLEVEL;
	
	if ( GetKeyboardTrigger(DIK_RETURN) && pSelectMenu->nSelectMenu == STATE_SELECTSTAGE && !(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) 
	{
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		pSelectMenu->bFade = true;
	}
	else if ( pSelectMenu->bFade && IsCheckFade() == FADE_OFF )
	{
		SetFade(0.0f,0.0f,0.0f,0.0f,15);
		SetData(DATA_SELECTSTAGE,pSelectMenu->nSelectStage);
		SetData(DATA_STAGELEVEL,pSelectMenu->nSelectLevel);
		SetData(DATA_MAINSTATE,STATE_NOWLOADING);
		pSelectMenu->bFade = false;
		pSelectMenu->bRollLeft = pSelectMenu->bRollRight = false;
		pSelectMenu->nSelectMenu = STATE_SELECTLEVEL;
	}
}

//-----------------------------------------------------------------------------------
// ステート/レベル/ステージ
//-----------------------------------------------------------------------------------
int GetSelectMenuInfo(int i)
{
	switch ( i )
	{
	case STATE_SELECTLEVEL:
		return pSelectMenu->nSelectLevel;

	case STATE_SELECTSTAGE:
		return pSelectMenu->nSelectStage;

	case STATE_SELECTMENU:
		return pSelectMenu->nSelectMenu;
	}
	return -1;
}

//-----------------------------------------------------------------------------------
// 回転させているかどうか
//-----------------------------------------------------------------------------------
bool GetSelectMenuRoll(void)
{
	if (!(pSelectMenu->bRollLeft) && !(pSelectMenu->bRollRight)) 
		return false;
	else 
		return true;

	return false;
}