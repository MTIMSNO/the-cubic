//===================================================================================
//	フェードイン/フェードアウト
//===================================================================================

#include "Fade.h"

//-----------------------------------------------------------------------------------
//	プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT MakeVertexFade(LPDIRECT3DDEVICE9 pDevice);
void SetVertexFade();

//-----------------------------------------------------------------------------------
//	構造体定義
//-----------------------------------------------------------------------------------
typedef struct _tFade
{
	D3DXVECTOR3				vtx;
	float					rhw;
	D3DCOLOR				diffuse;
	D3DXVECTOR2				tex;
}tFade;


typedef struct _tFadeDate
{
	float					Fr;					//赤
	float					Fg;					//緑
	float					Fb;					//青
	float					Alpha;				//アルファ値
	int						Time;				//時間
}tFadeData;

//-----------------------------------------------------------------------------------
//	グローバル変数
//-----------------------------------------------------------------------------------

//テクスチャへのポリゴン
tFade						g_vertexFade[NUM_VERTEX];
//フェードデータ
tFadeData					g_FadeData;
//アルファ値
float						g_fAlpha;
//１秒あたりの色の変化量
float						g_SetAlpha;
//フェードする時間
int							g_FadeCount;
//フェードするかしないか
bool						g_FadeFlag;
//フェードの状態
int							g_FadeState;

//-----------------------------------------------------------------------------------
//	初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitFade( LPDIRECT3DDEVICE9 pDevice )
{
	//アルファ値を透明に設定
	g_fAlpha = 0.0f;
	//フェードカウントの初期化
	g_FadeCount = 0;
	//フラグの初期化
	g_FadeFlag = false;
	//フェードの状態
	g_FadeState = FADE_OFF;
	//頂点情報の作成
	MakeVertexFade(pDevice);
	return S_OK;
}

//-----------------------------------------------------------------------------------
//	終了処理
//-----------------------------------------------------------------------------------
void UninitFade()
{
}

//-----------------------------------------------------------------------------------
//	更新処理
//-----------------------------------------------------------------------------------
void UpdateFade()
{
	if ( !(g_FadeFlag) ) return;

	if ( g_FadeCount <= g_FadeData.Time )
	{
		//反射光の設定
		Setvertexfade();
		//毎フレームアルファ値をプラス
		g_fAlpha += g_SetAlpha;
		//カウントアップ
		g_FadeCount++;
	}
	else
	{
		//フラグの設定
		g_FadeState = FADE_OFF;
		g_FadeFlag = false;
	}
}

//-----------------------------------------------------------------------------------
//	描画処理
//-----------------------------------------------------------------------------------
void DrawFade( LPDIRECT3DDEVICE9 pDevice )
{
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D_FADE);
	// テクスチャの設定
	pDevice->SetTexture( 0, NULL );
	// ポリゴンの描画
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, NUM_POLYGON, g_vertexFade, sizeof(tFade));
}

//-----------------------------------------------------------------------------------
//	
//-----------------------------------------------------------------------------------
HRESULT MakeVertexFade(LPDIRECT3DDEVICE9 pDevice)
{
	// 頂点座標の設定
	g_vertexFade[0].vtx = D3DXVECTOR3(   0.0f,   0.0f, 0.0f);
	g_vertexFade[1].vtx = D3DXVECTOR3( 800.0f,   0.0f, 0.0f);
	g_vertexFade[2].vtx = D3DXVECTOR3(   0.0f, 600.0f, 0.0f);
	g_vertexFade[3].vtx = D3DXVECTOR3( 800.0f, 600.0f, 0.0f);

	// rhwの設定
	g_vertexFade[0].rhw =
	g_vertexFade[1].rhw =
	g_vertexFade[2].rhw =
	g_vertexFade[3].rhw = 1.0f;

	// 反射光の設定
	g_vertexFade[0].diffuse = 
	g_vertexFade[1].diffuse = 
	g_vertexFade[2].diffuse = 
	g_vertexFade[3].diffuse = D3DXCOLOR(0.0f, 0.0f, 0.0f, g_fAlpha);

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 反射光の設定
//-----------------------------------------------------------------------------------
void Setvertexfade()
{
	// 反射光の設定
	g_vertexFade[0].diffuse = 
	g_vertexFade[1].diffuse = 
	g_vertexFade[2].diffuse =
	g_vertexFade[3].diffuse = D3DXCOLOR(g_FadeData.Fr, g_FadeData.Fg, g_FadeData.Fb, g_fAlpha);
}


//-----------------------------------------------------------------------------------
// フェードイン・アウトの値の設定
//-----------------------------------------------------------------------------------
void SetFade ( float fr, float fg, float fb, float SetAlpha , int time )
{
	//RGBの設定
	g_FadeData.Fr = fr;
	g_FadeData.Fg = fg;
	g_FadeData.Fb = fb;

	//目標アルファ値
	g_FadeData.Alpha = SetAlpha;

		//目標のアルファ値から現在のアルファ値を引く
		g_SetAlpha = g_FadeData.Alpha - g_fAlpha;

	//時間
	g_FadeData.Time = time;

		//１タイム毎の変化量
		g_SetAlpha = g_SetAlpha / g_FadeData.Time;

	//フェードカウント初期化
	g_FadeCount = 0;

	//フラグの設定
	g_FadeFlag = true;
	g_FadeState = FADE_ON;
}

//-----------------------------------------------------------------------------------
// フェードしているかしていないか
//-----------------------------------------------------------------------------------
int IsCheckFade()
{
	return g_FadeState;
}