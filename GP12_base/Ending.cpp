//===================================================================================
//
// エンディング
//
//===================================================================================

#include "Ending.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// 
cEnding*			pEnding;

// Xfile
TCHAR*				EndingFileName[MAX_ENDING] = {
	_T("data/MODEL/CLEAR.x"),
	_T("data/MODEL/GAMEOVER.x"),
};


//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitEnding(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	pEnding = new cEnding();

	for ( int i = 0; i < MAX_ENDING; i++ )
	{
		// 動的オブジェクトの作成
		pEnding->mEnding[i] = new cMeshClass();

		// オブジェクトの作成
		pEnding->mEnding[i]->Init(pDevice,EndingFileName[i]);
	}

	// 変数の初期化
	pEnding->nEndingState = -1;
	pEnding->bFade = false;
	pEnding->bRelease = true;
	pEnding->nCount = 0;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitEnding(void)
{
	for ( int i = 0; i < MAX_ENDING; i++ )
		delete pEnding->mEnding[i];

	delete pEnding;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateEnding(void)
{
	pEnding->nEndingState = GetData(DATA_MAINSTATE);

	// ステージ解放処理
	if ( pEnding->bRelease )
	{
		UninitStageData();
		ReInitFallBlocks();
		ReInitParticle();
		pEnding->bRelease = false;
	}

	switch ( pEnding->nEndingState )
	{
	case STATE_CLEAR:
		// 拡散ブロックの設定
		SetDiffusionBlocks();
		// 表示・非表示
		pEnding->mEnding[END_CLEAR]->SetMeshExist(true);
		pEnding->mEnding[END_GAMEOVER]->SetMeshExist(false);
		// 回転設定
		pEnding->mEnding[END_CLEAR]->SetMeshRot(0.02f,0.02f,0.02f);
		break;

	case STATE_GAMEOVER:
		// 表示・非表示
		pEnding->mEnding[END_GAMEOVER]->SetMeshExist(true);
		pEnding->mEnding[END_CLEAR]->SetMeshExist(false);
		// 回転設定
		pEnding->mEnding[END_GAMEOVER]->SetMeshRot(0.02f,0.02f,0.02f);
		break;
	}

	if ( pEnding->bRelease == false && pEnding->nCount == 450 && pEnding->nEndingState == STATE_CLEAR )
	{
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		pEnding->bFade = true;
	}
	else if ( pEnding->bRelease == false && pEnding->nCount == 120 && pEnding->nEndingState == STATE_GAMEOVER )
	{
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		pEnding->bFade = true;
	}
	else if ( pEnding->bFade && IsCheckFade() == FADE_OFF )
	{
		SetFade(0.0f,0.0f,0.0f,0.0f,15);
		SetData(DATA_MAINSTATE,STATE_SELECT);
		pEnding->bFade = false;
		pEnding->bRelease = true;
		pEnding->nCount = 0;
	}

	pEnding->nCount++;
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawEnding(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_ENDING; i++ )
	{
		if ( pEnding->mEnding[i]->GetMeshExist() == false )	continue;

		pEnding->mEnding[i]->Draw(pDevice);
	}
}

