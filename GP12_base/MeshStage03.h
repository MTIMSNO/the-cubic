//===================================================================================
//
// メッシュステージ
//
//===================================================================================
#pragma once 


#include "Main.h"
#include "StageClass.h"
#include "MeshClass.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define STAGE03_X				(11)
#define STAGE03_Y				(7)
#define STAGE03_Z				(11)
#define MAX_PLAYER03			(3)
#define MAX_HOLE03				(3)
#define MAX_BLOCK03				(2)
#define MAX_WARP03				(2)

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cMeshStage03 : public cStageClass
{
public:
	// メッシュ
	cMeshClass*			mStage03[STAGE03_Z][STAGE03_Y][STAGE03_X];
	// プレイヤー / ホール
	tStageData			Player03[MAX_PLAYER03+1];
	tStageData			Hole03[MAX_HOLE03];
	tStageData			Block03[MAX_BLOCK03];
	tStageData			Warp03[MAX_WARP03];
};


HRESULT InitMeshStage03 (LPDIRECT3DDEVICE9 pDevice);
void UninitMeshStage03(void);
void UpdateMeshStage03(void);
void DrawMeshStage03(LPDIRECT3DDEVICE9 pDevice);

int GetPlayerAngle03();
D3DXVECTOR3 GetPlayerPos03(void);
D3DXVECTOR3 GetCameraMoveVol03(void);
bool GetPlayerChange03(void);
