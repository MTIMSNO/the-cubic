//===================================================================================
// Tween.cpp
// 現在位置から目的位置まで設定したフレームで移動する。
//===================================================================================

#include "Tween.h"

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

float		g_fFrom;
float		g_fTo;
int			g_nMaxFrame;
int			g_nCurrentFrame;

D3DXVECTOR2 g_dFrom;
D3DXVECTOR2 g_dTo;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
void InitTween(void)
{
	g_fFrom = g_fTo = 0.0f;
	g_nMaxFrame = g_nCurrentFrame = 0;
	g_dFrom = g_dTo = D3DXVECTOR2(0.0f,0.0f);
}

//-----------------------------------------------------------------------------------
// Tween設定
// …オーバーロードしているので引数だけその形に合わせてくれれば
//	自動的に関数判断してくれます。
//-----------------------------------------------------------------------------------
// from		... 現在位置
// to		... 目的位置
// maxframe	... かかってほしいフレーム数
//-----------------------------------------------------------------------------------
void SetTween(float from,float to,int maxframe)
{
	g_fFrom = from;
	g_fTo = to;
	g_nMaxFrame = maxframe;
	g_nCurrentFrame = 0;
}

void SetTween(D3DXVECTOR2 from,D3DXVECTOR2 to,int maxframe)
{
	g_dFrom = from;
	g_dTo = to;
	g_nMaxFrame = maxframe;
	g_nCurrentFrame = 0;
}

//-----------------------------------------------------------------------------------
// 判定
//-----------------------------------------------------------------------------------
bool isEnd(void)
{
	return g_nCurrentFrame >= g_nMaxFrame;
}

//-----------------------------------------------------------------------------------
// 現在位置を取得
//-----------------------------------------------------------------------------------
float GetCurrentValue(void)
{
	if ( isEnd() )
		g_nCurrentFrame = g_nMaxFrame;

	return g_fFrom + (g_fTo - g_fFrom) * ( ++g_nCurrentFrame / (float)g_nMaxFrame );
}

D3DXVECTOR2 GetCurrentVec2Value(void)
{
	if ( isEnd() )
		g_nCurrentFrame = g_nMaxFrame;

	return g_dFrom + (g_dTo - g_dFrom) * ( ++g_nCurrentFrame / (float)g_nMaxFrame );
}
	