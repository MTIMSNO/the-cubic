//===================================================================================
//
// ステージデータ
//
//===================================================================================

#include "StageData.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitStageData(LPDIRECT3DDEVICE9 pDevice)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		InitMeshStage01(pDevice);
		return S_OK;
		break;

	case LEVEL_NORMAL:
		InitMeshStage02(pDevice);
		return S_OK;
		break;

	case LEVEL_HARD:
		InitMeshStage03(pDevice);
		return S_OK;
		break;
	}

	return -1;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitStageData(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		UninitMeshStage01();
		break;

	case LEVEL_NORMAL:
		UninitMeshStage02();
		break;

	case LEVEL_HARD:
		UninitMeshStage03();
		break;
	}
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateStageData(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		UpdateMeshStage01();
		break;

	case LEVEL_NORMAL:
		UpdateMeshStage02();
		break;

	case LEVEL_HARD:
		UpdateMeshStage03();
		break;
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawStageData(LPDIRECT3DDEVICE9 pDevice)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		DrawMeshStage01(pDevice);
		break;

	case LEVEL_NORMAL:
		DrawMeshStage02(pDevice);
		break;

	case LEVEL_HARD:
		DrawMeshStage03(pDevice);
		break;
	}
}

//-----------------------------------------------------------------------------------
// プレイヤーの角度の取得
//-----------------------------------------------------------------------------------
int GetPlayerAngle(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		return GetPlayerAngle01();

	case LEVEL_NORMAL:
		return GetPlayerAngle02();

	case LEVEL_HARD:
		return GetPlayerAngle03();
	}
	return -1;
}

//-----------------------------------------------------------------------------------
// プレイヤーの中心座標の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetPlayerPos(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		return GetPlayerPos01();

	case LEVEL_NORMAL:
		return GetPlayerPos02();

	case LEVEL_HARD:
		return GetPlayerPos03();
	}
	return D3DXVECTOR3(0.0f,0.0f,0.0f);
}

//-----------------------------------------------------------------------------------
// カメラの移動量の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCameraMoveVec(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		return GetCameraMoveVol01();

	case LEVEL_NORMAL:
		return GetCameraMoveVol02();

	case LEVEL_HARD:
		return GetCameraMoveVol03();
	}
	return D3DXVECTOR3(0.0f,0.0f,0.0f);
}

//-----------------------------------------------------------------------------------
// プレイヤーを切り替えることができるか 
//-----------------------------------------------------------------------------------
bool GetPlayerChange(void)
{
	switch(GetData(DATA_STAGELEVEL))
	{
	case LEVEL_EASY:
		return GetPlayerChange01();

	case LEVEL_NORMAL:
		return GetPlayerChange02();

	case LEVEL_HARD:
		return GetPlayerChange03();
	}
	return false;
}
