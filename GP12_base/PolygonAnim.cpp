// アニメーションデータ

#include "polygonAnim.h"

//===========================================================
// パーティクル
//===========================================================
const D3DXVECTOR2 c_sizeParticle(96,32);	// 画像のサイズ
const tAnimKeyData c_animParticle00[] =
{
	{
		1,	// フレーム
		{0, 0, 32, 32},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animParticle01[] =
{
	{
		1,	// フレーム
		{33, 0, 64, 32},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animParticle02[] =
{
	{
		1,	// フレーム
		{65, 0, 96, 32},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData* c_animParticle[] = {
	c_animParticle00,
	c_animParticle01,
	c_animParticle02,
};

//===========================================================
// ビーコン
//===========================================================
const D3DXVECTOR2 c_sizeBeacon(96,128);	// 画像のサイズ
const tAnimKeyData c_animBeacon00[] =
{
	{
		1,	// フレーム
		{0, 0, 32, 128},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animBeacon01[] =
{
	{
		1,	// フレーム
		{33, 0, 64, 128},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData c_animBeacon02[] =
{
	{
		1,	// フレーム
		{65, 0, 96, 128},	// 座標
		ANIM_FLAG_LOOP	// フラグ
	},
};
const tAnimKeyData* c_animBeacon[] = {
	c_animBeacon00,
	c_animBeacon01,
	c_animBeacon02,
};

//===========================================================
// ロード
//===========================================================
const D3DXVECTOR2 c_sizeNowLoading(256,256);
const tAnimKeyData c_animNowLoading_00[] =
{
	{
		1,
		{ 0, 0, 256, 256 },
		ANIM_FLAG_LOOP
	}
};
const tAnimKeyData* c_animNowLoading[] = {
	c_animNowLoading_00,
};

//===========================================================
// セレクト画面のマニュアル
//===========================================================
const D3DXVECTOR2 c_sizeSelectMenuManual(256,256);
const tAnimKeyData c_animSelectMenuManual_00[] =
{
	{
		1,
		{ 0, 0, 128, 128 },
		ANIM_FLAG_LOOP
	}
};
const tAnimKeyData c_animSelectMenuManual_01[] =
{
	{
		1,
		{ 129, 0, 256, 128 },
		ANIM_FLAG_LOOP
	}
};
const tAnimKeyData c_animSelectMenuManual_02[] =
{
	{
		1,
		{ 0, 129, 256, 256 },
		ANIM_FLAG_LOOP
	}
};
const tAnimKeyData* c_animSelectMenuManual[] = {
	c_animSelectMenuManual_00,
	c_animSelectMenuManual_01,
	c_animSelectMenuManual_02,
};