//=======================================================================================
//
//	2Dポリゴンベース
//
// 作成者：fujimura koichi	作成月：2013.11
//=======================================================================================
#include "PolygonBase.h"
#include "polygonAnim.h"

// 設定されたデータからポリゴン作成
HRESULT MakePolygon( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* tex /*= NULL*/ )
{
	// オブジェクトの頂点バッファを生成
	if( FAILED( pDevice->CreateVertexBuffer( sizeof(VERTEX_2D)*NUM_VERTEX,	// 頂点データ用に確保するバッファサイズ（バイト単位）
												D3DUSAGE_WRITEONLY,			// 頂点バッファの使用法
												FVF_VERTEX_2D,				// 使用する頂点フォーマット
												D3DPOOL_MANAGED,			// リソースのバッファを保持するメモリクラスを指定
												&obj->pD3DVtxBuff,			// 頂点バッファインターフェースへのポインタ
												NULL ) ) )					// NULL固定
	{
		return E_FAIL;
	}

	{	// 頂点バッファの中身を埋める
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
		// 基本値を設定
		obj->fRadius = sqrtf(obj->size.x * obj->size.x + obj->size.y * obj->size.y) / 2.0f;
		obj->fBaseAngle = atan2f(obj->size.x, obj->size.y);

		// 頂点座標の設定
		pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[0].vtx.z = 0.0f;
		pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[1].vtx.z = 0.0f;
		pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[2].vtx.z = 0.0f;
		pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
		pVtx[3].vtx.z = 0.0f;

		// rhwの設定
		pVtx[0].rhw =
		pVtx[1].rhw =
		pVtx[2].rhw =
		pVtx[3].rhw = 1.0f;

		// 反射光の設定
		pVtx[0].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[1].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[2].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[3].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2( 0.0f, 0.0f );
		pVtx[1].tex = D3DXVECTOR2( 1.0f, 0.0f );
		pVtx[2].tex = D3DXVECTOR2( 0.0f, 1.0f );
		pVtx[3].tex = D3DXVECTOR2( 1.0f, 1.0f );

		// 頂点データをアンロックする
		obj->pD3DVtxBuff->Unlock();
	}

	if( tex != NULL )
	{
		// テクスチャの読み込み
		D3DXCreateTextureFromFile( pDevice,		// デバイスのポインタ
			tex,								// ファイルの名前
			&obj->pD3DTexture );				// 読み込むメモリのポインタ
	}

	// 初期化
	obj->nAnimCount = 0;
	obj->nAnimPattern = 0;
	obj->bAnim = true;
	obj->bExist = true;

	return S_OK;
}

// 頂点データの変更
void SetVertexPolygon( tObjData* obj )
{
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 頂点座標の設定
	pVtx[0].vtx.x = obj->pos.x - sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.y = obj->pos.y - cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[0].vtx.z = 0.0f;
	pVtx[1].vtx.x = obj->pos.x + sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.y = obj->pos.y - cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[1].vtx.z = 0.0f;
	pVtx[2].vtx.x = obj->pos.x - sinf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.y = obj->pos.y + cosf(obj->fBaseAngle + obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[2].vtx.z = 0.0f;
	pVtx[3].vtx.x = obj->pos.x + sinf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.y = obj->pos.y + cosf(obj->fBaseAngle - obj->rot.y) * obj->fRadius * obj->fScale;
	pVtx[3].vtx.z = 0.0f;

	// 表示ずれ対策（これを行っても、テクスチャサイズの問題でずれる可能性はある）
	for( int i = 0; i <= 3; i++ )
	{
		pVtx[i].vtx.x -= 0.5f;
		pVtx[i].vtx.y -= 0.5f;
	}

	// 頂点データをアンロックする
	obj->pD3DVtxBuff->Unlock();
}

// 後片付け(名前が変なのは元々UninitPolygonがあったため)
void UninitPolygonAndTexture( tObjData* obj )
{
	if( obj->pD3DTexture != NULL )
	{	// テクスチャの開放
		obj->pD3DTexture->Release();
		obj->pD3DTexture = NULL;
	}

	if( obj->pD3DVtxBuff != NULL )
	{	// 頂点バッファの開放
		obj->pD3DVtxBuff->Release();
		obj->pD3DVtxBuff = NULL;
	}
}

// テクスチャを変更
HRESULT ChangeTexture( LPDIRECT3DDEVICE9 pDevice, tObjData* obj, TCHAR* filename )
{
	UninitPolygonAndTexture( obj );
	MakePolygon( pDevice, obj, filename );

	return S_OK;
}

// テクスチャアニメーション
void SetTextureAnimation( tObjData* obj )
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// アニメーション処理
	int flag = pAnim[ obj->nAnimPattern ].flag;
	obj->nAnimCount++;
	if( obj->nAnimCount >= pAnim[ obj->nAnimPattern ].frame )
	{	// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			obj->bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			obj->nAnimCount = 0;
			obj->nAnimPattern = 0;
		} else {
			obj->nAnimPattern++;
		}
	}
}

// テクスチャ設定
void SetTexturePolygon( tObjData* obj )
{
	// アニメデータがない場合何もしない
	if( obj->pAnim == NULL ) return;

	SetTextureAnimation( obj );
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(obj->pAnim))[obj->nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		obj->pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ obj->nAnimPattern ].pos;	// 場所
		float& x = obj->sizeMax.x;	// 大きさを別名に。（長いから）
		float& y = obj->sizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		obj->pD3DVtxBuff->Unlock();
	}

}

// アニメナンバーを変更
void SetAnimationNo( tObjData* obj, int no )
{
	obj->nAnimNo = no;
	obj->nAnimPattern = 0;
	obj->nAnimCount = 0;
	obj->bAnim = true;
}

// アニメーション中かどうか
bool IsPolygonAnimation( tObjData* obj, int no /*=-1*/ )
{
	// どのアニメでもいいけど、アニメしているかどうかを判定
	if( no == -1 ) return obj->bAnim;

	// 指定のアニメナンバーでアニメしているかどうか
	if( obj->bAnim && obj->nAnimNo == no )
	{
		return true;
	}
	return false;
}
