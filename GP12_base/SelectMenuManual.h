//===================================================================================
//
// セレクトメニューのマニュアル
//
//===================================================================================
#pragma once

#include "Main.h"
#include "PolygonClass.h"
#include "PolygonAnim.h"
#include "SelectMenu.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define TEXTURE_MANUAL		_T("data/TEXTURE/SelectMenuManual.png")
#define MANUAL_ARROWSIZE_X	(50)
#define MANUAL_ARROWSIZE_Y	(25)
#define MANUAL_ENTER_X		(100)
#define MANUAL_ENTER_Y		(50)

#define MAX_MANUAL			(3)

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cSelectMenuManual
{
public:
	// オブジェクト
	cPolygonClass*					mSelectMenuManual[MAX_MANUAL];
	// テクスチャの番号
	int								nTexNumber[MAX_MANUAL];
	// フラグ
	bool							bBigFlag[MAX_MANUAL];
	bool							bSmallFlag[MAX_MANUAL];	
};


//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitSelectMenuManual(LPDIRECT3DDEVICE9 pDevice);
void UninitSelectMenuManual(void);
void UpdateSelectMenuManual(void);
void DrawSelectMenuManual(LPDIRECT3DDEVICE9 pDevice);