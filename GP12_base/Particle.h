//=============================================================================
//
// パーティクル処理 [Particle.h]
//
//=============================================================================
#pragma once


#include "main.h"
#include "PolygonClass.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define TEXTURE_GAME_PARTICLE		_T("data/TEXTURE/Particle.png")	
#define PARTICLE_SIZE_X				(10) // ポリゴンサイズ
#define PARTICLE_SIZE_Y				(10) // 同上
#define MAX_PARTICLE				(100)

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tParticle
{
	D3DXVECTOR3		vel;
	float			alpha;
	int				count;
}tParticle;

//*****************************************************************************
// クラス定義
//*****************************************************************************
class cParticle
{
public:
	// オブジェクト
	cPolygonClass*			mParticle[MAX_PARTICLE];
	// パーティクル値設定用
	tParticle				tParticle[MAX_PARTICLE];
};

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT InitParticle(LPDIRECT3DDEVICE9 pDevice);
void UninitParticle(void);
void UpdateParticle(void);
void DrawParticle(LPDIRECT3DDEVICE9 pDevice);
//void SetParticle(D3DXVECTOR3 pos,int num);
void SetParticle(D3DXVECTOR3 pos,int num,int HoleCount);
void ReInitParticle(void);