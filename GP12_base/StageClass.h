//===================================================================================
//
// ステージクラス
//
//===================================================================================
#pragma once

#include "Main.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cStageClass
{
private:
	// 選択プレイヤーのナンバー
	int					m_nPlayerNum;
	// 一つ前に選択していたプレイヤーのナンバー
	int					m_nBeforePlayerNum;
	// ワープナンバー
	int					m_nWarpNum;
	// ワープを使用するか
	bool				m_bUseWarp;
	// プレイヤーの切り替え
	bool				m_bPlayerChange;
	// プレイヤーの移動
	bool				m_bPlayerMove;
	// 移動先の配列
	tStageData			m_tDecision;
	// ブロックギミックを使用するか
	bool				m_bBlocksGimmick;
	// ワープギミックを使用するか
	bool				m_bWarpGimmick;
	// フェード
	bool				m_bFade;

public:
	// コンストラクタ
	cStageClass();
	// デストラクタ
	~cStageClass();

public:
	// 選択プレイヤーのナンバーの取得
	int GetPlayerNum(void);
	// 選択プレイヤーのナンバーの設定
	void SetPlayerNum(int num);
	// 一つ前に選択していたプレイヤーのナンバーの取得
	int GetBeforePlayerNum(void);
	// 一つ前に選択していたプレイヤーのナンバーの設定
	void SetBeforePlayerNum(int num);
	// プレイヤーの切り替え状態の取得
	bool GetPlayerChange(void);
	// プレイヤーの切り替え状態の設定
	void SetPlayerChange(bool state);
	// プレイヤーの移動状態の取得
	bool GetPlayerMove(void);
	// プレイヤーの移動状態の設定
	void SetPlayerMove(bool state);
	// 移動先の配列の取得
	tStageData GetDecision(void);
	// 移動先の配列の設定
	void SetDecision(tStageData tArray);
	// プレイヤーの角度設定
	int SetPlayerAngle(int pAngle);
	// 画面遷移
	int Transition(tStageData tPlayer[],tStageData tHole[],int nPlayer,int nHole);
	// ブロックギミックの状態取得
	bool GetBlockGimmick(void);
	// ブロックギミックの使用変更
	void SetBlockGimmick(bool states);
	// ワープギミックの状態取得
	bool GetWarpGimmick(void);
	// ワープギミックの使用変更
	void SetWarpGimmick(bool states);
	// ワープナンバーの取得
	int GetWarpNumber(void);
	// ワープナンバーの設定
	void SetWarpNumber(int Num);
	// ワープを使用しているか取得
	bool GetWarpUseFlag(void);
	// ワープを使用しているかの設定
	void SetWarpUseFlag(bool states);
	// フェードの取得
	bool GetFade(void);
	// フェードの設定
	void SetFade(bool states);
};

