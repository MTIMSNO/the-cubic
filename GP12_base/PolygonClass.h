//===================================================================================
//
// ポリゴンクラス.h
//
//===================================================================================
#pragma once

#include "Main.h"
#include "PolygonAnim.h"

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cPolygonClass
{
private:
	// テクスチャへのポリゴン
	LPDIRECT3DTEXTURE9			m_pD3DTexture;
	// 頂点バッファインターフェースへのポインタ
	LPDIRECT3DVERTEXBUFFER9		m_pD3DVtxBuff;	
	// 中心座標
	D3DXVECTOR3					m_dpos;	
	// ポリゴンのサイズ
	D3DXVECTOR2					m_dsize;	
	// 画像自体のサイズ
	D3DXVECTOR2					m_dsizeMax;	
	// 回転量
	D3DXVECTOR3					m_drot;				
	// 中心から頂点への長さ
	float						m_fRadius;		
	// 中心から頂点への角度
	float						m_fBaseAngle;			
	// 拡大率
	float						m_fScale;				
	// 透過度
	float						m_fAlpha;
	// アニメーションカウント
	int							m_nAnimCount;			
	// アニメーションパターンナンバー
	int							m_nAnimPattern;		
	// アニメナンバー
	int							m_nAnimNo;			
	// アニメーション中かどうか
	bool						m_bAnim;				
	// 表示するかどうか
	bool						m_bExist;				
	// アニメーションデータ配列のポインタ(tAnimKeyData*型に変換すること)
	void*						m_pAnim;				

public:
	// コンストラクタ
	cPolygonClass();
	cPolygonClass(D3DXVECTOR2 size,D3DXVECTOR2 sizemax,bool exist);
	// デストラクタ
	~cPolygonClass();
	// 初期化処理
	HRESULT Init(LPDIRECT3DDEVICE9 pDevice , TCHAR* xfile);
	// 終了処理
	void Uninit(void);
	// 描画処理
	void Draw(LPDIRECT3DDEVICE9 pDevice);
	// 描画処理
	void Draw3D(LPDIRECT3DDEVICE9 pDevice);
	// 描画処理
	void Draw3D(LPDIRECT3DDEVICE9 pDevice,LPDIRECT3DTEXTURE9 Textuer);
	// 頂点データの変更
	void SetVertexPolygon(void);
	// テクスチャ変更
	HRESULT ChangeTexture(LPDIRECT3DDEVICE9 pDevice,TCHAR* filename);
	// テクスチャアニメーション
	void SetTextureAnimation(void);
	// テクスチャ設定
	void SetTexturePolygon(void);
	// アニメナンバー変更
	void SetAnimationNo(int num);
	// アニメーション中かどうか
	bool IsPolygonAnimation(int num);

	//****************** 3 D *************************************

	// 初期化処理
	HRESULT Init3D(LPDIRECT3DDEVICE9 pDevice , TCHAR* xfile);
	// 頂点データの変更
	void SetVertexPolygon3D(void);
	// テクスチャ設定
	void SetTexturePolygon3D(void);

public:
	// 中心座標の取得
	D3DXVECTOR3 GetPolygonPos(void);
	// 中心座標の設定
	void SetPolygonPos(D3DXVECTOR3 pos);
	// 中心座標の移動
	void SetPolygonPos(float x, float y, float z);
	// 回転角度の取得
	D3DXVECTOR3 GetPolygonRot(void);
	// 回転角度の設定
	void SetPolygonRot(D3DXVECTOR3 rot);
	// 回転の設定
	void SetPolygonRot(float x, float y, float z);
	// 拡縮の取得
	float GetPolygonScale(void);
	// 拡縮の設定
	void SetPolygonScale(float scale);
	// 拡縮設定
	void AddPolygonScale(float scale);
	// ポリゴンのサイズ取得
	D3DXVECTOR2 GetPolygonSize(void);
	// ポリゴンのサイズ設定
	void SetPolygonSize(D3DXVECTOR2 size);
	// 画像のサイズ取得
	D3DXVECTOR2 GetPolygonSizeMax(void);
	// 画像のサイズ設定
	void SetPolygonSizeMax(D3DXVECTOR2 size);
	// 透過度の取得
	float GetPolygonAlpha(void);
	// 透過度の設定
	void SetPolygonAlpha(float alpha);
	// アニメーション関係の設定
	void SetPolygonAnim(int Acount,int Apattern,int Ano);
	// 表示の取得
	bool GetPolygonExist(void);
	// 表示の設定
	void SetPolygonExist(bool exist);
	// アニメーションデータ配列のポインタ設定
	void SetPolygonPAnim(void* pAnim);
	// テクスチャの取得
	LPDIRECT3DTEXTURE9 GetTexturePolygon(void);
	// テクスチャカラーの変更
	void SetTextureColor(LPDIRECT3DDEVICE9 pDevice,D3DXCOLOR color);
};
