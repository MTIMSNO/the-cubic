//===================================================================================
//
// ライト.h
//
//===================================================================================

#include "Main.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define MAX_LIGHT				(1)

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// 初期化処理
HRESULT InitLight(LPDIRECT3DDEVICE9 pDevice);
// 更新処理
void UpdateLight(LPDIRECT3DDEVICE9 pDevice);
// 平行光源
void SetDirectional(LPDIRECT3DDEVICE9 pDevice , D3DXCOLOR color , DWORD ambi , D3DXVECTOR3 Direct);
// 点光源
void SetPoint( LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR3 pos, D3DXCOLOR color , DWORD ambi);
// スポットライト
void SetSpot( LPDIRECT3DDEVICE9 pDevice, D3DXCOLOR color , D3DXVECTOR3 pos , D3DXVECTOR3 Direct , DWORD ambi);
