//=============================================================================
//
// ビーコン処理 [Beacon.h]
//
//=============================================================================
#pragma once

#include "main.h"
#include "PolygonClass.h"

// マクロ定義
#define TEXTURE_GAME_BEACON			_T("data/TEXTURE/Beacon.png")	// サンプル用画像
#define BEACON_SIZE_X				(5) // ポリゴンサイズ
#define BEACON_SIZE_Y				(300) // 同上
#define MAX_BEACON					(10)

//*****************************************************************************
// 構造体定義
//*****************************************************************************
typedef struct _tBeacon
{
	D3DXVECTOR3		vel;
	D3DXVECTOR3		pos;
	float			alpha;
	int				count;
}tBeacon;

//*****************************************************************************
// クラス定義
//*****************************************************************************

class cBeacon
{
public:
	// オブジェクト
	cPolygonClass*			mBeacon[MAX_BEACON];
	// パーティクル値設定用
	tBeacon					tBeacon[MAX_BEACON];
	// ホールの数(保持)
	int						nHoleCount;
};

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
HRESULT InitBeacon(LPDIRECT3DDEVICE9 pDevice);
void UninitBeacon(void);
void UpdateBeacon(void);
void DrawBeacon(LPDIRECT3DDEVICE9 pDevice);
void SetBeacon(LPDIRECT3DDEVICE9 pDevice,D3DXVECTOR3 pos,int HoleCount,int num);