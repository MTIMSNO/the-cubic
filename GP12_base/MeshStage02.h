//===================================================================================
//
// メッシュステージ
//
//===================================================================================
#pragma once


#include "Main.h"
#include "StageClass.h"
#include "MeshClass.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define STAGE02_X				(11)
#define STAGE02_Y				(7)
#define STAGE02_Z				(11)
#define MAX_PLAYER02			(3)
#define MAX_HOLE02				(3)
#define MAX_BLOCK02				(2)
#define MAX_WARP02				(2)

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cMeshStage02 : public cStageClass
{
public:
	// メッシュ
	cMeshClass*			mStage02[STAGE02_Z][STAGE02_Y][STAGE02_X];
	// プレイヤー / ホール
	tStageData			Player02[MAX_PLAYER02+1];
	tStageData			Hole02[MAX_HOLE02];
	tStageData			Block02[MAX_BLOCK02];
	tStageData			Warp02[MAX_WARP02];
};


HRESULT InitMeshStage02 (LPDIRECT3DDEVICE9 pDevice);
void UninitMeshStage02(void);
void UpdateMeshStage02(void);
void DrawMeshStage02(LPDIRECT3DDEVICE9 pDevice);

int GetPlayerAngle02();
D3DXVECTOR3 GetPlayerPos02(void);
D3DXVECTOR3 GetCameraMoveVol02(void);
bool GetPlayerChange02(void);
