//===================================================================================
//
// メッシュステージ
//
//===================================================================================

#include "MeshStage02.h"
#include "Particle.h"
#include "Beacon.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// ステージ配置
void SetInitialStage02(void);
// ステージのタイプ設定
void SetStageType02(int z,int y,int x);
// ステージカラーの設定
void SetStageColor02(LPDIRECT3DDEVICE9 pDevice);
// ステージの床のカラー設定
void DirectionStageColor02(LPDIRECT3DDEVICE9 pDevice);
// 進む方向を調べる
tStageData SearchDirection02(tStageData Player,tStageData Direct);
// 角度判定
void AngleJudgement02(void);
// プレイヤーの切り替え
void ChangePlayer02(void);
// プレイヤーの移動
void MovePlayer02(void);
// オブジェクトカラーの設定
void SetStageColor02(LPDIRECT3DDEVICE9 pDevice);
// その列にあるホールの数
int GetHoleCount02(int x, int y, int z);
// 配列更新
void UpdateArray02(tStageData tArray[],int nArray,tStageData tDecision);
// 表示・非表示設定
void SetExist02(tStageData tPlayer,tStageData tDicision);
// ワープ使用時の表示・非表示設定
void SetWarpExist02(tStageData tP,tStageData tD);
// 終了判定
bool ExitStage02(void);


//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

// ポインタ作成
cMeshStage02*				pMeshStage02;

// 方向
tStageData					Direction02[4] =
							{
								{0,1,1},
								{-1,1,0},
								{0,1,-1},
								{1,1,0}
							};

//-----------------------------------------------------------------------------------
// ステージの配置
//-----------------------------------------------------------------------------------
void SetInitialStage02(void)
{
	if (GetData(DATA_SELECTSTAGE) == STAGE_ONE)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER02+1] = {
			{8,STAGE02_Y-1,6},
			{2,STAGE02_Y-1,4},
			{6,STAGE02_Y-1,8},
			{STAGE02_X/2,STAGE02_Y/2,STAGE02_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER02+1; i++ )
			pMeshStage02->Player02[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE02] = {
			{4,STAGE02_Y-2,8},
			{6,STAGE02_Y-2,2},
			{6,STAGE02_Y-3,2},
		};

		for ( int i = 0; i < MAX_HOLE02; i++ )
			pMeshStage02->Hole02[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK02] = {
			{5,STAGE02_Y-1,4},
			{5,STAGE02_Y-1,6},
		};

		for ( int i = 0; i < MAX_BLOCK02; i++ )
			pMeshStage02->Block02[i] = B[i];
	}
	else if (GetData(DATA_SELECTSTAGE) == STAGE_TWO)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER02+1] = {
			{3,STAGE02_Y-1,3},
			{7,STAGE02_Y-1,6},
			{8,STAGE02_Y-1,8},
			{STAGE02_X/2,STAGE02_Y/2,STAGE02_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER02+1; i++ )
			pMeshStage02->Player02[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE02] = {
			{4,STAGE02_Y-2,2},
			{7,STAGE02_Y-2,3},
			{7,STAGE02_Y-2,7},
		};

		for ( int i = 0; i < MAX_HOLE02; i++ )
			pMeshStage02->Hole02[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK02] = {
			{8,STAGE02_Y-1,2},
			{3,STAGE02_Y-1,8},
		};

		for ( int i = 0; i < MAX_BLOCK02; i++ )
			pMeshStage02->Block02[i] = B[i];
	}
	else if (GetData(DATA_SELECTSTAGE) == STAGE_THREE)
	{
		// プレイヤー
		tStageData P[MAX_PLAYER02+1] = {
			{4,STAGE02_Y-1,8},
			{8,STAGE02_Y-1,5},
			{6,STAGE02_Y-1,2},
			{STAGE02_X/2,STAGE02_Y/2,STAGE02_Z/2},
		};
	
		for ( int i = 0; i < MAX_PLAYER02+1; i++ )
			pMeshStage02->Player02[i] = P[i];

		// ホール
		tStageData	H[MAX_HOLE02] = {
			{2,STAGE02_Y-2,5},
			{2,STAGE02_Y-3,5},
			{4,STAGE02_Y-2,7},
		};

		for ( int i = 0; i < MAX_HOLE02; i++ )
			pMeshStage02->Hole02[i] = H[i];

		// ブロック
		tStageData B[MAX_BLOCK02] = {
			{4,STAGE02_Y-1,4},
			{6,STAGE02_Y-1,7},
		};

		for ( int i = 0; i < MAX_BLOCK02; i++ )
			pMeshStage02->Block02[i] = B[i];

	}
}

//-----------------------------------------------------------------------------------
// ステージのタイプ設定
//-----------------------------------------------------------------------------------
void SetStageType02(int z,int y,int x)
{

	// 外枠
	// Z+.Z-.X+.X-.Y-.Y+方向に配列１つ分づつ表示しない
	if ( y == 0 || y == STAGE02_Y-1 || x == 0 || x == STAGE02_X-1 || z == 0 || z == STAGE02_Z-1 )
	{	
		pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_NONE);
		pMeshStage02->mStage02[z][y][x]->SetMeshExist(false);
	}

	// プレイヤーの設定
	for ( int i = 0; i < MAX_PLAYER02+1; i++ )
	{
		if ( pMeshStage02->Player02[i].x == x && pMeshStage02->Player02[i].y == y && pMeshStage02->Player02[i].z == z )
		{
			pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_PLAYER);
			pMeshStage02->mStage02[z][y][x]->SetMeshExist(true);
			if ( i == MAX_PLAYER02 )
			{
				pMeshStage02->mStage02[z][y][x]->SetMeshMove(false);
				pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_NONE);
			}
		}
	}
	

	// ホールの設定
	for ( int i = 0; i < MAX_HOLE02; i++ )
	{
		if (pMeshStage02->Hole02[i].x == x && pMeshStage02->Hole02[i].y == y && pMeshStage02->Hole02[i].z == z )
		{
			pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_HOLE);
			pMeshStage02->mStage02[z][y][x]->SetMeshExist(true);
		}
	}

	// 各ギミックがONのとき
	// ブロック
	if ( pMeshStage02->GetBlockGimmick())
	{
		for ( int i = 0; i < MAX_BLOCK02; i++ )
		{
			if ( pMeshStage02->Block02[i].x == x && pMeshStage02->Block02[i].y == y && pMeshStage02->Block02[i].z == z )
			{
				pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_BLOCK);
				pMeshStage02->mStage02[z][y][x]->SetMeshExist(true);
			}
		}
	}
	// ワープ
	if ( pMeshStage02->GetWarpGimmick())
	{
		for ( int i = 0; i < MAX_WARP02; i++ )
		{
			if ( pMeshStage02->Warp02[i].x == x && pMeshStage02->Warp02[i].y == y && pMeshStage02->Warp02[i].z == z )
			{
				pMeshStage02->mStage02[z][y][x]->SetMeshType(STYPE_WARP);
				pMeshStage02->mStage02[z][y][x]->SetMeshExist(true);				
			}
		}
	}
}

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitMeshStage02 ( LPDIRECT3DDEVICE9 pDevice )
{
	// 動的オブジェクトの作成
	pMeshStage02 = new cMeshStage02();

	// ギミックの設定
	pMeshStage02->SetBlockGimmick(true);
	pMeshStage02->SetWarpGimmick(false);

	// ステージの配置設定
	SetInitialStage02();

	for ( int z = 0; z < STAGE02_Z; z++ )
		for ( int y = 0; y < STAGE02_Y; y++ )
			for ( int x = 0; x < STAGE02_X; x++ )
			{
				// 動的オブジェクトの作成
				pMeshStage02->mStage02[z][y][x] = new cMeshClass();

				// 位置の設定
				pMeshStage02->mStage02[z][y][x]->SetMeshPos(D3DXVECTOR3(
					(float)(-CUBESIZE*(STAGE02_Z/2))+CUBESIZE*z,
					(float)(-CUBESIZE*(STAGE02_Y/2))+CUBESIZE*y,
					(float)(-CUBESIZE*(STAGE02_X/2))+CUBESIZE*x)
					);

				// オブジェクトのタイプ設定
				SetStageType02(z,y,x);

				pMeshStage02->mStage02[z][y][x]->Init(pDevice,MESH_CUBE);

			}

	return S_OK;
}
//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitMeshStage02(void)
{
	for ( int z = 0; z < STAGE02_Z; z++ )
	for ( int y = 0; y < STAGE02_Y; y++ )
	for ( int x = 0; x < STAGE02_X; x++ )
	{
		delete pMeshStage02->mStage02[z][y][x];
	}

	delete pMeshStage02;
}
//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateMeshStage02(void)
{
	// 選択中のプレイヤー配列を取得
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	
	// 終了判定
	if ( !(ExitStage02()) && pMeshStage02->GetFade() == false )
	{
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		pMeshStage02->SetFade(true);
	}
	else if ( pMeshStage02->GetFade() && IsCheckFade() == FADE_OFF )
	{
		SetFade(0.0f,0.0f,0.0f,0.0f,15);
		SetData(DATA_MAINSTATE,pMeshStage02->Transition(pMeshStage02->Player02,pMeshStage02->Hole02,MAX_PLAYER02,MAX_HOLE02));
		pMeshStage02->SetFade(false);
	}

	// 角度設定
	if ( pMeshStage02->GetPlayerChange() == true )
		pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshAngle(
			pMeshStage02->SetPlayerAngle(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle()));
	
	// 角度判定
	if ( pMeshStage02->GetPlayerChange() == true )
		AngleJudgement02();

	// プレイヤーの切り替え
	if ( pMeshStage02->GetPlayerChange() == true )
	if( GetKeyboardTrigger(DIK_SPACE) || GetJoyStickTrigger(1) )
		ChangePlayer02();

	// プレイヤーの移動
	if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() == true )
	if ( GetKeyboardTrigger(DIK_RETURN) || GetJoyStickTrigger(2))
	{
		// 移動を可能に
		pMeshStage02->SetPlayerMove(true);
		// プレイヤーの切り替えを無効に
		pMeshStage02->SetPlayerChange(false);
	}
	MovePlayer02();

}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawMeshStage02(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int z = 0; z < STAGE02_Z; z++ )
	for ( int y = 0; y < STAGE02_Y; y++ )
	for ( int x = 0; x < STAGE02_X; x++ )
	{
		// 表示しない
		if ( pMeshStage02->mStage02[z][y][x]->GetMeshExist() == false ) continue;

		// ビーコンの設置
		if ( pMeshStage02->mStage02[z][y][x]->GetMeshType() == STYPE_HOLE )
			SetBeacon( pDevice,pMeshStage02->mStage02[z][y][x]->GetMeshPos(),GetHoleCount02(x,y,z),1);
		// 色の初期化処理
		pMeshStage02->mStage02[z][y][x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
		// ステージカラーの設定
		SetStageColor02(pDevice);
		// 進む方向の床のカラー設定
		DirectionStageColor02(pDevice);
		
		pMeshStage02->mStage02[z][y][x]->Draw(pDevice);
	}
}

//-----------------------------------------------------------------------------------
// プレイヤーの中心座標の取得
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetPlayerPos02(void)
{
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	return pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos();
}

//-----------------------------------------------------------------------------------
// プレイヤー角度の取得
//-----------------------------------------------------------------------------------
int GetPlayerAngle02()
{
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	return pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle();
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替えの取得
//-----------------------------------------------------------------------------------
bool GetPlayerChange02(void)
{
	return pMeshStage02->GetPlayerChange();
}

//-----------------------------------------------------------------------------------
// 進む方向の色の設定
//-----------------------------------------------------------------------------------
void DirectionStageColor02(LPDIRECT3DDEVICE9 pDevice)
{
	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	// 角度の取得
	int nAngle = pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle();

	// ステージカラーの設定
	switch ( nAngle / 90 )
	{
	case 0:
		for ( int i = P.z; i <= pMeshStage02->GetDecision().z; i++ )
			if ( P.y != 0 || pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage02->mStage02[i][P.y-1][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 1:
		for ( int i = P.x; i >= pMeshStage02->GetDecision().x; i-- )
			if ( P.y != 0 || pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage02->mStage02[P.z][P.y-1][i]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 2:
		for ( int i = P.z; i >= pMeshStage02->GetDecision().z; i-- )
			if ( P.y != 0 || pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage02->mStage02[i][P.y-1][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;

	case 3:
		for ( int i = P.x; i <= pMeshStage02->GetDecision().x; i++ )
			if ( P.y != 0 || pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() )
				pMeshStage02->mStage02[P.z][P.y-1][i]->SetMeshColor(pDevice,1,D3DXCOLOR(0.5f,0.5f,0.5f,1.0f));
		break;
	}
}

//-----------------------------------------------------------------------------------
// 角度判定
//-----------------------------------------------------------------------------------
void AngleJudgement02(void)
{
	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	// 角度代入
	int nAngle = pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle();
	// 移動先代入先の初期化
	tStageData D = {0,0,0};
	pMeshStage02->SetDecision(D);

	// 角度によってその方向を調べる
	switch ( nAngle / 90 )
	{
	case 0:
		P.z = P.z + 1;
		pMeshStage02->SetDecision(SearchDirection02(P,Direction02[DIR_Z_UP]));
		break;

	case 1:
		P.x = P.x - 1;
		pMeshStage02->SetDecision(SearchDirection02(P,Direction02[DIR_X_DOWN]));
		break;

	case 2:
		P.z = P.z - 1;
		pMeshStage02->SetDecision(SearchDirection02(P,Direction02[DIR_Z_DOWN]));
		break;

	case 3:
		P.x = P.x + 1;
		pMeshStage02->SetDecision(SearchDirection02(P,Direction02[DIR_X_UP]));
		break;
	}
}

//-----------------------------------------------------------------------------------
// 進む方向を調べる
// 一つ一つ見ていき条件に当てはまったらreturn
//-----------------------------------------------------------------------------------
tStageData SearchDirection02(tStageData Player,tStageData Direct)
{
	// 今見ている位置
	// カレントPos
	tStageData cP = Player;
	// 下から見るので一番下に設定
	cP.y = 1;

	while ( cP.z > 0 && cP.z < STAGE02_Z-1 && 
			cP.y > 0 && cP.y < STAGE02_Y-1 &&
			cP.x > 0 && cP.x < STAGE02_X-1 )
	{
		// 下から上に
		cP.y += Direct.y;

		//
		// ほかの要素をいかに記入
		//

		// 選択外プレイヤー or ブロック があるか
		if ( pMeshStage02->mStage02[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_PLAYER ||
			 pMeshStage02->mStage02[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_BLOCK )
		{
			pMeshStage02->SetWarpUseFlag(false);
			cP.z -= Direct.z;
			cP.x -= Direct.x;
			return cP;
		}
		// ワープはあるか
		else if ( pMeshStage02->mStage02[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_WARP )
		{
			for ( int i = 0; i < MAX_WARP02; i++ )
			{

				if ( pMeshStage02->Warp02[i].x == cP.x && pMeshStage02->Warp02[i].y == cP.y && pMeshStage02->Warp02[i].z == cP.z )
					pMeshStage02->SetWarpNumber(i);
			}
			pMeshStage02->SetWarpUseFlag(true);
			cP.y = cP.y + 1;
			return cP;
		}
		// 穴があるか
		else if ( pMeshStage02->mStage02[cP.z][cP.y][cP.x]->GetMeshType() == STYPE_HOLE )
		{
			pMeshStage02->SetWarpUseFlag(false);
			return cP;
		}

		// 一番上まで見たら見ている方向に一つ進める
		if ( cP.y >= STAGE02_Y-1 )
		{
			cP.x += Direct.x;
			cP.y = 1;
			cP.z += Direct.z;
		}
	}

	// ステージ外まで見た場合
	cP.y = 0;
	return cP;
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替え
//-----------------------------------------------------------------------------------
void ChangePlayer02(void)
{
	// 押したタイミングのプレイヤーナンバーを保持
	pMeshStage02->SetBeforePlayerNum(pMeshStage02->GetPlayerNum());

	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	// 何か所確認したかカウント
	int nCount = 0;

	// プレイヤーナンバーの増加
	do
	{
		// 増加
		pMeshStage02->SetPlayerNum(pMeshStage02->GetPlayerNum()+1);
		nCount++;

		// プレイヤー全てが穴に入っていた場合
		if ( nCount > MAX_PLAYER02 )
		{
			P = pMeshStage02->Player02[MAX_PLAYER02];
			break;
		}

		// MAXまで見たら0へ戻す
		if ( pMeshStage02->GetPlayerNum() >= MAX_PLAYER02 ) pMeshStage02->SetPlayerNum(0);

		// 現プレイヤーナンバーの配列を代入
		P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	}
	// 穴に入っているか(移動が可能かどうか)
	while ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() == false );
	
}

//-----------------------------------------------------------------------------------
// 配列の更新
// Playerの配列を移動後に変更する
//-----------------------------------------------------------------------------------
void UpdateArray02(tStageData tArray[],int nArray,tStageData tDecision)
{
	// 配列更新
	tArray[nArray] = tDecision;

	// 位置を整列
	for ( int z = 0; z < STAGE02_Z; z++ )
	for ( int y = 0; y < STAGE02_Y; y++ )
	for ( int x = 0; x < STAGE02_X; x++ )
	{
		// 位置の設定
		pMeshStage02->mStage02[z][y][x]->SetMeshPos(D3DXVECTOR3(
			(float)(-CUBESIZE*(STAGE02_Z/2))+CUBESIZE*z,
			(float)(-CUBESIZE*(STAGE02_Y/2))+CUBESIZE*y,
			(float)(-CUBESIZE*(STAGE02_X/2))+CUBESIZE*x)
			);
		// 回転角度の設定
		pMeshStage02->mStage02[z][y][x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
		if ( !(pMeshStage02->GetWarpGimmick())) 
		// スケールの設定
		pMeshStage02->mStage02[z][y][x]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));
	}
}

//-----------------------------------------------------------------------------------
// 色の設定
//-----------------------------------------------------------------------------------
void SetStageColor02(LPDIRECT3DDEVICE9 pDevice)
{
	// ホールの色の設定
	for ( int cH = 0; cH < MAX_HOLE02; cH++ )
	{
		// ホールの配列代入
		tStageData H = pMeshStage02->Hole02[cH];
		pMeshStage02->mStage02[H.z][H.y][H.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.0f,0.0f,0.0f,0.85f));
	}

	// ブロックの色の設定
	if ( pMeshStage02->GetBlockGimmick() )
	{
		for ( int cB = 0; cB < MAX_BLOCK02; cB++ )
		{
			// ブロックの配列代入
			tStageData B = pMeshStage02->Block02[cB];
			pMeshStage02->mStage02[B.z][B.y][B.x]->SetMeshColor(pDevice,0,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
			pMeshStage02->mStage02[B.z][B.y][B.x]->SetMeshColor(pDevice,1,D3DXCOLOR(0.0f,0.0f,0.0f,1.0f));
		}
	}

	// ワープの色の設定
	if ( pMeshStage02->GetWarpGimmick() )
	{
		for ( int cW = 0; cW < MAX_WARP02; cW++ )
		{
			// ワープの配列代入
			tStageData W = pMeshStage02->Warp02[cW];
			pMeshStage02->mStage02[W.z][W.y][W.x]->SetMeshColor(pDevice,D3DXCOLOR(0.0f,0.0f,0.5f,1.0f));
		}
	}

	// プレイヤーの色の設定
	for ( int cP = 0; cP < MAX_PLAYER02; cP++ )
	{
		// プレイヤーの配列代入
		tStageData P = pMeshStage02->Player02[cP];
		// 選択プレイヤー
		if ( cP == pMeshStage02->GetPlayerNum() )
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,0.2f,0.2f,1.0f));
		// 移動不可プレイヤー(穴に入っているプレイヤー)
		else if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshMove() == false )
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,1.0f,1.0f,1.0f));
		// 選択外プレイヤー
		else 
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshColor(pDevice,1,D3DXCOLOR(1.0f,0.6f,0.6f,1.0f));
	}	
}

//-----------------------------------------------------------------------------------
// ホールが何個連なっているか
//-----------------------------------------------------------------------------------
int GetHoleCount02(int x, int y, int z)
{
	int HoleCount = 0;

	for ( int cH = 0; cH < STAGE02_Y; cH++ )
	{
		if ( pMeshStage02->mStage02[z][cH][x]->GetMeshType() == STYPE_HOLE ) HoleCount++;
		else HoleCount;
	}
	return HoleCount;
}

//-----------------------------------------------------------------------------------
// 終了判定
//-----------------------------------------------------------------------------------
bool ExitStage02(void)
{
	// 一つでもステージ外にいた場合ゲーム終了
	// cP...カレントプレイヤー
	for ( int cP = 0; cP < MAX_PLAYER02; cP++ )
	{
		tStageData P = pMeshStage02->Player02[cP];
		if ( P.y == 0 )		return false;
	}

	// ステージタイプがプレイヤーであるブロックが
	// 一つでも残っていたらゲーム続行
	for ( int z = 0; z < STAGE02_Z; z++ )
	for ( int y = 0; y < STAGE02_Y; y++ )
	for ( int x = 0; x < STAGE02_X; x++ )
	{
		if ( y == STAGE02_Y-1 )
			if ( pMeshStage02->mStage02[z][y][x]->GetMeshType() == STYPE_PLAYER )		return true;
	}

	// プレイヤーが一つもいなければゲーム終了
	return false;
}

//-----------------------------------------------------------------------------------
// プレイヤーの表示・非表示設定
//-----------------------------------------------------------------------------------
void SetExist02(tStageData tPlayer,tStageData tDicision)
{
	// 配列更新
	UpdateArray02(pMeshStage02->Player02,pMeshStage02->GetPlayerNum(),pMeshStage02->GetDecision());

	// 移動前の位置を非表示
	pMeshStage02->mStage02[tPlayer.z][tPlayer.y][tPlayer.x]->SetMeshType(STYPE_NONE);
	pMeshStage02->mStage02[tPlayer.z][tPlayer.y][tPlayer.x]->SetMeshExist(false);

	// 移動後を表示

	// 移動後の位置がステージ一番上の場合
	// ブロックにぶつかったときetc...
	if ( tDicision.y == STAGE02_Y-1 )
	{
		pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_PLAYER);
		pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshExist(true);
	}
	// 移動後の位置が 穴 or ステージ外の場合
	else
	{
		// 移動できるかどうか(穴に入っているか)
		// 移動不能に設定
		pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshMove(false);

		// ステージ外のとき
		if ( tDicision.y == 0 )
		{
			pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_NONE);
			pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshExist(false);
		}
		// 穴に入るとき
		else
		{
			SetParticle(pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->GetMeshPos(),50,GetHoleCount02(tDicision.x,tDicision.y,tDicision.z));
			pMeshStage02->mStage02[tDicision.z][tDicision.y][tDicision.x]->SetMeshType(STYPE_STAGE);
		}

		// 次のプレイヤーに切り替え
		ChangePlayer02();
	}

	// この移動処理に入るか
	pMeshStage02->SetPlayerMove(false);
	// プレイヤーの切り替え
	pMeshStage02->SetPlayerChange(true);
}

//-----------------------------------------------------------------------------------
// ワープ使用時のプレイヤーの表示・非表示etc...
//-----------------------------------------------------------------------------------
void SetWarpExist02(tStageData tP,tStageData tD)
{
	if (pMeshStage02->GetWarpUseFlag() == false)		return; 

	// 配列更新
	UpdateArray02(pMeshStage02->Player02,pMeshStage02->GetPlayerNum(),pMeshStage02->GetDecision());

	// 移動前の位置を非表示
	pMeshStage02->mStage02[tP.z][tP.y][tP.x]->SetMeshType(STYPE_NONE);
	pMeshStage02->mStage02[tP.z][tP.y][tP.x]->SetMeshExist(false);

	// 移動後の位置を表示
	pMeshStage02->mStage02[tD.z][tD.y][tD.x]->SetMeshType(STYPE_PLAYER);
	pMeshStage02->mStage02[tD.z][tD.y][tD.x]->SetMeshExist(true);	

	static tStageData W;	

	// だんだん小さく・だんだん大きく
	if (pMeshStage02->mStage02[tD.z][tD.y][tD.x]->GetMeshScale().x >= 0.0f &&
		pMeshStage02->mStage02[tD.z][tD.y][tD.x]->GetMeshScale().y >= 0.0f &&
		pMeshStage02->mStage02[tD.z][tD.y][tD.x]->GetMeshScale().z >= 0.0f )
	{
		pMeshStage02->mStage02[tD.z][tD.y][tD.x]->SetMeshScale(-0.025f,-0.025f,-0.025f);
		D3DXVECTOR3 Scale = pMeshStage02->mStage02[tD.z][tD.y][tD.x]->GetMeshScale();
		// 入った方と違う方のワープの位置を代入
		W = pMeshStage02->Warp02[!(pMeshStage02->GetWarpNumber())];
		// 位置修正
		W.y = W.y + 1;
		// ワープ先を表示
		pMeshStage02->mStage02[W.z][W.y][W.x]->SetMeshType(STYPE_PLAYER);
		pMeshStage02->mStage02[W.z][W.y][W.x]->SetMeshExist(true);

		pMeshStage02->mStage02[W.z][W.y][W.x]->SetMeshScale(D3DXVECTOR3(1.0f-Scale.x,1.0f-Scale.y,1.0f-Scale.z));
	}
	else 
	{
		// 移動後の位置を非表示
		pMeshStage02->mStage02[tD.z][tD.y][tD.x]->SetMeshType(STYPE_NONE);
		pMeshStage02->mStage02[tD.z][tD.y][tD.x]->SetMeshExist(false);

		// 大きさの修正
		pMeshStage02->mStage02[W.z][W.y][W.x]->SetMeshScale(D3DXVECTOR3(1.0f,1.0f,1.0f));

		// 配列の更新
		UpdateArray02(pMeshStage02->Player02,pMeshStage02->GetPlayerNum(),W);

		// この移動処理に入るか
		pMeshStage02->SetPlayerMove(false);
		// プレイヤーの切り替え
		pMeshStage02->SetPlayerChange(true);
	}
}

//-----------------------------------------------------------------------------------
// カメラの移動量の計算
//-----------------------------------------------------------------------------------
D3DXVECTOR3 GetCameraMoveVol02(void)
{
	// 現在のプレイヤー位置
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	// 一つ前のプレイヤーの位置
	tStageData BeP = pMeshStage02->Player02[pMeshStage02->GetBeforePlayerNum()];

	// 計算
	D3DXVECTOR3 dMoveVol = 
		D3DXVECTOR3(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().x - pMeshStage02->mStage02[BeP.z][BeP.y][BeP.x]->GetMeshPos().x,
					pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().y - pMeshStage02->mStage02[BeP.z][BeP.y][BeP.x]->GetMeshPos().y,
					pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().z - pMeshStage02->mStage02[BeP.z][BeP.y][BeP.x]->GetMeshPos().z);

	return dMoveVol;
}

//-----------------------------------------------------------------------------------
// プレイヤー移動
//-----------------------------------------------------------------------------------
void MovePlayer02(void)
{
	// 移動できなければreturn 
	if ( pMeshStage02->GetPlayerMove() != true ) return;

	// 選択プレイヤーの配列代入
	tStageData P = pMeshStage02->Player02[pMeshStage02->GetPlayerNum()];
	// 移動先の位置を代入
	tStageData D = pMeshStage02->GetDecision();
	// 選択プレイヤーの角度取得
	int nAngle = pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle();

	// 代入用変数の初期化
	// cPlayerPos...カレントプレイヤーポス
	// DecisionPos...移動先
	// Distance...距離
	D3DXVECTOR3 cPlayerPos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	D3DXVECTOR3 DecisionPos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	D3DXVECTOR3 Distance = D3DXVECTOR3(0.0f,0.0f,0.0f);

	// 各変数に座標を代入
	cPlayerPos = pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos();
	DecisionPos = pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos();
	// 距離の計算
	Distance = cPlayerPos - DecisionPos;

	
	// 移動
	switch ( nAngle / 90 )
	{
	case 0:
		// 横移動
		if ( Distance.x <= 0.0f )
		{
			// プレイヤー移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,0,CUBESPEED);
			// 回転
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(-CUBEROLL,0,0);
			// 距離の減少
			Distance.x += CUBESPEED;
			// 位置・回転修正
			if ( Distance.x >= 0.0f )
			{
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().z));
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			}
		}
		// 縦移動
		if ( Distance.x >= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage02->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.x >= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos() == pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage02->mStage02[D.z][D.y][D.x]->SetMeshAngle(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage02->GetWarpUseFlag() )
				SetWarpExist02(P,D);
			else if ( pMeshStage02->GetWarpUseFlag() == false )
				SetExist02(P,D);
		}
		break;

	case 1:
		// 横移動
		if ( Distance.z >= 0.0f )
		{
			// プレイヤー移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(-CUBESPEED,0,0);
			// 回転
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(0,0,CUBEROLL);
			// 距離の減少
			Distance.z -= CUBESPEED;
			// 位置・回転修正
			if ( Distance.z <= 0.0f )
			{
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().z));
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			
			}
		}
		// 縦移動
		if ( Distance.z <= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage02->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.z <= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos() == pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos() )		
		{
			// 保持角度の受け渡し
			pMeshStage02->mStage02[D.z][D.y][D.x]->SetMeshAngle(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage02->GetWarpUseFlag() )
				SetWarpExist02(P,D);
			else if ( pMeshStage02->GetWarpUseFlag() == false )
				SetExist02(P,D);
		}
		break;

	case 2:
		// 横移動
		if ( Distance.x >= 0.0f )
		{
			// プレイヤー移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,0,-CUBESPEED);
			// 回転
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(CUBEROLL,0,0);
			// 距離の減少
			Distance.x -= CUBESPEED;
			// 位置・回転修正
			if ( Distance.x <= 0.0f )
			{
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().z));
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
			
			}
		}
		// 縦移動
		if ( Distance.x <= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage02->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().x,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.x <= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos() == pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage02->mStage02[D.z][D.y][D.x]->SetMeshAngle(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage02->GetWarpUseFlag() )
				SetWarpExist02(P,D);
			else if ( pMeshStage02->GetWarpUseFlag() == false )
				SetExist02(P,D);
		}
		break;

	case 3:
		// 横移動
		if ( Distance.z <= 0.0f )
		{
			// プレイヤー移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(CUBESPEED,0,0);
			// 回転
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(0,0,CUBEROLL);
			// 距離の増加
			Distance.z += CUBESPEED;
			// 位置・回転修正
			if ( Distance.z >= 0.0f )
			{
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().y,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().z));
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshRot(D3DXVECTOR3(0.0f,0.0f,0.0f));
		
			}
		}
		// 縦移動
		if ( Distance.z >= 0.0f && Distance.y >= 0.0f )
		{
			// ステージ外へ落ちて落ちていくとき
			if ( pMeshStage02->GetDecision().y == 0 ) {}		// サウンド再生
			// 落下スピード
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshSpeed(GRAVITY);
			// プレイヤーの移動
			pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(0,-(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed()),0);
			// 距離の減少
			Distance.y -= pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshSpeed();
			// 位置修正
			if ( Distance.y <= 0.0f )
				pMeshStage02->mStage02[P.z][P.y][P.x]->SetMeshPos(D3DXVECTOR3(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos().x,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().y,pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos().z));
		}
		// 目的の位置まで来たら
		//if ( Distance.z >= 0.0f && Distance.y <= 0.0f )
		if ( pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshPos() == pMeshStage02->mStage02[D.z][D.y][D.x]->GetMeshPos() )
		{
			// 保持角度の受け渡し
			pMeshStage02->mStage02[D.z][D.y][D.x]->SetMeshAngle(pMeshStage02->mStage02[P.z][P.y][P.x]->GetMeshAngle());
			// 表示・非表示設定			
			if ( pMeshStage02->GetWarpUseFlag() )
				SetWarpExist02(P,D);
			else if ( pMeshStage02->GetWarpUseFlag() == false )
				SetExist02(P,D);
		}
		break;
	}
}