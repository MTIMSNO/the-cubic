//===================================================================================
// ロード
//===================================================================================

#include "NowLoading.h"
#include "StageData.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
void LoadTextureScaling(void);
//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

cNowLoading*				pNowLoading;
bool						g_bLoadFlag;
bool						g_bLoadBig;
bool						g_bLoadSmall;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitNowLoading(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	pNowLoading = new cNowLoading();
	pNowLoading->mNowLoading = new cPolygonClass();

	// 位置の設定
	pNowLoading->mNowLoading->SetPolygonPos(D3DXVECTOR3((float)SCREEN_CENTER_X,(float)SCREEN_CENTER_Y,0.0f));

	// サイズ
	pNowLoading->mNowLoading->SetPolygonSize(D3DXVECTOR2((float)NOWLOADING_SIZE_X,(float)NOWLOADING_SIZE_Y));
	pNowLoading->mNowLoading->SetPolygonSizeMax(c_sizeNowLoading);
	pNowLoading->mNowLoading->SetPolygonPAnim(c_animNowLoading);

	pNowLoading->mNowLoading->Init(pDevice,TEXTURE_GAME_NOWLOADING);

	g_bLoadFlag = false;
	g_bLoadBig = true;
	g_bLoadSmall = false;
	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitNowLoading(void)
{
	delete pNowLoading->mNowLoading;
	delete pNowLoading;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateNowLoading(LPDIRECT3DDEVICE9 pDevice)
{
	pNowLoading->nLoadCount++;

	LoadTextureScaling();

	if ( pNowLoading->nLoadCount == NOWLOADINGTIME )
	{
		InitStageData(pDevice);
		SetFade(0.0f,0.0f,0.0f,1.0f,15);
		g_bLoadFlag = true;
	}
	else if ( g_bLoadFlag == true && IsCheckFade() == FADE_OFF)
	{
		SetFade(0.0f,0.0f,0.0f,0.0f,15);
		SetData(DATA_MAINSTATE,STATE_GAME);
		g_bLoadFlag = false;
		pNowLoading->nLoadCount = 0;
	}

	// アニメーションの設定
	pNowLoading->mNowLoading->SetAnimationNo(0);
	// アニメーション
	pNowLoading->mNowLoading->SetTexturePolygon();
	// ポリゴンの座標セット
	pNowLoading->mNowLoading->SetVertexPolygon();
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawNowLoading(LPDIRECT3DDEVICE9 pDevice)
{
	pNowLoading->mNowLoading->Draw(pDevice);
}

//-----------------------------------------------------------------------------------
// 拡縮
//-----------------------------------------------------------------------------------
void LoadTextureScaling(void)
{
	if ( g_bLoadBig && !(g_bLoadSmall) )
	{
		pNowLoading->mNowLoading->AddPolygonScale(0.01f);
		if ( pNowLoading->mNowLoading->GetPolygonScale() >= 1.25f )
		{
			g_bLoadBig = false;
			g_bLoadSmall = true;
		}
	}
	else if ( !(g_bLoadBig) && g_bLoadSmall )
	{
		pNowLoading->mNowLoading->AddPolygonScale(-0.01f);
		if ( pNowLoading->mNowLoading->GetPolygonScale() <= 0.75f )
		{
			g_bLoadBig = true;
			g_bLoadSmall = false;
		}
	}
}