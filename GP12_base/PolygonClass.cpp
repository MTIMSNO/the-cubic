//===================================================================================
//
// ポリゴンクラス.cpp
//
//===================================================================================

#include "PolygonClass.h"

//===================================================================================
// コンストラクタ
//===================================================================================
cPolygonClass::cPolygonClass()
{
	m_pD3DTexture = NULL;
	m_pD3DVtxBuff = NULL;
	// 中心から頂点への探さ
	m_fRadius = 1.0f;
	// 中心から頂点への角度
	m_fBaseAngle = 1.0f;

	
	// 中心座標
	m_dpos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// 回転角度
	m_drot = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// ポリゴンサイズ
	m_dsize = D3DXVECTOR2(1.0f,1.0f);
	// 画像のサイズ
	m_dsizeMax = D3DXVECTOR2(1.0f,1.0f);
	// スケール
	m_fScale = 1.0f;
	// 透過度
	m_fAlpha = 1.0f;
	// アニメーションカウント
	m_nAnimCount = 0;
	// アニメーションパターンナンバー
	m_nAnimPattern = 0;
	// アニメーションナンバー
	m_nAnimNo = 0;
	// アニメーション中か
	m_bAnim = false;
	// 表示するか
	m_bExist = false;
	// アニメーションデータ配列のポインタ
	m_pAnim = NULL;
}

cPolygonClass::cPolygonClass(D3DXVECTOR2 size,D3DXVECTOR2 sizemax,bool exist)
{
	m_pD3DTexture = NULL;
	m_pD3DVtxBuff = NULL;
	// 中心から頂点への探さ
	m_fRadius = 1.0f;
	// 中心から頂点への角度
	m_fBaseAngle = 1.0f;
	
	// 中心座標
	m_dpos = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// 回転角度
	m_drot = D3DXVECTOR3(0.0f,0.0f,0.0f);
	// ポリゴンサイズ
	m_dsize = size;
	// 画像のサイズ
	m_dsizeMax = sizemax;
	// スケール
	m_fScale = 1.0f;
	// 透過度
	m_fAlpha = 1.0f;
	// アニメーションカウント
	m_nAnimCount = 0;
	// アニメーションパターンナンバー
	m_nAnimPattern = 0;
	// アニメーションナンバー
	m_nAnimNo = 0;
	// アニメーション中か
	m_bAnim = false;
	// 表示するか
	m_bExist = exist;
	// アニメーションデータ配列のポインタ
	m_pAnim = NULL;
}

//===================================================================================
// デストラクタ
//===================================================================================
cPolygonClass::~cPolygonClass()
{
	this->Uninit();
}

//---------------------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------------------
HRESULT cPolygonClass::Init(LPDIRECT3DDEVICE9 pDevice,TCHAR* tex /*= NULL*/ )
{
    // オブジェクトの頂点バッファを生成
	if( FAILED( pDevice->CreateVertexBuffer( sizeof(VERTEX_2D)*NUM_VERTEX,	// 頂点データ用に確保するバッファサイズ（バイト単位）
												D3DUSAGE_WRITEONLY,			// 頂点バッファの使用法
												FVF_VERTEX_2D,				// 使用する頂点フォーマット
												D3DPOOL_MANAGED,			// リソースのバッファを保持するメモリクラスを指定
												&m_pD3DVtxBuff,				// 頂点バッファインターフェースへのポインタ
												NULL ) ) )					// NULL固定
	{
		return E_FAIL;
	}

	{	// 頂点バッファの中身を埋める
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
		// 基本値を設定
		m_fRadius = sqrtf(m_dsize.x * m_dsize.x + m_dsize.y * m_dsize.y) / 2.0f;
		m_fBaseAngle = atan2f(m_dsize.x, m_dsize.y);

		// 頂点座標の設定
		pVtx[0].vtx.x = m_dpos.x - sinf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
		pVtx[0].vtx.y = m_dpos.y - cosf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
		pVtx[0].vtx.z = 0.0f;
		pVtx[1].vtx.x = m_dpos.x + sinf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
		pVtx[1].vtx.y = m_dpos.y - cosf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
		pVtx[1].vtx.z = 0.0f;
		pVtx[2].vtx.x = m_dpos.x - sinf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
		pVtx[2].vtx.y = m_dpos.y + cosf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
		pVtx[2].vtx.z = 0.0f;
		pVtx[3].vtx.x = m_dpos.x + sinf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
		pVtx[3].vtx.y = m_dpos.y + cosf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
		pVtx[3].vtx.z = 0.0f;

		// rhwの設定
		pVtx[0].rhw =
		pVtx[1].rhw =
		pVtx[2].rhw =
		pVtx[3].rhw = 1.0f;

		// 反射光の設定
		pVtx[0].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[1].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[2].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[3].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2( 0.0f, 0.0f );
		pVtx[1].tex = D3DXVECTOR2( 1.0f, 0.0f );
		pVtx[2].tex = D3DXVECTOR2( 0.0f, 1.0f );
		pVtx[3].tex = D3DXVECTOR2( 1.0f, 1.0f );

		// 頂点データをアンロックする
		m_pD3DVtxBuff->Unlock();
	}

	if( tex != NULL )
	{
		// テクスチャの読み込み
		D3DXCreateTextureFromFile( pDevice,		// デバイスのポインタ
			tex,								// ファイルの名前
			&m_pD3DTexture );				// 読み込むメモリのポインタ
	}

	// 初期化
	m_nAnimCount = 0;
	m_nAnimPattern = 0;
	m_bAnim = true;
	m_bExist = true;

	return S_OK;
}

//---------------------------------------------------------------------------------------
// 頂点データの変更
//---------------------------------------------------------------------------------------
void cPolygonClass::SetVertexPolygon(void)
{
	VERTEX_2D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	// 頂点座標の設定
	pVtx[0].vtx.x = m_dpos.x - sinf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
	pVtx[0].vtx.y = m_dpos.y - cosf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
	pVtx[0].vtx.z = 0.0f;
	pVtx[1].vtx.x = m_dpos.x + sinf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
	pVtx[1].vtx.y = m_dpos.y - cosf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
	pVtx[1].vtx.z = 0.0f;
	pVtx[2].vtx.x = m_dpos.x - sinf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
	pVtx[2].vtx.y = m_dpos.y + cosf(m_fBaseAngle + m_drot.y) * m_fRadius * m_fScale;
	pVtx[2].vtx.z = 0.0f;
	pVtx[3].vtx.x = m_dpos.x + sinf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
	pVtx[3].vtx.y = m_dpos.y + cosf(m_fBaseAngle - m_drot.y) * m_fRadius * m_fScale;
	pVtx[3].vtx.z = 0.0f;

	// 表示ずれ対策（これを行っても、テクスチャサイズの問題でずれる可能性はある）
	for( int i = 0; i <= 3; i++ )
	{
		pVtx[i].vtx.x -= 0.5f;
		pVtx[i].vtx.y -= 0.5f;
	}

	// 頂点データをアンロックする
	m_pD3DVtxBuff->Unlock();
}

//---------------------------------------------------------------------------------------
// 終了処理
//---------------------------------------------------------------------------------------
void cPolygonClass::Uninit(void)
{
	if( m_pD3DTexture != NULL )
	{	// テクスチャの開放
		m_pD3DTexture->Release();
		m_pD3DTexture = NULL;
	}

	if( m_pD3DVtxBuff != NULL )
	{	// 頂点バッファの開放
		m_pD3DVtxBuff->Release();
		m_pD3DVtxBuff = NULL;
	}
}

//---------------------------------------------------------------------------------------
// 描画処理
//---------------------------------------------------------------------------------------
void cPolygonClass::Draw(LPDIRECT3DDEVICE9 pDevice)
{
	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, m_pD3DVtxBuff, 0, sizeof(VERTEX_2D) );

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D);

	// テクスチャの設定
	pDevice->SetTexture( 0, m_pD3DTexture );
	
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
}

//---------------------------------------------------------------------------------------
// 描画処理
//---------------------------------------------------------------------------------------
void cPolygonClass::Draw3D(LPDIRECT3DDEVICE9 pDevice)
{
	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, m_pD3DVtxBuff, 0, sizeof(VERTEX_3D) );

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	// ワールドトランスフォーム（絶対座標変換）
	D3DXMATRIXA16 matWorld, matRY, matTran;

	D3DXMatrixIdentity( &matWorld );									//初期化
	D3DXMatrixRotationY( &matRY, m_drot.y );							//ローテーションY
	D3DXMatrixTranslation( &matTran, m_dpos.x, m_dpos.y, m_dpos.z );	//平行移動

	D3DXMatrixMultiply( &matWorld, &matWorld, &matRY );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matTran );
	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	// テクスチャの設定
	pDevice->SetTexture( 0, m_pD3DTexture );
	
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
}

//---------------------------------------------------------------------------------------
// 描画処理
//---------------------------------------------------------------------------------------
void cPolygonClass::Draw3D(LPDIRECT3DDEVICE9 pDevice,LPDIRECT3DTEXTURE9 Textuer)
{
	// 頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource( 0, m_pD3DVtxBuff, 0, sizeof(VERTEX_3D) );

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);

	// ワールドトランスフォーム（絶対座標変換）
	D3DXMATRIXA16 matWorld, matRY, matTran;

	D3DXMatrixIdentity( &matWorld );									//初期化
	D3DXMatrixRotationY( &matRY, m_drot.y );							//ローテーションY
	D3DXMatrixTranslation( &matTran, m_dpos.x, m_dpos.y, m_dpos.z );	//平行移動

	D3DXMatrixMultiply( &matWorld, &matWorld, &matRY );
	D3DXMatrixMultiply( &matWorld, &matWorld, &matTran );
	pDevice->SetTransform( D3DTS_WORLD, &matWorld );

	// テクスチャの設定
	pDevice->SetTexture( 0, Textuer );
	
	// ポリゴンの描画
	pDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON );
}

//---------------------------------------------------------------------------------------
// テクスチャ変更
//---------------------------------------------------------------------------------------
HRESULT cPolygonClass::ChangeTexture(LPDIRECT3DDEVICE9 pDevice,TCHAR* filename)
{
	this->Uninit();
	this->Init( pDevice,filename );

	return S_OK;
}

//---------------------------------------------------------------------------------------
// テクスチャアニメーション
//---------------------------------------------------------------------------------------
void cPolygonClass::SetTextureAnimation(void)
{
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(m_pAnim))[m_nAnimNo];

	// アニメーション処理
	int flag = pAnim[ m_nAnimPattern ].flag;
	m_nAnimCount++;
	if( m_nAnimCount >= pAnim[ m_nAnimPattern ].frame )
	{	// 次の絵は何枚目かを決定する
		if( flag == ANIM_FLAG_STOP )
		{
			m_bAnim = false;
		} else if( flag == ANIM_FLAG_LOOP )
		{
			m_nAnimCount = 0;
			m_nAnimPattern = 0;
		} else {
			m_nAnimPattern++;
		}
	}
}

//---------------------------------------------------------------------------------------
// テクスチャ設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetTexturePolygon(void)
{
	// アニメデータがない場合何もしない
	if( m_pAnim == NULL ) return;

	SetTextureAnimation();
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(m_pAnim))[m_nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_2D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ m_nAnimPattern ].pos;	// 場所
		float& x = m_dsizeMax.x;	// 大きさを別名に。（長いから）
		float& y = m_dsizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		m_pD3DVtxBuff->Unlock();
	}
}

//---------------------------------------------------------------------------------------
// アニメナンバー変更
//---------------------------------------------------------------------------------------
void cPolygonClass::SetAnimationNo(int num)
{
	m_nAnimNo = num;
	m_nAnimPattern = 0;
	m_nAnimCount = 0;
	m_bAnim = true;
}

//---------------------------------------------------------------------------------------
// アニメーション中かどうか
//---------------------------------------------------------------------------------------
bool cPolygonClass::IsPolygonAnimation(int num)
{
	// どのアニメでもいいけど、アニメしているかどうかを判定
	if( num == -1 ) return m_bAnim;

	// 指定のアニメナンバーでアニメしているかどうか
	if( m_bAnim && m_nAnimNo == num )
	{
		return true;
	}
	return false;


}

//---------------------------------------------------------------------------------------
// テクスチャカラー変更
//---------------------------------------------------------------------------------------
void cPolygonClass::SetTextureColor(LPDIRECT3DDEVICE9 pDevice,D3DXCOLOR color)
{
	VERTEX_3D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	for ( int i = 0; i < 4; i++ )
	{
		D3DXCOLOR temp = color;
		temp.a = m_fAlpha;
		pVtx[i].diffuse = temp;
	}

	// 頂点データをアンロックする
	m_pD3DVtxBuff->Unlock();

}

//************************************** 3 D ************************************


//---------------------------------------------------------------------------------------
// 初期化処理
//---------------------------------------------------------------------------------------
HRESULT cPolygonClass::Init3D(LPDIRECT3DDEVICE9 pDevice, TCHAR* tex)
{
	// オブジェクトの頂点バッファを生成
	if( FAILED( pDevice->CreateVertexBuffer( sizeof(VERTEX_3D)*NUM_VERTEX,	// 頂点データ用に確保するバッファサイズ（バイト単位）
												D3DUSAGE_WRITEONLY,			// 頂点バッファの使用法
												FVF_VERTEX_3D,				// 使用する頂点フォーマット
												D3DPOOL_MANAGED,			// リソースのバッファを保持するメモリクラスを指定
												&m_pD3DVtxBuff,			// 頂点バッファインターフェースへのポインタ
												NULL ) ) )					// NULL固定
	{
		return E_FAIL;
	}

	{	// 頂点バッファの中身を埋める
		VERTEX_3D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );
		
		// 基本値を設定
		m_fRadius = sqrtf(m_dsize.x * m_dsize.x + m_dsize.y * m_dsize.y) / 2.0f;
		m_fBaseAngle = atan2f(m_dsize.x, m_dsize.y);

		// 頂点座標の設定
		pVtx[0].vtx.z = -m_dsize.x/2.0f;
		pVtx[0].vtx.y = m_dsize.y/2.0f;
		pVtx[0].vtx.x = 0.0f;
		pVtx[1].vtx.z = m_dsize.x/2.0f;
		pVtx[1].vtx.y = m_dsize.y/2.0f;
		pVtx[1].vtx.x = 0.0f;
		pVtx[2].vtx.z = -m_dsize.x/2.0f;
		pVtx[2].vtx.y = -m_dsize.y/2.0f;
		pVtx[2].vtx.x = 0.0f;
		pVtx[3].vtx.z = m_dsize.x/2.0f;
		pVtx[3].vtx.y = -m_dsize.y/2.0f;
		pVtx[3].vtx.x = 0.0f;

		// rhwの設定
		pVtx[0].normal =
		pVtx[1].normal =
		pVtx[2].normal =
		pVtx[3].normal = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		// 反射光の設定
		pVtx[0].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[1].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[2].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		pVtx[3].diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2( 0.0f, 0.0f );
		pVtx[1].tex = D3DXVECTOR2( 1.0f, 0.0f );
		pVtx[2].tex = D3DXVECTOR2( 0.0f, 1.0f );
		pVtx[3].tex = D3DXVECTOR2( 1.0f, 1.0f );

		// 頂点データをアンロックする
		m_pD3DVtxBuff->Unlock();
	}

	if( tex != NULL )
	{
		// テクスチャの読み込み
		D3DXCreateTextureFromFile( pDevice,		// デバイスのポインタ
			tex,								// ファイルの名前
			&m_pD3DTexture );				// 読み込むメモリのポインタ
	}

	// 初期化
	m_nAnimCount = 0;
	m_nAnimPattern = 0;
	m_bAnim = true;
	m_fAlpha = 1.0f;

	return S_OK;
}

//---------------------------------------------------------------------------------------
// 頂点データの変更
//---------------------------------------------------------------------------------------
void cPolygonClass::SetVertexPolygon3D(void)
{
	VERTEX_3D* pVtx;

	// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
	m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

	for ( int i = 0; i < 4; i++ )
	{
		D3DXCOLOR temp = pVtx[i].diffuse;
		temp.a = m_fAlpha;
		pVtx[i].diffuse = temp;
	}

	// 頂点データをアンロックする
	m_pD3DVtxBuff->Unlock();

}

//---------------------------------------------------------------------------------------
// テクスチャ変更
//---------------------------------------------------------------------------------------
void cPolygonClass::SetTexturePolygon3D(void)
{
	// アニメデータがない場合何もしない
	if( m_pAnim == NULL ) return;

	SetTextureAnimation();
	// アニメーションデータのポインタだけ
	tAnimKeyData* pAnim = ((tAnimKeyData**)(m_pAnim))[m_nAnimNo];

	// テクスチャ座標設定
	{
		VERTEX_3D* pVtx;

		// 頂点データの範囲をロックし、頂点バッファへのポインタを取得
		m_pD3DVtxBuff->Lock( 0, 0, (void**)&pVtx, 0 );

		// テクスチャ座標の計算
		RECT rect = pAnim[ m_nAnimPattern ].pos;	// 場所
		float& x = m_dsizeMax.x;	// 大きさを別名に。（長いから）
		float& y = m_dsizeMax.y;

		pVtx[0].tex = D3DXVECTOR2( rect.left / x , rect.top / y );
		pVtx[1].tex = D3DXVECTOR2( rect.right / x, rect.top / y );
		pVtx[2].tex = D3DXVECTOR2( rect.left / x , rect.bottom / y );
		pVtx[3].tex = D3DXVECTOR2( rect.right / x, rect.bottom / y );

		// アンロック
		m_pD3DVtxBuff->Unlock();
	}
}

//---------------------------------------------------------------------------------------
// 中心座標の取得
//---------------------------------------------------------------------------------------
D3DXVECTOR3 cPolygonClass::GetPolygonPos(void)
{
	return m_dpos;
}

//---------------------------------------------------------------------------------------
// 中心座標の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonPos(D3DXVECTOR3 pos)
{
	m_dpos = pos;
}

//---------------------------------------------------------------------------------------
// 中心座標の移動
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonPos(float x, float y, float z)
{
	m_dpos.x += x; m_dpos.y += y; m_dpos.z += z;
}

//---------------------------------------------------------------------------------------
// 回転角度の取得
//---------------------------------------------------------------------------------------
D3DXVECTOR3 cPolygonClass::GetPolygonRot(void)
{
	return m_drot;
}

//---------------------------------------------------------------------------------------
// 回転角度の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonRot(D3DXVECTOR3 rot)
{
	m_drot = rot;
}

//---------------------------------------------------------------------------------------
// 回転の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonRot(float x, float y, float z)
{
	m_drot.x += x; m_drot.y += y; m_drot.z += z;
}

//---------------------------------------------------------------------------------------
// 拡縮の取得
//---------------------------------------------------------------------------------------
float cPolygonClass::GetPolygonScale(void)
{
	return m_fScale;
}

//---------------------------------------------------------------------------------------
// 拡縮の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonScale(float scale)
{
	m_fScale = scale;
}

//---------------------------------------------------------------------------------------
// 拡縮の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::AddPolygonScale(float scale)
{
	m_fScale += scale;
}

//---------------------------------------------------------------------------------------
// ポリゴンサイズの取得
//---------------------------------------------------------------------------------------
D3DXVECTOR2 cPolygonClass::GetPolygonSize(void)
{
	return m_dsize;
}

//---------------------------------------------------------------------------------------
// ポリゴンサイズの設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonSize(D3DXVECTOR2 size)
{
	m_dsize = size;
}

//---------------------------------------------------------------------------------------
// 画像サイズの取得
//---------------------------------------------------------------------------------------
D3DXVECTOR2 cPolygonClass::GetPolygonSizeMax(void)
{
	return m_dsizeMax;
}

//---------------------------------------------------------------------------------------
// 画像サイズの設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonSizeMax(D3DXVECTOR2 size)
{
	m_dsizeMax = size;
}

//---------------------------------------------------------------------------------------
// 透過度の取得
//---------------------------------------------------------------------------------------
float cPolygonClass::GetPolygonAlpha(void)
{
	return m_fAlpha;
}

//---------------------------------------------------------------------------------------
// 透過度の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonAlpha(float alpha)
{
	m_fAlpha = alpha;
}

//---------------------------------------------------------------------------------------
// アニメーション関係の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonAnim(int Acount,int Apattern,int Ano)
{
	m_nAnimCount = Acount;	m_nAnimPattern = Apattern;	m_nAnimNo = Ano;
}

//---------------------------------------------------------------------------------------
// 表示の取得
//---------------------------------------------------------------------------------------
bool cPolygonClass::GetPolygonExist(void)
{
	return m_bExist;
}

//---------------------------------------------------------------------------------------
// 表示の設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonExist(bool exist)
{
	m_bExist = exist;
}

//---------------------------------------------------------------------------------------
// アニメーションデータ配列のポインタ設定
//---------------------------------------------------------------------------------------
void cPolygonClass::SetPolygonPAnim(void* pAnim)
{
	m_pAnim = pAnim;
}

//---------------------------------------------------------------------------------------
// テクスチャの取得
//---------------------------------------------------------------------------------------
LPDIRECT3DTEXTURE9 cPolygonClass::GetTexturePolygon(void)
{
	return m_pD3DTexture;
}
