//===================================================================================
// ロード
//===================================================================================

#include "Main.h"
#include "PolygonAnim.h"
#include "PolygonClass.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------
#define TEXTURE_GAME_NOWLOADING	_T("data/TEXTURE/NowLoading.png")
#define NOWLOADING_SIZE_X			(256) 
#define NOWLOADING_SIZE_Y			(256) 
#define NOWLOADINGTIME				(300)

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cNowLoading
{
public:
	// オブジェクト
	cPolygonClass*				mNowLoading;
	// カウント
	int							nLoadCount;
	// テクスチャナンバー
	int							nTexture;
};

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitNowLoading(LPDIRECT3DDEVICE9 pDevice);
void UninitNowLoading(void);
void UpdateNowLoading(LPDIRECT3DDEVICE9 pDevice);
void DrawNowLoading(LPDIRECT3DDEVICE9 pDevice);
