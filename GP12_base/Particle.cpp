//=============================================================================
//
// ポリゴン処理 [polygon.cpp]
//
//=============================================================================

#include "Particle.h"
#include "Input.h"
#include "PolygonAnim.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// グローバル変数
//*****************************************************************************

cParticle*			p_Particle;

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitParticle(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	p_Particle = new cParticle();

	for ( int i = 0; i < MAX_PARTICLE; i++ )
	{
		// オブジェクトの作成
		p_Particle->mParticle[i] = new cPolygonClass(
			D3DXVECTOR2((float)PARTICLE_SIZE_X,(float)PARTICLE_SIZE_Y),
			c_sizeParticle,false);

		// アニメーションデータの設定
		p_Particle->mParticle[i]->SetPolygonPAnim(c_animParticle);

		// 設定用変数の初期化
		p_Particle->tParticle[i].alpha = 1.0f;
		p_Particle->tParticle[i].count = 0;
		p_Particle->tParticle[i].vel = D3DXVECTOR3(0.0f,0.0f,0.0f);

		// ポリゴン作成
		if ( i == 0 )
			p_Particle->mParticle[i]->Init3D(pDevice,TEXTURE_GAME_PARTICLE);
		else
			p_Particle->mParticle[i]->Init3D(pDevice,NULL);
	}
	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void UninitParticle(void)
{
	for ( int i = 0; i < MAX_PARTICLE; i++ )
		delete p_Particle->mParticle[i];

	delete p_Particle;
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdateParticle(void)
{
	for ( int i = 0; i < MAX_PARTICLE; i++ )
	{
		// 非表示
		if ( p_Particle->mParticle[i]->GetPolygonPos().y >= 400.0f )
			p_Particle->mParticle[i]->SetPolygonExist(false);

		// パーティクルの移動
		p_Particle->mParticle[i]->SetPolygonPos(
			p_Particle->tParticle[i].vel.x,
			p_Particle->tParticle[i].vel.y,
			p_Particle->tParticle[i].vel.z
			);

		// パーティクルの透過度
		p_Particle->tParticle[i].alpha -= 1.0f/20.0f;
		p_Particle->mParticle[i]->SetPolygonAlpha(p_Particle->tParticle[i].alpha);

		// ポリゴン座標のセット
		p_Particle->mParticle[i]->SetVertexPolygon3D();
		p_Particle->mParticle[i]->SetTexturePolygon3D();
	}
}

//=============================================================================
// 描画処理
//=============================================================================
void DrawParticle(LPDIRECT3DDEVICE9 pDevice)
{
	// 加算合成
	pDevice->SetRenderState ( D3DRS_DESTBLEND , D3DBLEND_ONE );

	//透明色部分を正しく表示する設定
	pDevice->SetRenderState ( D3DRS_ALPHAREF , 0x00000080 );
	pDevice->SetRenderState ( D3DRS_ALPHATESTENABLE , TRUE );
	pDevice->SetRenderState ( D3DRS_ALPHAFUNC , D3DCMP_GREATER );

	pDevice->SetRenderState ( D3DRS_ZENABLE , FALSE );

	for ( int i = 0; i < MAX_PARTICLE; i++ )
	{
		if ( p_Particle->mParticle[i]->GetPolygonExist() == false ) continue;

		// 描画
		p_Particle->mParticle[i]->Draw3D(pDevice,p_Particle->mParticle[0]->GetTexturePolygon());
	}

	pDevice-> SetRenderState ( D3DRS_ALPHATESTENABLE , FALSE );

	// 復帰
	pDevice-> SetRenderState (D3DRS_DESTBLEND , D3DBLEND_INVSRCALPHA );
	pDevice-> SetRenderState (D3DRS_ZENABLE , TRUE);
}

//=============================================================================
// パーティクルの設置
//=============================================================================
void SetParticle(D3DXVECTOR3 pos,int num,int HoleCount)
{
	for ( int count = num; count >= 0; count-- )
	{
		for ( int i = 0; i < MAX_PARTICLE; i++ )
		{
			if ( p_Particle->mParticle[i]->GetPolygonExist() ) continue;

			// ビルボード設定
			float y = -atan2f(GetCameraEyePt().z - GetCameraLook().z,GetCameraEyePt().x - GetCameraLook().x);
			p_Particle->mParticle[i]->SetPolygonRot(D3DXVECTOR3(0.0f,y,0.0f));
			// 表示
			p_Particle->mParticle[i]->SetPolygonExist(true);
			// 位置
			p_Particle->mParticle[i]->SetPolygonPos(pos);
			// 透過度
			p_Particle->tParticle[i].alpha = 1.0f;
			p_Particle->mParticle[i]->SetPolygonAlpha(1.0f);
			// 速度
			p_Particle->tParticle[i].vel.x = rand()% 100/10.0f - 5;
			p_Particle->tParticle[i].vel.y = rand()% 100/10.0f + 2;
			p_Particle->tParticle[i].vel.z = rand()% 100/10.0f - 5;

			switch ( HoleCount )
			{
			case 1:
				p_Particle->mParticle[i]->SetAnimationNo(ANIM_PARTICLE00);
				break;

			case 2:
				p_Particle->mParticle[i]->SetAnimationNo(ANIM_PARTICLE01);
				break;

			case 3:
				p_Particle->mParticle[i]->SetAnimationNo(ANIM_PARTICLE02);
				break;
			}
			break;
		}
	}
}

//=============================================================================
// 再初期化
//=============================================================================
void ReInitParticle(void)
{
	for ( int i = 0; i < MAX_PARTICLE; i++ )
	{
		// 位置の修正
		p_Particle->mParticle[i]->SetPolygonPos(D3DXVECTOR3(0.0f,0.0f,0.0f));
		// 表示設定
		p_Particle->mParticle[i]->SetPolygonExist(false);

		// 設定用変数の初期化
		p_Particle->tParticle[i].alpha = 1.0f;
		p_Particle->tParticle[i].count = 0;
		p_Particle->tParticle[i].vel = D3DXVECTOR3(0.0f,0.0f,0.0f);
	}
}