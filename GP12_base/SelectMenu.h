//===================================================================================
//
// セレクトメニュー[SelectMenu.h]
//
//===================================================================================
#pragma once

#include "Main.h"
#include "MeshClass.h"
#include "Tween.h"
#include "DiffusionBlocks.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define MAX_SELECTMENU				(3)
#define MAX_LEVEL					(3)
#define MAX_STAGE					(3)
#define SPEED						(120)
#define ROLLRIGHT					(-1)
#define ROLLLEFT					(1)

//-----------------------------------------------------------------------------------
// 列挙型定義
//-----------------------------------------------------------------------------------

enum SELECTMENUSTATES
{
	STATE_SELECTLEVEL,
	STATE_SELECTSTAGE,
	STATE_SELECTMENU,
};

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cSelectMenu
{
public:
	// オブジェクト
	cMeshClass*					mSelectLevel[MAX_LEVEL];
	cMeshClass*					mSelectStage[MAX_STAGE];
	// 選択中のメニュー・レベル・ステージ
	int							nSelectMenu;
	int							nSelectLevel;
	int							nSelectStage;
	// 回転角度
	float						fSelectAngle;
	// 距離
	int							nSelectDistance;
	// 回転フラグ
	bool						bRollRight;
	bool						bRollLeft;
	// 回転カウント
	int							nCount;
	// フェードフラグ
	bool						bFade;
};

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitSelectMenu(LPDIRECT3DDEVICE9 pDevice);
void UninitSelectMenu(void);
void UpdateSelectMenu(void);
void DrawSelectMenu(LPDIRECT3DDEVICE9 pDevice);
int GetSelectMenuInfo(int i);
bool GetSelectMenuRoll(void);