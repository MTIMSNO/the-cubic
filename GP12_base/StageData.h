//===================================================================================
//
// ステージデータ
//
//===================================================================================

#include "Main.h"
#include "MeshStage01.h"
#include "MeshStage02.h"
#include "MeshStage03.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitStageData(LPDIRECT3DDEVICE9 pDevice);
void UninitStageData(void);
void UpdateStageData(void);
void DrawStageData(LPDIRECT3DDEVICE9 pDevice);
int GetPlayerAngle(void);
D3DXVECTOR3 GetPlayerPos(void);
D3DXVECTOR3 GetCameraMoveVec(void);
bool GetPlayerChange(void);
