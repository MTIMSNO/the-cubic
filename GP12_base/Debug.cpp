//===================================================================================
//
// デバック
//
//===================================================================================

#include "Debug.h"

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

// デバックON/OFF
bool					g_bDebug;
// デバック
LPD3DXFONT				g_lDebug = NULL;

// バージョン
LPD3DXFONT				g_lVersion = NULL;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitDebug(LPDIRECT3DDEVICE9 pDevice)
{
	// デバックON/OFF
	g_bDebug = true;

	// 情報表示用フォントを設定
	D3DXCreateFont( pDevice, 15, 0, 0, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Terminal"), &g_lDebug );

	D3DXCreateFont( pDevice, 16, 0, 0, 0, FALSE, SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Dotum"), &g_lVersion );

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateDebug(void)
{
	if ( GetKeyboardTrigger(DIK_F1) )	g_bDebug = !(g_bDebug);
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawDebug(LPDIRECT3DDEVICE9 pDevice)
{
	if ( !(g_bDebug) ) return;

	RECT rect = { 5, 5, SCREEN_WIDTH, SCREEN_HEIGHT };
	TCHAR str[256];

	wsprintf( str, _T("MAIN STATE:%d \nSTAGELEVEL:%d \nSELECTSTAGE:%d \n"),GetData(DATA_MAINSTATE),GetSelectMenuInfo(0),GetSelectMenuInfo(1));

	// テキスト描画
	g_lDebug->DrawText( NULL, str, -1, &rect, DT_LEFT, D3DCOLOR_ARGB( 0xff, 0x00, 0x00, 0x00 ) );
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawVersion(LPDIRECT3DDEVICE9 pDevice)
{
	RECT rect = { 5, 5, SCREEN_WIDTH, SCREEN_HEIGHT };
	TCHAR str[256];

	wsprintf( str, _T("Version:3.0"));

	// テキスト描画
	g_lVersion->DrawText( NULL, str, -1, &rect, DT_LEFT, D3DCOLOR_ARGB( 0xcc, 0xff, 0xff, 0xff ) );
}
