//=============================================================================
//
// ベースプログラム [main.h]
//
//=============================================================================
#ifndef ___MAIN_H___
#define ___MAIN_H___

#include "windows.h"
#include "d3dx9.h"
#include <tchar.h>

#include "Input.h"
#include "Light.h"
#include "Camera.h"
#include "Data.h"
#include "Fade.h"

#define DIRECTINPUT_VERSION 0x0800		// 警告対処
#include "dinput.h"
#include "mmsystem.h"

#if 1	// [ここを"0"にした場合、"構成プロパティ" -> "リンカ" -> "入力" -> "追加の依存ファイル"に対象ライブラリを設定する]
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "winmm.lib")
#endif

#define SCREEN_WIDTH	(800)				// ウインドウの幅
#define SCREEN_HEIGHT	(600)				// ウインドウの高さ
#define SCREEN_CENTER_X	(SCREEN_WIDTH / 2)	// ウインドウの中心Ｘ座標
#define SCREEN_CENTER_Y	(SCREEN_HEIGHT / 2)	// ウインドウの中心Ｙ座標

#define	NUM_VERTEX		(4)					// 頂点数
#define	NUM_POLYGON		(2)					// ポリゴン数

// 3D用
const float	ASPECT_RATIO	= (float)SCREEN_WIDTH/SCREEN_HEIGHT;	// アスペクト比
const float	NEAR_CLIP		= 1.0f;									// ニアクリップを行う距離
const float	FAR_CLIP		= 3000.0f;								// ファークリップを行う距離

#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr) = NULL; } }

// 頂点フォーマット( 頂点座標[2D] / 反射光 / テクスチャ座標 )
#define	FVF_VERTEX_2D	(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
// 頂点フォーマット( 頂点座標[3D] / 法線ベクトル / 反射光 / テクスチャ座標 )
#define	FVF_VERTEX_3D	(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)

// 上記頂点フォーマットに合わせた構造体を定義
typedef struct
{
	D3DXVECTOR3 vtx;		// 頂点座標
	float rhw;				// テクスチャのパースペクティブコレクト用
	D3DCOLOR diffuse;		// 反射光
	D3DXVECTOR2 tex;		// テクスチャ座標
} VERTEX_2D;

typedef struct
{
	D3DXVECTOR3 vtx;		// 頂点座標
	D3DXVECTOR3 normal;		// 法線ベクトル
	D3DCOLOR diffuse;		// 反射光
	D3DXVECTOR2 tex;		// テクスチャ座標
} VERTEX_3D;

//-----------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------

#define MAX_FRAME			(60)

#define LEFTROLL			(-1)
#define RIGHTROLL			(1)
#define UPROLL				(-1)
#define DOWNROLL			(1)

#define CUBESIZE			(20)
#define CUBESPEED			((float)(CUBESIZE/2)/5)
#define CUBEROLL			((D3DX_PI/4)/5)
#define GRAVITY				((float)(9.8/25))

#define MESH_CUBE			_T("data/MODEL/Cube.x")
#define MESH_BALL			_T("data/MODEL/Ball.x")

//-----------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------
typedef struct _tStageData
{
	int			x;
	int			y;
	int			z;
}tStageData;

//-----------------------------------------------------------------------------
// 列挙型
//-----------------------------------------------------------------------------

enum MAINSTATE
{
	STATE_MAINMENU,
	STATE_SELECT,
	STATE_GAME,
	STATE_CLEAR,
	STATE_GAMEOVER,
	STATE_NOWLOADING,
};

enum STAGETYPE 
{
	STYPE_NONE,
	STYPE_STAGE,
	STYPE_PLAYER,
	STYPE_HOLE,
	STYPE_BLOCK,
	STYPE_WARP,
};

enum DATA
{
	DATA_MAINSTATE,		// メイン(画面遷移)
	DATA_STAGELEVEL,	// ステージレベル
	DATA_SELECTSTAGE,	// セレクトステージ
	DATA_TIME,			// 時間
	DATA_TIMELIMIT,		// 制限時間
};

enum DIRECTION
{
	DIR_Z_UP,
	DIR_X_DOWN,
	DIR_Z_DOWN,
	DIR_X_UP,
};

enum FADE
{
	FADE_ON,
	FADE_OFF,
};

enum SELECTSTATE
{
	SELECT_LEVEL,
	SELECT_NUMBER,
};

enum STAGELEVEL
{
	LEVEL_EASY,
	LEVEL_NORMAL,
	LEVEL_HARD,
};

enum SELECTSTAGE
{
	STAGE_ONE,
	STAGE_TWO,
	STAGE_THREE,
};

#endif