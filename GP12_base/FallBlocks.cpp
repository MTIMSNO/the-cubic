//===================================================================================
//
// 落下ブロック
//
//===================================================================================

#include "FallBlocks.h"

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------

// ブロックの設置
void SetFallBlocks(void);

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

cFallBlocks*		p_FallBlocks;

int					g_nFallBlocksColor[5];

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitFallBlocks(LPDIRECT3DDEVICE9 pDevice)
{
	// オブジェクトの作成
	p_FallBlocks = new cFallBlocks();

	for ( int i = 0; i < MAX_BLOCK; i++ )
	{
		// オブジェクトの作成
		p_FallBlocks->mFallBlocks[i] = new cMeshClass();

		// 表示
		p_FallBlocks->mFallBlocks[i]->SetMeshExist(false);
		p_FallBlocks->mFallBlocks[i]->Init(pDevice,MESH_CUBE);

		p_FallBlocks->tFallBlocks[i].d_Pos = D3DXVECTOR3(0.0f,0.0f,0.0f);
		p_FallBlocks->tFallBlocks[i].f_Speed = 0.0f;
		p_FallBlocks->tFallBlocks[i].d_Color = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
	}

	g_nFallBlocksColor[0] = 0xff0030;
	g_nFallBlocksColor[1] = 0xff7e00;
	g_nFallBlocksColor[2] = 0xffc600;
	g_nFallBlocksColor[3] = 0x34ef1b;
	g_nFallBlocksColor[4] = 0x2c6edb;

	return S_OK;
}

//-----------------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------------
void UninitFallBlocks(void)
{
	for ( int i = 0; i < MAX_BLOCK; i++ )
		delete p_FallBlocks->mFallBlocks[i];

	delete p_FallBlocks;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateFallBlocks(void)
{
	// 毎フレームカウントアップ
	p_FallBlocks->nFBcount++;
	// １秒に6個設置する
	if ( p_FallBlocks->nFBcount % 10 == 0 )
		SetFallBlocks();

	for ( int i = 0; i < MAX_BLOCK; i++ )
	{
		// メッシュの移動(表示しているののみ)
		if ( p_FallBlocks->mFallBlocks[i]->GetMeshExist() == false) continue;

		// 非表示にする
		if ( p_FallBlocks->mFallBlocks[i]->GetMeshPos().y <= -350.0f )
			p_FallBlocks->mFallBlocks[i]->SetMeshExist(false);
	
		// 移動処理
		p_FallBlocks->mFallBlocks[i]->SetMeshPos(0.0f,-(p_FallBlocks->tFallBlocks[i].f_Speed),0.0f);
		// 透過処理
		if ( p_FallBlocks->mFallBlocks[i]->GetMeshPos().y <= -300.0f )
			p_FallBlocks->tFallBlocks[i].d_Color.a -= 0.05f;
	}
}

//-----------------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------------
void DrawFallBlocks(LPDIRECT3DDEVICE9 pDevice)
{
	for ( int i = 0; i < MAX_BLOCK; i++ )
	{
		if ( p_FallBlocks->mFallBlocks[i]->GetMeshExist() != true ) continue;

		p_FallBlocks->mFallBlocks[i]->SetMeshColor(pDevice,p_FallBlocks->tFallBlocks[i].d_Color);
		p_FallBlocks->mFallBlocks[i]->Draw(pDevice);
	}
}

//-----------------------------------------------------------------------------------
// ブロックの設置
//-----------------------------------------------------------------------------------
void SetFallBlocks(void)
{
	for ( int i = 0; i < MAX_BLOCK; i++ )
	{
		// 表示している場合はコンテニュー
		if ( p_FallBlocks->mFallBlocks[i]->GetMeshExist() )		continue;

		switch ( GetData(DATA_MAINSTATE) )
		{
			/*
		case STATE_MAINMENU:
			// 位置の設定
			p_FallBlocks->tFallBlocks[i].d_Pos.y = 400.0f;
			p_FallBlocks->tFallBlocks[i].d_Pos.x = -(float)(rand()% (400-(-400)+400) - 400);
			p_FallBlocks->tFallBlocks[i].d_Pos.z = -(float)(rand()% 500 + 300);
			p_FallBlocks->tFallBlocks[i].f_Speed = (float)(rand()%(30-10+10)+10)/10;
			break;
			*/

		case STATE_SELECT:
			// 位置の設定
			p_FallBlocks->tFallBlocks[i].d_Pos.y = 300.0f;
			p_FallBlocks->tFallBlocks[i].d_Pos.x = (float)(rand()% (400-(-400)+400) - 400);
			p_FallBlocks->tFallBlocks[i].d_Pos.z = -(float)(rand()% 400 + 200);
			p_FallBlocks->tFallBlocks[i].f_Speed = (float)(rand()%(30-5+5)+5)/10;
			break;

		case STATE_GAME:
			// 位置の設定
			p_FallBlocks->tFallBlocks[i].d_Pos.y = 200.0f;

			do
			{
				p_FallBlocks->tFallBlocks[i].d_Pos.x = (float)(rand()%(250-(-250)+250)-250);
				p_FallBlocks->tFallBlocks[i].d_Pos.z = (float)(rand()%(250-(-250)+250)-250);
			}
			while ( p_FallBlocks->tFallBlocks[i].d_Pos.x > -200.0f && p_FallBlocks->tFallBlocks[i].d_Pos.x < 200.0f &&
					p_FallBlocks->tFallBlocks[i].d_Pos.z > -200.0f && p_FallBlocks->tFallBlocks[i].d_Pos.z < 200.0f );

			// 移動スピードの設定
			p_FallBlocks->tFallBlocks[i].f_Speed = (float)(rand()% (20-5+5)+5)/10;
			break;
		}

		// 表示するように設定
		p_FallBlocks->mFallBlocks[i]->SetMeshExist(true);

		// 位置の代入
		p_FallBlocks->mFallBlocks[i]->SetMeshPos(p_FallBlocks->tFallBlocks[i].d_Pos);

		// 色の設定
		p_FallBlocks->tFallBlocks[i].d_Color = g_nFallBlocksColor[0 + rand()%5-0];
		p_FallBlocks->tFallBlocks[i].d_Color.a = 1.0f;
		
		break;
	}
}

//-----------------------------------------------------------------------------------
// 再初期化処理
//-----------------------------------------------------------------------------------
void ReInitFallBlocks(void)
{
	for ( int i = 0; i < MAX_BLOCK; i++ )
	{
		// 表示
		p_FallBlocks->mFallBlocks[i]->SetMeshExist(false);

		p_FallBlocks->tFallBlocks[i].d_Pos = D3DXVECTOR3(0.0f,0.0f,0.0f);
		p_FallBlocks->tFallBlocks[i].f_Speed = 0.0f;
		p_FallBlocks->tFallBlocks[i].d_Color = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
	}
}