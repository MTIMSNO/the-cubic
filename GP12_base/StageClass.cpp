//===================================================================================
//
// ステージクラス
//
//===================================================================================

#pragma once

#include "StageClass.h"

//-----------------------------------------------------------------------------------
// コンストラクタ
//-----------------------------------------------------------------------------------
cStageClass::cStageClass()
{
	m_nPlayerNum = 0;
	m_nBeforePlayerNum = 0;
	m_bPlayerMove = false;
	m_bPlayerChange = true;
	m_tDecision.x = m_tDecision.y = m_tDecision.x = 0;
	m_bBlocksGimmick = false;
	m_bWarpGimmick = false;
	m_nWarpNum = 0;
	m_bUseWarp = false;
	m_bFade = false;
}

//-----------------------------------------------------------------------------------
// デストラクタ
//-----------------------------------------------------------------------------------
cStageClass::~cStageClass()
{
}

//-----------------------------------------------------------------------------------
// 選択プレイヤーのナンバーの取得
//-----------------------------------------------------------------------------------
int cStageClass::GetPlayerNum(void)
{
	return m_nPlayerNum;
}

//-----------------------------------------------------------------------------------
// 選択プレイヤーのナンバーの設定
//-----------------------------------------------------------------------------------
void cStageClass::SetPlayerNum(int num)
{
	m_nPlayerNum = num;
}

//-----------------------------------------------------------------------------------
// 一つ前に選択していたプレイヤーのナンバーの取得
//-----------------------------------------------------------------------------------
int cStageClass::GetBeforePlayerNum(void)
{
	return m_nBeforePlayerNum;
}

//-----------------------------------------------------------------------------------
// 一つ前に選択していたプレイヤーのナンバーの設定
//-----------------------------------------------------------------------------------
void cStageClass::SetBeforePlayerNum(int num)
{
	m_nBeforePlayerNum = num;
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替え状態の取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetPlayerChange(void)
{
	return m_bPlayerChange;
}

//-----------------------------------------------------------------------------------
// プレイヤーの切り替え状態の設定
//-----------------------------------------------------------------------------------
void cStageClass::SetPlayerChange(bool state)
{
	m_bPlayerChange = state;
}

//-----------------------------------------------------------------------------------
// プレイヤーの移動状態の取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetPlayerMove(void)
{
	return m_bPlayerMove;
}

//-----------------------------------------------------------------------------------
// プレイヤーの移動状態の設定
//-----------------------------------------------------------------------------------
void cStageClass::SetPlayerMove(bool state)
{
	m_bPlayerMove = state;
}

//-----------------------------------------------------------------------------------
// 移動先配列の取得
//-----------------------------------------------------------------------------------
tStageData cStageClass::GetDecision(void)
{
	return m_tDecision;
}

//-----------------------------------------------------------------------------------
// 移動先配列の設定
//-----------------------------------------------------------------------------------
void cStageClass::SetDecision(tStageData tArray)
{
	m_tDecision = tArray;
}

//-----------------------------------------------------------------------------------
// ブロックギミックの状態取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetBlockGimmick(void)
{
	return m_bBlocksGimmick;
}

//-----------------------------------------------------------------------------------
// ブロックギミックの使用変更
//-----------------------------------------------------------------------------------
void cStageClass::SetBlockGimmick(bool states)
{
	m_bBlocksGimmick = states;
}

//-----------------------------------------------------------------------------------
// ワープギミックの状態取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetWarpGimmick(void)
{
	return m_bWarpGimmick;
}

//-----------------------------------------------------------------------------------
// ワープギミックの使用変更
//-----------------------------------------------------------------------------------
void cStageClass::SetWarpGimmick(bool states)
{
	m_bWarpGimmick = states;
}

//-----------------------------------------------------------------------------------
// ワープナンバーの取得
//-----------------------------------------------------------------------------------
int cStageClass::GetWarpNumber(void)
{
	return m_nWarpNum;
}

//-----------------------------------------------------------------------------------
// ワープナンバーの取得
//-----------------------------------------------------------------------------------
void cStageClass::SetWarpNumber(int Num)
{
	m_nWarpNum = Num;
}

//-----------------------------------------------------------------------------------
// ワープを使用しているかの取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetWarpUseFlag(void)
{
	return m_bUseWarp;
}

//-----------------------------------------------------------------------------------
// ワープを使用しているかの取得
//-----------------------------------------------------------------------------------
void cStageClass::SetWarpUseFlag(bool states)
{
	m_bUseWarp = states;
}

//-----------------------------------------------------------------------------------
// フェードの取得
//-----------------------------------------------------------------------------------
bool cStageClass::GetFade(void)
{
	return m_bFade;
}

//-----------------------------------------------------------------------------------
// フェードの設定
//-----------------------------------------------------------------------------------
void cStageClass::SetFade(bool states)
{
	m_bFade = states;
}

//-----------------------------------------------------------------------------------
// プレイヤーの角度設定
//-----------------------------------------------------------------------------------
int cStageClass::SetPlayerAngle(int pAngle)
{
	// カメラ角度
	int cAngle = (int)GetCameraAngle();

	switch ( cAngle / 90 )
	{
	case 0:
		if ( GetKeyboardTrigger(DIK_UP) || GetCrossKeyPress() == 0)				return 180;
		else if ( GetKeyboardTrigger(DIK_RIGHT) || GetCrossKeyPress() == 1)		return 270;
		else if ( GetKeyboardTrigger(DIK_DOWN) || GetCrossKeyPress() == 2)		return 0;
		else if ( GetKeyboardTrigger(DIK_LEFT) || GetCrossKeyPress() == 3)		return 90;
		break;

	case 1:		
		if ( GetKeyboardTrigger(DIK_UP) || GetCrossKeyPress() == 0)				return 90;
		else if ( GetKeyboardTrigger(DIK_RIGHT) || GetCrossKeyPress() == 1)		return 180;
		else if ( GetKeyboardTrigger(DIK_DOWN) || GetCrossKeyPress() == 2)		return 270;
		else if ( GetKeyboardTrigger(DIK_LEFT) || GetCrossKeyPress() == 3)		return 0;
		break;

	case 2:
		if ( GetKeyboardTrigger(DIK_UP) || GetCrossKeyPress() == 0)				return 0;
		else if ( GetKeyboardTrigger(DIK_RIGHT) || GetCrossKeyPress() == 1)		return 90;
		else if ( GetKeyboardTrigger(DIK_DOWN) || GetCrossKeyPress() == 2)		return 180;
		else if ( GetKeyboardTrigger(DIK_LEFT) || GetCrossKeyPress() == 3)		return 270;
		break;

	case 3:
		if ( GetKeyboardTrigger(DIK_UP) || GetCrossKeyPress() == 0)				return 270;
		else if ( GetKeyboardTrigger(DIK_RIGHT) || GetCrossKeyPress() == 1)		return 0;
		else if ( GetKeyboardTrigger(DIK_DOWN) || GetCrossKeyPress() == 2)		return 90;
		else if ( GetKeyboardTrigger(DIK_LEFT) || GetCrossKeyPress() == 3)		return 180;
		break;
	}
	return pAngle;
}

//-----------------------------------------------------------------------------------
// 遷移判定
//-----------------------------------------------------------------------------------
int cStageClass::Transition ( tStageData tPlayer[] , tStageData tHole[] , int nPlayer , int nHole )
{
	// 穴に入っている数
	int HoleInCount = 0;

	for ( int p = 0; p < nPlayer; p++ )
	{
		// プレイヤーの配列代入
		tStageData* CurrentPlayer = &tPlayer[p];
		for ( int h = 0; h < nHole; h++ )
		{
			// ホールの配列代入
			tStageData* CurrentHole = &tHole[h];

			// プレイヤーと穴が一致した場合カウントアップ
			if ( CurrentPlayer->x == CurrentHole->x &&
				 CurrentPlayer->y == CurrentHole->y &&
				 CurrentPlayer->z == CurrentHole->z )
				 HoleInCount++;
		}
	}

	if ( HoleInCount == nPlayer )
		return STATE_CLEAR;
	else 
		return STATE_GAMEOVER;
}
