//===================================================================================
//
// ライト.h
//
//===================================================================================

#include "Light.h"

//-----------------------------------------------------------------------------------
// グローバル変数
//-----------------------------------------------------------------------------------

D3DLIGHT9			g_light;
int					g_nLightState;
D3DXCOLOR			g_dColor;
D3DXVECTOR3			g_dPos;
D3DXVECTOR3			g_dDirect;

//-----------------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------------
HRESULT InitLight(LPDIRECT3DDEVICE9 pDevice)
{
	// ライトを有効にする
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// アンビエントライト（環境光）を設定する
	pDevice->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
	// スペキュラ（鏡面反射）を有効にする
	pDevice->SetRenderState(D3DRS_SPECULARENABLE, TRUE);
	// ライトをあてる（白色で鏡面反射ありに設定）
	// 光の向きを決める
	D3DXVECTOR3 vecDirection(0, -1, 0);		
	// ライトオブジェクト
	g_light.Type = D3DLIGHT_DIRECTIONAL;			
	g_light.Diffuse.r  = 0.75f;
	g_light.Diffuse.g  = 0.75f;
	g_light.Diffuse.b  = 0.75f;
	g_light.Specular.r = 0.0f;
	g_light.Specular.g = 0.0f;
	g_light.Specular.b = 0.0f;
	// 単位ベクトルにする
	D3DXVec3Normalize((D3DXVECTOR3*)&g_light.Direction, &vecDirection);

	pDevice->SetLight(0, &g_light);
	pDevice->LightEnable(0, TRUE);			// ライト０を有効

	g_dColor = D3DXCOLOR(0.0f,0.0f,0.0f,0.0f);
	g_dDirect = D3DXVECTOR3(0,-1,0);
	
	return S_OK;
}

//-----------------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------------
void UpdateLight(LPDIRECT3DDEVICE9 pDevice)
{
	if (GetData(DATA_MAINSTATE) == STATE_MAINMENU || GetData(DATA_MAINSTATE) == STATE_SELECT ||	GetData(DATA_MAINSTATE) == STATE_GAME )
	{
		g_dColor = D3DXCOLOR(0.5f,0.5f,0.5f,1.0f);
		SetDirectional(pDevice,g_dColor,0xaaaaaaaa,g_dDirect);
	}
	else if (GetData(DATA_MAINSTATE) == STATE_CLEAR )
	{
		g_dColor = D3DXCOLOR(1.0f,1.0f,1.0f,1.0f);
		g_dPos = D3DXVECTOR3(0.0f,100.0f,-100.0f);
		SetPoint(pDevice,g_dPos,g_dColor,0xffffffff);
	}
	else if (GetData(DATA_MAINSTATE) == STATE_GAMEOVER )
	{
		g_dColor = D3DXCOLOR(0.4f,0.4f,0.4f,1.0f);
		g_dPos = D3DXVECTOR3(0.0f,100.0f,-100.0f);
		SetPoint(pDevice,g_dPos,g_dColor,0xffffffff);
	}
}

//-----------------------------------------------------------------------------------
// 平行光源
//-----------------------------------------------------------------------------------
void SetDirectional(LPDIRECT3DDEVICE9 pDevice , D3DXCOLOR color , DWORD ambi , D3DXVECTOR3 Direct)
{
	g_light.Type = D3DLIGHT_DIRECTIONAL;
	g_light.Diffuse.r = color.r;
	g_light.Diffuse.g = color.g;
	g_light.Diffuse.b = color.b;
	pDevice->SetRenderState(D3DRS_AMBIENT, ambi);
	//位置
	g_light.Position = D3DXVECTOR3 ( 0.0f, 200.0f, 0.0f );
	//方向
	g_light.Direction = Direct;

	pDevice->SetLight(0, &g_light);
}
//-----------------------------------------------------------------------------------
// 点光源
//-----------------------------------------------------------------------------------
void SetPoint( LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR3 pos, D3DXCOLOR color , DWORD ambi)
{
	g_light.Type = D3DLIGHT_POINT;
	g_light.Diffuse.r = color.r;
	g_light.Diffuse.g = color.g;
	g_light.Diffuse.b = color.b;
	pDevice->SetRenderState(D3DRS_AMBIENT, ambi);
	//位置
	g_light.Position = pos;
	//光の届く距離
	g_light.Range = 2000.0f;
	//減衰率
	g_light.Falloff = 10.0f;
	g_light.Attenuation0 = 1.0f;
	g_light.Attenuation1 = 0.0f;
	g_light.Attenuation2 = 0.0f;

	pDevice->SetLight(0, &g_light);
}
//-----------------------------------------------------------------------------------
// スポットライト
//-----------------------------------------------------------------------------------
void SetSpot( LPDIRECT3DDEVICE9 pDevice, D3DXCOLOR color , D3DXVECTOR3 pos , D3DXVECTOR3 Direct , DWORD ambi)
{
	g_light.Type = D3DLIGHT_SPOT;
	g_light.Diffuse.r = color.r;
	g_light.Diffuse.g = color.g;
	g_light.Diffuse.b = color.b;
	pDevice->SetRenderState(D3DRS_AMBIENT, ambi);
	//位置
	g_light.Position = pos;
	//方向
	g_light.Direction = Direct;
	//コーン
	g_light.Theta = 1.5f;
	g_light.Phi = 2.0f;
	//光の届く距離
	g_light.Range = 2000.0f;
	//減衰率
	g_light.Falloff = 10.0f;
	g_light.Attenuation0 = 1.0f;
	g_light.Attenuation1 = 0.0f;
	g_light.Attenuation2 = 0.0f;

	pDevice->SetLight(0, &g_light);
}