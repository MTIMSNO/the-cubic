//=============================================================================
//
// ベースプログラム [main.cpp]
//
//=============================================================================

#include "Main.h"
#include "Input.h"
#include "FallBlocks.h"
#include "Particle.h"
#include "Beacon.h"
#include "SelectMenu.h"
#include "Fog.h"
#include "DiffusionBlocks.h"
#include "Debug.h"
#include "Ending.h"
#include "StageData.h"
#include "SelectMenuManual.h"

#include "NowLoading.h"

//#include "MeshStage01.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define CLASS_NAME			_T("AppClass")							// ウインドウのクラス名
#define WINDOW_NAME			_T("Cubic - キュービック -")			// ウインドウのキャプション名

//*****************************************************************************
// 構造体定義
//*****************************************************************************

//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HRESULT Init(HWND hWnd, BOOL bWindow);
void Uninit(void);
void Update(void);
void Draw(void);
void BufferClear(void);

//*****************************************************************************
// グローバル変数:
//*****************************************************************************
LPDIRECT3D9				g_pD3D = NULL;				// Direct3Dオブジェクト
LPDIRECT3DDEVICE9		g_pD3DDevice = NULL;		// Deviceオブジェクト(描画に必要)

int						g_nMainState;

//=============================================================================
// メイン関数
//=============================================================================
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);	// 無くても良いけど、警告が出る（未使用宣言）
	UNREFERENCED_PARAMETER(lpCmdLine);		// 無くても良いけど、警告が出る（未使用宣言）

	// 時間計測用
	DWORD dwExecLastTime;
	DWORD dwFPSLastTime;
	DWORD dwCurrentTime;
	DWORD dwFrameCount;

	WNDCLASSEX	wcex = {
		sizeof(WNDCLASSEX),
		CS_CLASSDC,
		WndProc,
		0,
		0,
		hInstance,
		NULL,
		LoadCursor(NULL, IDC_ARROW),
		(HBRUSH)(COLOR_WINDOW+1),
		NULL,
		CLASS_NAME,
		NULL
	};
	HWND		hWnd;
	MSG			msg;
	
	// ウィンドウクラスの登録
	RegisterClassEx(&wcex);

	// ウィンドウの作成
	hWnd = CreateWindow(CLASS_NAME,
						WINDOW_NAME,
						WS_OVERLAPPEDWINDOW,
						CW_USEDEFAULT,																		// ウィンドウの左座標
						CW_USEDEFAULT,																		// ウィンドウの上座標
						SCREEN_WIDTH + GetSystemMetrics(SM_CXDLGFRAME)*2,									// ウィンドウ横幅
						SCREEN_HEIGHT + GetSystemMetrics(SM_CXDLGFRAME)*2+GetSystemMetrics(SM_CYCAPTION),	// ウィンドウ縦幅
						NULL,
						NULL,
						hInstance,
						NULL);

	// DirectXの初期化(ウィンドウを作成してから行う)
	if(FAILED(Init(hWnd, true)))
	{
		return -1;
	}

	// 入力処理の初期化
	InitInput(hInstance, hWnd);

	// フレームカウント初期化
	timeBeginPeriod( 1 );	// 分解能を設定
	dwExecLastTime =
	dwFPSLastTime =	timeGetTime();	// システム時刻をミリ秒単位で取得
	dwCurrentTime =
	dwFrameCount =	0;

	// ウインドウの表示(Init()の後に呼ばないと駄目)
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	
	// メッセージループ
	while(1)
	{
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{// PostQuitMessage()が呼ばれたらループ終了
				break;
			}
			else
			{
				// メッセージの翻訳とディスパッチ
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
        }
		else
		{
			dwCurrentTime = timeGetTime();					// システム時刻を取得

			if( ( dwCurrentTime - dwFPSLastTime ) >= 500 )	// 0.5秒ごとに実行
			{
				dwFPSLastTime = dwCurrentTime;				// FPSを測定した時刻を保存
				dwFrameCount = 0;							// カウントをクリア
			}

			if( ( dwCurrentTime - dwExecLastTime ) >= (1000/60) )	// 1/60秒ごとに実行
			{
				dwExecLastTime = dwCurrentTime;	// 処理した時刻を保存

				// 更新処理
				Update();

				// 描画処理
				Draw();

				dwFrameCount++;		// 処理回数のカウントを加算
			}
		}
	}
	
	// ウィンドウクラスの登録を解除
	UnregisterClass(CLASS_NAME, wcex.hInstance);

	// 終了処理
	Uninit();

	// 入力処理の終了処理
	UninitInput();

	return (int)msg.wParam;
}

//=============================================================================
// プロシージャ
//=============================================================================
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch( message )
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_DELETE:					// [ESC]キーが押された
			DestroyWindow(hWnd);		// ウィンドウを破棄するよう指示する
			break;
		}
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT Init(HWND hWnd, BOOL bWindow)
{
	D3DPRESENT_PARAMETERS d3dpp;
    D3DDISPLAYMODE d3ddm;

	// Direct3Dオブジェクトの作成
	g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if(g_pD3D == NULL)
	{
		return E_FAIL;
	}

	// 現在のディスプレイモードを取得
    if(FAILED(g_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
	{
		return E_FAIL;
	}

	// デバイスのプレゼンテーションパラメータの設定
	ZeroMemory(&d3dpp, sizeof(d3dpp));							// ワークをゼロクリア
	d3dpp.BackBufferCount			= 1;						// バックバッファの数
	d3dpp.BackBufferWidth			= SCREEN_WIDTH;				// ゲーム画面サイズ(幅)
	d3dpp.BackBufferHeight			= SCREEN_HEIGHT;			// ゲーム画面サイズ(高さ)
	d3dpp.BackBufferFormat			= D3DFMT_UNKNOWN;			// バックバッファのフォーマットは現在設定されているものを使う
	d3dpp.SwapEffect				= D3DSWAPEFFECT_DISCARD;	// 映像信号に同期してフリップする
	d3dpp.Windowed					= bWindow;					// ウィンドウモード
	d3dpp.EnableAutoDepthStencil	= TRUE;						// デプスバッファ（Ｚバッファ）とステンシルバッファを作成
	d3dpp.AutoDepthStencilFormat	= D3DFMT_D24S8;				// デプスバッファとして24bit、ステンシルバッファとして8bitを使う
	d3dpp.BackBufferFormat			= d3ddm.Format;				// カラーモードの指定

	if(bWindow)
	{// ウィンドウモード
		d3dpp.BackBufferFormat           = D3DFMT_UNKNOWN;					// バックバッファ
		d3dpp.FullScreen_RefreshRateInHz = 0;								// リフレッシュレート
		d3dpp.PresentationInterval       = D3DPRESENT_INTERVAL_IMMEDIATE;	// インターバル
	}
	else
	{// フルスクリーンモード
		d3dpp.BackBufferFormat           = D3DFMT_R5G6B5;					// バックバッファ
		d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;			// リフレッシュレート
		d3dpp.PresentationInterval       = D3DPRESENT_INTERVAL_DEFAULT;		// インターバル
	}

	// デバイスの生成
	// ディスプレイアダプタを表すためのデバイスを作成
	// 描画と頂点処理をハードウェアで行なう
	if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT,							// ディスプレイアダプタ
									D3DDEVTYPE_HAL,								// ディスプレイタイプ
									hWnd,										// フォーカスするウインドウへのハンドル
									D3DCREATE_HARDWARE_VERTEXPROCESSING,		// デバイス作成制御の組み合わせ
									&d3dpp,										// デバイスのプレゼンテーションパラメータ
									&g_pD3DDevice)))							// デバイスインターフェースへのポインタ
	{
		// 上記の設定が失敗したら
		// 描画をハードウェアで行い、頂点処理はCPUで行なう
		if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
										D3DDEVTYPE_HAL, 
										hWnd, 
										D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
										&d3dpp,
										&g_pD3DDevice)))
		{
			// 上記の設定が失敗したら
			// 描画と頂点処理をCPUで行なう
			if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
											D3DDEVTYPE_REF,
											hWnd, 
											D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
											&d3dpp,
											&g_pD3DDevice)))
			{
				// 初期化失敗
				return E_FAIL;
			}
		}
	}

	// レンダリングステートパラメータの設定
    g_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);				// カリングを行わない
	g_pD3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);						// Zバッファを使用
	g_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを行う
	g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの指定
	g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// αデスティネーションカラーの指定

	// サンプラーステートパラメータの設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);	// テクスチャＵ値の繰り返し設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);	// テクスチャＶ値の繰り返し設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);	// テクスチャ拡大時の補間設定
	g_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);	// テクスチャ縮小時の補間設定

	// 画面遷移
	g_nMainState = STATE_MAINMENU;
	// デバック
	InitDebug(g_pD3DDevice);
	// カメラ
	InitCamera(g_pD3DDevice);
	// ライト
	InitLight(g_pD3DDevice);
	// データ
	InitData();
	// フェード
	InitFade(g_pD3DDevice);
	// 落下ブロック
	InitFallBlocks(g_pD3DDevice);
	// パーティクル
	InitParticle(g_pD3DDevice);
	// ビーコン
	InitBeacon(g_pD3DDevice);
	// セレクトメニュー
	InitSelectMenu(g_pD3DDevice);
	// 拡散ブロック
	InitDiffusionBlocks(g_pD3DDevice);
	// エンディング
	InitEnding(g_pD3DDevice);
	// ロード
	InitNowLoading(g_pD3DDevice);
	// Manual
	InitSelectMenuManual(g_pD3DDevice);

	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void Uninit(void)
{
	if(g_pD3DDevice != NULL)
	{// デバイスの開放
		g_pD3DDevice->Release();
	}

	if(g_pD3D != NULL)
	{// Direct3Dオブジェクトの開放
		g_pD3D->Release();
	}
	
	// パーティクル
	UninitParticle();
	// 落下ブロック
	UninitFallBlocks();
	// ビーコン
	UninitBeacon();
	// セレクトメニュー
	UninitSelectMenu();
	// 拡散ブロック
	UninitDiffusionBlocks();
	// エンディング
	UninitEnding();
	// マニュアル
	UninitSelectMenuManual();
}

//=============================================================================
// 更新処理
//=============================================================================
void Update(void)
{
	// 画面遷移
	g_nMainState = GetData(DATA_MAINSTATE);
	// 入力の更新処理
	UpdateInput();
	// カメラの更新処理
	UpdateCamera(g_pD3DDevice);
	// ライトの更新処理
	UpdateLight(g_pD3DDevice);
	// フェードの更新処理
	UpdateFade();
	// デバックの更新処理
	UpdateDebug();

	switch ( g_nMainState )
	{
	case STATE_MAINMENU:
		UpdateFallBlocks();
		break;

	case STATE_SELECT:
		UpdateFallBlocks();
		UpdateSelectMenu();
		UpdateSelectMenuManual();
		break;

	case STATE_GAME:
		UpdateStageData();
		UpdateParticle();
		UpdateBeacon();
		UpdateFallBlocks();
		break;

	case STATE_CLEAR:
		UpdateEnding();
		UpdateDiffusionBlocks();
		break;

	case STATE_GAMEOVER:
		UpdateEnding();
		break;

	case STATE_NOWLOADING:
		UpdateNowLoading(g_pD3DDevice);
		break;
	}
	
}

//=============================================================================
// 描画処理
//=============================================================================
void Draw(void)
{
	// バックバッファ＆Ｚバッファのクリア
	//g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(255, 255, 255, 255), 1.0f, 0);
	BufferClear();

	// Direct3Dによる描画の開始
	if(SUCCEEDED(g_pD3DDevice->BeginScene()))
	{
		// 2Dポリゴンを3Dポリゴンより奥に描く場合はここ

		// Zバッファだけクリア
		g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(128, 128, 255, 0), 1.0f, 0);
		
		switch ( g_nMainState )
		{
		case STATE_MAINMENU:
			DrawVersion(g_pD3DDevice);
			DrawFallBlocks(g_pD3DDevice);
			break;

		case STATE_SELECT:
			DrawSelectMenu(g_pD3DDevice);
			DrawFallBlocks(g_pD3DDevice);
			DrawSelectMenuManual(g_pD3DDevice);

			// デバック
			DrawDebug(g_pD3DDevice);
			break;

		case STATE_GAME:
			DrawFallBlocks(g_pD3DDevice);
			DrawStageData(g_pD3DDevice);
			DrawBeacon(g_pD3DDevice);

			// パーティクルの描画処理
			DrawParticle(g_pD3DDevice);
			break;

		case STATE_CLEAR:
			DrawEnding(g_pD3DDevice);
			DrawDiffusionBlocks(g_pD3DDevice);
			break;

		case STATE_GAMEOVER:
			DrawEnding(g_pD3DDevice);
			break;

		case STATE_NOWLOADING:
			DrawNowLoading(g_pD3DDevice);
			break;
		}

		
		
		// フェードの描画処理
		DrawFade(g_pD3DDevice);

		// Direct3Dによる描画の終了
		g_pD3DDevice->EndScene();
	}

	// バックバッファとフロントバッファの入れ替え
	g_pD3DDevice->Present(NULL, NULL, NULL, NULL);
}

//=============================================================================
// バックバッファ＆Ｚバッファのクリア
//=============================================================================
void BufferClear(void)
{
	if(GetData(DATA_MAINSTATE) == STATE_MAINMENU || GetData(DATA_MAINSTATE) == STATE_SELECT ||
	   GetData(DATA_MAINSTATE) == STATE_GAME || GetData(DATA_MAINSTATE) == STATE_NOWLOADING )
		g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(100, 100, 100, 100), 1.0f, 0);
	else if (GetData(DATA_MAINSTATE) == STATE_CLEAR)
		g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(255, 255, 255, 255), 1.0f, 0);
	else if (GetData(DATA_MAINSTATE) == STATE_GAMEOVER)
		g_pD3DDevice->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(0, 0, 0, 0), 1.0f, 0);
}

