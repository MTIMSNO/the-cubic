//===================================================================================
//
// 拡散ブロック
//
//===================================================================================
#pragma once

#include "Main.h"
#include "MeshClass.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define MAX_DIFBLOCK						(120)

//-----------------------------------------------------------------------------------
// 構造体定義
//-----------------------------------------------------------------------------------

typedef struct _tDiffusionBlocks
{
	// 速度
	D3DXVECTOR3		d_Speed;
	// 中心座標
	D3DXVECTOR3		d_Pos;
	// 色
	D3DXCOLOR		d_Color;
	// スケール
	D3DXVECTOR3		d_Scale;

}tDiffusionBlocks;

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cDiffusionBlocks
{
public:
	// オブジェクト
	cMeshClass*			mDiffusionBlocks[MAX_DIFBLOCK];
	// フォールブロック
	tDiffusionBlocks	tDiffusionBlocks[MAX_DIFBLOCK];
	// カウント
	int					nDBcount;
		
	// Tween用変数
	int					nMaxFrame;
	int					nCurrentFrame[MAX_DIFBLOCK];
};

//-----------------------------------------------------------------------------------
// プロトタイプ宣言
//-----------------------------------------------------------------------------------
HRESULT InitDiffusionBlocks(LPDIRECT3DDEVICE9 pDevice);
void UninitDiffusionBlocks(void);
void UpdateDiffusionBlocks(void);
void DrawDiffusionBlocks(LPDIRECT3DDEVICE9 pDevice);
void SetDiffusionBlocks(void);