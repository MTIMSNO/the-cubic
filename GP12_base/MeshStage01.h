//===================================================================================
//
// メッシュステージ
//
//===================================================================================
#pragma once

#include "Main.h"
#include "StageClass.h"
#include "MeshClass.h"

//-----------------------------------------------------------------------------------
// マクロ定義
//-----------------------------------------------------------------------------------

#define STAGE01_X				(11)
#define STAGE01_Y				(7)
#define STAGE01_Z				(11)
#define MAX_PLAYER01			(2)
#define MAX_HOLE01				(2)
#define MAX_BLOCK01				(2)
#define MAX_WARP01				(2)

//-----------------------------------------------------------------------------------
// クラス定義
//-----------------------------------------------------------------------------------

class cMeshStage01 : public cStageClass
{
public:
	// メッシュ
	cMeshClass*			mStage01[STAGE01_Z][STAGE01_Y][STAGE01_X];
	// プレイヤー / ホール
	tStageData			Player01[MAX_PLAYER01+1];
	tStageData			Hole01[MAX_HOLE01];
	tStageData			Block01[MAX_BLOCK01];
	tStageData			Warp01[MAX_WARP01];
};


HRESULT InitMeshStage01 (LPDIRECT3DDEVICE9 pDevice);
void UninitMeshStage01(void);
void UpdateMeshStage01(void);
void DrawMeshStage01(LPDIRECT3DDEVICE9 pDevice);

int GetPlayerAngle01();
D3DXVECTOR3 GetPlayerPos01(void);
D3DXVECTOR3 GetCameraMoveVol01(void);
bool GetPlayerChange01(void);
